angular.module('main', ['main.users', 'main.dashboard', 'main.pins', 'main.profile', 'ui.router', 'angular-jwt', 'httpPostFix', 'angularMoment', 'angular-loading-bar', 'datatables', 'ui.bootstrap', 'environment'])
    .run(['$rootScope', '$state', function($rootScope, $state) {
    }]).factory('httpInterceptor', function ($q, $rootScope, $injector) {
        return {
            request: function (config) {
                return config || $q.when(config)
            },
            response: function (response) {
                return response || $q.when(response);
            },
            responseError: function (response) {
                if (response.status === 401) {
                    console.log('401 Occurred');
                    $injector.get('$state').transitionTo('main', { 'e': 1 });
                }
                return $q.reject(response);
            }
        };
    })
    .config(function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, jwtInterceptorProvider, /*common,*/ envServiceProvider/*, envService*/) {
        $stateProvider
            .state('main', {
                url: '/login?e=:expired',
                templateUrl: 'modules/shared/views/login.html',
                controller: 'AuthController'
            })
            .state('dashboard', {
                url: '/dashboard',
                //templateUrl: 'modules/shared/views/dashboard.html',
                templateUrl: 'modules/shared/views/layout.html',
                controller: 'DashboardController'
            })
            .state('users', {
                url: '/users',
                //template: '<div ui-view></div>',
                templateUrl: 'modules/shared/views/layout.html',
                //controller: 'UsersController'
            })
            .state('pins', {
                url: '/pins',
                //template: '<div ui-view></div>',
                templateUrl: 'modules/shared/views/layout.html',
                //controller: 'PinsController'
            })
			.state('profile', {
                url: '/profile',
                //template: '<div ui-view></div>',
                templateUrl: 'modules/shared/views/layout.html',
                //controller: 'ProfileController'
            });
        $urlRouterProvider.otherwise('/login');

        jwtInterceptorProvider.tokenGetter = ['common', function(common) {
            return localStorage.getItem('admin_token');
        }];

        $httpProvider.interceptors.push('jwtInterceptor');
        $httpProvider.interceptors.push('httpInterceptor');
        envServiceProvider.config({
            domains: {
                development: ['admin.addypin'],
                staging: ['admin.addypin.digitalmeetings.net'],
                production: ['admin.addypin.com']
                // anotherStage: []
            },
            vars: {
                development: {
                    apiUrl: '//api.addypin.com',
                    //staticUrl: '//localhost/static'
                    // antoherCustomVar: ''
                },
                staging: {
					//apiUrl: '//api.addypin'
                    apiUrl: '//api.addypin.digitalmeetings.net'
                },
                production: {
                    apiUrl: '//api.addypin.com',
                    //staticUrl: '//static.acme.com'
                    // antoherCustomVar: ''
                }
            }
        });
        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.withCredentials = true;
        delete $httpProvider.defaults.headers.common["X-Requested-With"];
        $httpProvider.defaults.headers.common["Accept"] = "application/json";
        $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
        //common.environmentConfig();
        //envService.set('development');
    })
    .controller('AuthController', ['$scope', '$rootScope', '$location', '$timeout', '$stateParams', function ($scope, $rootScope, $location, $timeout, $stateParams) {

    }])
;