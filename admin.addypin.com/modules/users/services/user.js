angular.module('main').factory('UserService', function ($timeout, common, $http, url){
    return {
        GetTotalUsersCount: function (){
            var promise = $http({ url: url.apiUrl('/users/get/all/count'), method: 'GET',})
                .then(function (response){
                    return response.data;
                });
            return promise;
        },
        GetTotalIndividualUsersCount: function (){
            var promise = $http({ url: url.apiUrl('/users/get/all/individual/count'), method: 'GET',})
                .then(function (response){
                    return response.data;
                });
            return promise;
        },
        GetTotalCorporateUsersCount: function (){
            var promise = $http({ url: url.apiUrl('/users/get/all/corporate/count'), method: 'GET',})
                .then(function (response){
                    return response.data;
                });
            return promise;
        },
        GetUserById: function (user_id){
            var promise = $http({ url: url.apiUrl('/users/by/id/' + user_id), method: 'GET',})
                .then(function (response){
                    return response.data;
                });
            return promise;
        },
		SaveUser: function (requestData){
            var promise = $http({ url: url.apiUrl('/users/save'), method: 'POST', data:requestData})
                .then(function (response){
					return response.data;
					
                });
            return promise;
        },
		DeleteUser: function (user_id){
            var promise = $http({ url: url.apiUrl('/users/delete/' +user_id), method: 'POST' })
                .then(function (response){
                    //console.log(response.data);
					return response.data;
					
                });
            return promise;
        }
    };
});