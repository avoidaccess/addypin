angular.module('main.users').controller('UsersController', ['$scope', '$rootScope', '$location', '$timeout', '$stateParams', 'DTOptionsBuilder', 'DTColumnBuilder', 'UserService', 'jwtHelper', 'url', 'envService','$state', function ($scope, $rootScope, $location, $timeout, $stateParams, DTOptionsBuilder, DTColumnBuilder, UserService, jwtHelper, url, envService,$state) {
    envService.set('development');
	
    
    var loc = $location.path();
    $scope.loc = loc.substring(loc.lastIndexOf("/") + 1, loc.length) 

    var token = localStorage.getItem('admin_token');


    if(token != null && token != undefined && !jwtHelper.isTokenExpired(token)) {

        var current_url = '/users/get/all?api_key=' + token +'&is_deleted=0';
        //alert($scope.loc);
        if($scope.loc == 'corporate'){
             current_url = '/users/get/corporate?api_key=' + token +'&is_deleted=0&is_active=1';
        }else  if($scope.loc == 'individual'){
             current_url = '/users/get/individual?api_key=' + token +'&is_deleted=0&is_active=1';
        }else  if($scope.loc == 'all'){
             current_url = '/users/get/all?api_key=' + token +'&is_deleted=0';
        }else  if($scope.loc == 'active'){
             current_url = '/users/get/all?api_key=' + token +'&is_deleted=0&is_active=1';
        }
       //  alert(url);
        $scope.dtOptions = DTOptionsBuilder.fromSource(url.apiUrl(current_url))
         .withOption('stateSave', true)

   /* .withOption('stateSaveCallback', function(settings,data) {
    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
  })
  .withOption('stateLoadCallback', function(settings) {
    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance ))
  })*/

            .withPaginationType('full_numbers').withDataProp('users');
        $scope.dtColumns = [
            //DTColumnBuilder.newColumn('first_name').withTitle('First Name'),
            //DTColumnBuilder.newColumn('last_name').withTitle('Last name'),
            DTColumnBuilder.newColumn('email').withTitle('Email'),
            DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                .renderWith(actionsHtml)
        ];
    }
    function actionsHtml(data, type, full, meta) {
        console.log(data);
        var user_id = data.user_id;
        //vm.persons[data.id] = data;
        return '<a href="/#/pins/userPins/' + user_id + ' " class="btn btn-primary ">' +
            '   <i class="fa fa-eye"></i> ' +
            '</a>&nbsp;' +

            '<a href="/#/users/edit/' + user_id + '" class="btn btn-warning ">' +
            '   <i class="fa fa-edit"></i>' +
            '</a>&nbsp;' +

            '<a href="/#/users/delete/' + user_id + ' " class="btn btn-danger ">' +
            '   <i class="fa fa-trash-o"></i>' +
            '</a>'

           ;
    }
	$scope.fun = function(){
		console.log('Delete Button Clicked');
	}
	$scope.userUpdated = false;
    var id = $stateParams.id;
    var editMode = id != undefined && parseInt(id) > 0;




    if(editMode){
        $rootScope.is_edit = 1;
        $scope.user_id = id;
        UserService.GetUserById(id).then(function (data){
            $scope.userData = data.user[0];
			
        });
        $scope.saveUser = function (){
			console.log($scope.userData);
//alert(JSON.stringify($scope.userData));

			UserService.SaveUser($scope.userData).then(function (data){

			if(data.rowsAffected != null && data.rowsAffected != undefined && data.rowsAffected > 0){
                $scope.messageClass = "success";
                $scope.message = "Success! The User was Updated successfully.";
                $scope.userUpdated = true;
				console.log('Saved');
            }
            else{
                $scope.messageClass = "danger";
                $scope.message = "Error! There was an error. Please try again in a moment.";
                $scope.userUpdated = true;
            }
			
        });
        }
    }else{

         if($rootScope.is_edit == 1){
            $rootScope.is_edit = 0;
           location.reload(); 
          // $route.reload();
        }
        $rootScope.is_edit = 0;
    }
	$scope.showDelete = false;
	var uid = $stateParams.uid;
	var deleteMode = uid != undefined && parseInt(uid) > 0;

    if(deleteMode){
        $scope.user_id = uid;
		$scope.showDelete = true;
	}
	$scope.cancel = function(){
		$state.go('users.all');
	}
	$scope.deleted = function(uid){
		UserService.DeleteUser(uid).then(function (data){
			console.log('Deleted');
			$scope.showDelete = false;
			$state.go('users.all');
			
			});
	}
   

   


}]);