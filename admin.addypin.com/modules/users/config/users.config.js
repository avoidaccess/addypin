angular.module('main.users').config(function($stateProvider) {
    $stateProvider
        .state('users.all', {
            url: '/type/:any',
            templateUrl: 'modules/users/views/users.html',
            controller: 'UsersController'
        })
        .state('users.view', {
            url: '/view/:id',
            templateUrl: 'modules/users/views/user.html',
            controller: 'UsersController'
        })
        .state('users.edit', {
            url: '/edit/:id',
            templateUrl: 'modules/users/views/user-edit.html',
            controller: 'UsersController'
        })
		.state('users.delete', {
            url: '/delete/:uid',
            templateUrl: 'modules/users/views/users.html',
            controller: 'UsersController'
        })
    ;
});