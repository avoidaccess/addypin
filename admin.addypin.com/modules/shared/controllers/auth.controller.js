angular.module('main').controller('AuthController', ['$scope', '$rootScope', '$location', '$timeout', '$stateParams', '$http', '$state', 'jwtHelper', 'url', 'envService', function ($scope, $rootScope, $location, $timeout, $stateParams, $http, $state, jwtHelper, url, envService) {
    $scope.user = {
        username: '',
        password: ''
    };

    var token = localStorage.getItem('admin_token');
    //envService.set('development');
	envService.set(url.host($location.host()));
    /*var allVars = common.apiUrl();
    console.log(allVars);*/

    if(token != null && token != undefined && !jwtHelper.isTokenExpired(token)){
        var currentPath = $location.path();
        var parts = currentPath.substring(1).split('/');

        if(parts[0] == 'login')
            $state.go('dashboard.index');
    }

    var e = $stateParams.e;
    $scope.isExpired = e != undefined && parseInt(e) == 1;
    $scope.expiredMessage = "Session Expired. Please login again";

    $scope.loginFailed = false;
    $scope.shakeClass = "";
    $scope.failedMessage = "Invalid Username/Password";

    $scope.login = function (){
        /*console.log('login button clicked');
        return;*/
        $scope.shakeClass = "";
        console.log($state);
        $http({
            url: url.apiUrl('/login'),
            method: 'POST',
            data: $scope.user
        })
        .then(function(response) {
            // success
            console.log('success');
            console.log(response);

            if(response.data.valid){
                var token = response.data.token;
                localStorage.setItem('admin_token', token);
                $scope.shakeClass = "";
                $state.go('dashboard.index');
            }
            else{
                $scope.loginFailed = true;
                $scope.shakeClass = "animated shake";
            }

            //console.log(response.data.token);

        },
        function(response) { // optional
            // failed
            console.log('failed');
            console.log(response);
        });
    };
}]);