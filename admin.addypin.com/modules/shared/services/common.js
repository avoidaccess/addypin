angular.module('main').factory('common', function ($timeout, $http){
    return {
        http: {
            post: function (url, data, successCallback, errorCallback){
                var promise = $http({
                    url: url,
                    method: 'POST',
                    data: data
                }).then(function (response){
                    if(successCallback && successCallback != undefined && successCallback != null){
                        successCallback(response);
                    }
                    return response.data;
                },
                function (response){
                    if(errorCallback && errorCallback != undefined && errorCallback != null){
                        errorCallback(response);
                    }
                    //return response.data;
                });
                return promise;
            },
            get: function (url, data, successCallback, errorCallback){
                var promise = $http({
                    url: url,
                    method: 'GET',
                }).then(function (response){
                        if(successCallback && successCallback != undefined && successCallback != null){
                            successCallback(response);
                        }
                        return response.data;
                    },
                    function (response){
                        if(errorCallback && errorCallback != undefined && errorCallback != null){
                            errorCallback(response);
                        }
                        //return response.data;
                    });
                return promise;
            }
        },
        environmentConfig: function (){
            return {
                domains: {
                    development: ['admin.addypin'],
                    production: ['admin.addypin.com']
                    // anotherStage: []
                },
                vars: {
                    development: {
                        apiUrl: '//api.addypin',
                            //staticUrl: '//localhost/static'
                        // antoherCustomVar: ''
                    },
                    production: {
                        apiUrl: '//api.addypin.com',
                            //staticUrl: '//static.acme.com'
                        // antoherCustomVar: ''
                    }
                }
            }
        }
    };
});