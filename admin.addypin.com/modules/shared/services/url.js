angular.module('main').factory('url', function ($timeout, $http, envService){
    return {
        apiUrl: function (url){
            var apiUrl = envService.read('apiUrl');
            return apiUrl + url;
        },
        host: function (h){
            switch(h){
                case 'admin.addypin':
                    return 'development';
                    break;

                case 'admin.addypin.digitalmeetings.net':
                    return 'staging';
                    break;

                case 'admin.addypin.com':
                    return 'production';
                    break;
            }
        }
    };
});