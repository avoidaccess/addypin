angular.module('main.profile').config(function($stateProvider) {
    $stateProvider
        .state('profile.view', {
            url: '/view',
            templateUrl: 'modules/profile/views/profile.html',
            controller: 'ProfileController'
        })
        .state('profile.edit', {
            url: '/edit/:id',
            templateUrl: 'modules/profile/views/profile-edit.html',
            controller: 'ProfileController'
        })
    ;
});