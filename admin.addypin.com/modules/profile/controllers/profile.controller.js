angular.module('main.profile').controller('ProfileController', ['$scope', '$rootScope', '$location', '$timeout', '$stateParams', 'DTOptionsBuilder', 'DTColumnBuilder', 'ProfileService', 'jwtHelper', 'url', 'envService','$state', function ($scope, $rootScope, $location, $timeout, $stateParams, DTOptionsBuilder, DTColumnBuilder, ProfileService, jwtHelper, url, envService,$state) {
    //envService.set('development');
	envService.set(url.host($location.host()));
	var token = localStorage.getItem('admin_token');

    ProfileService.GetUserName().then(function (data){
        $scope.admin_user = data.admin_user[0];
		console.log($scope.admin_user);
    });
	
	$scope.userUpdated = false;
   
    $scope.updatePassword = function (){
		console.log($scope.admin_user);
		if($scope.admin_user.new_pswd == $scope.admin_user.conf_pswd){
			ProfileService.SavePassword($scope.admin_user).then(function (data){
				if(data.rowsAffected != null && data.rowsAffected != undefined && data.rowsAffected > 0){
					$scope.messageClass = "success";
					$scope.message = "Success! The Password was updated successfully.";
					$scope.userUpdated = true;
					console.log('Saved');
				}
				else{
					$scope.messageClass = "danger";
					$scope.message = "Error! The old password is incorrect.";
					$scope.userUpdated = true;
				}
			});
		}
		else {
			$scope.messageClass = "warning";
			$scope.message = "Error! Password did not match.";
            $scope.userUpdated = true;
		}
	}
}]);