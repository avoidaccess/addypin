angular.module('main.dashboard').config(function($stateProvider, $httpProvider, jwtInterceptorProvider) {
    $stateProvider
        .state('dashboard.index', {
            url: '/main',
            templateUrl: 'modules/dashboard/views/dashboard.html',
            //controller: 'DashboardController'
        })
    ;
    /*jwtInterceptorProvider.tokenGetter = ['common', function(common) {
        return localStorage.getItem('token');
    }];

    $httpProvider.interceptors.push('jwtInterceptor');*/
});