angular.module('main.dashboard').controller('DashboardController', ['$scope', '$rootScope', '$location', '$timeout', '$stateParams', 'UserService', 'PinService', 'envService', 'url', function ($scope, $rootScope, $location, $timeout, $stateParams, UserService, PinService, envService, url) {
    //envService.set('development');
	envService.set(url.host($location.host()));
    // Active Users Count
    UserService.GetTotalUsersCount().then(function (data){
        //console.log(data.usersCount[0].TotalUsers);
        var totalUsers = data.usersCount[0].TotalUsers;
        $scope.ActiveUsers = totalUsers;
        $scope.ActiveUsersMessage = totalUsers == 1 ? "Active User" : "Active Users";
    });

    // Individual Users Count
    UserService.GetTotalIndividualUsersCount().then(function (data){
        //console.log(data.usersCount[0].TotalUsers);
        var totalUsers = data.usersCount[0].TotalUsers;
        $scope.IndividualUsers = totalUsers;
        $scope.IndividualUsersMessage = totalUsers == 1 ? "Individual User" : "Individual Users";
    });

    // Corporate Users Count
    UserService.GetTotalCorporateUsersCount().then(function (data){
        //console.log(data.usersCount[0].TotalUsers);
        var totalUsers = data.usersCount[0].TotalUsers;
        $scope.CorporateUsers = totalUsers;
        $scope.CorporateUsersMessage = totalUsers == 1 ? "Corporate User" : "Corporate Users";
    });

    // Latest Pin
    PinService.GetLatestPin().then(function (data){
        //console.log(data.pin[0].date_created);
        $scope.pin_id = data.pin[0].pin_id;
        $scope.LatestPin = new Date(Date.parse(data.pin[0].date_created));
    });

    // Identify the currently loaded path
    var currentPath = $location.path();
    var parts = currentPath.substring(1).split('/');

    // By Default Initialize with Dashboard being active link
    $scope.navClass = {
        dashboard: parts[0] == "dashboard" ? "active" : "",
        users: parts[0] == "users" ? "active" : "",
        pins: parts[0] == "pins" ? "active" : ""
    };

    $scope.setActiveNavLink = function (item){
        /*console.log($location.path());
        console.log($scope.navClass);

        console.log(parts);

        switch(item){
            case 'dashboard':
                $scope.navClass.dashboard = "active";
                $scope.navClass.users = "";
                $scope.navClass.pins = "";
            break;

            case 'users':
                $scope.navClass.dashboard = "";
                $scope.navClass.users = "active";
                $scope.navClass.pins = "";
            break;

            case 'pins':
                $scope.navClass.dashboard = "";
                $scope.navClass.users = "";
                $scope.navClass.pins = "active";
            break;
        }*/
    }
	$scope.logout = function (){
        localStorage.removeItem('admin_token');
        //$state.go('main');
        //window.location = "/#/login";
    }
}]);