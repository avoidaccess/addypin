angular.module('main.pins').controller('PinsController', ['$scope', '$rootScope', '$location', '$timeout', '$stateParams', 'DTOptionsBuilder', 'DTColumnBuilder','PinService', 'url', 'envService','$state', function ($scope, $rootScope, $location, $timeout, $stateParams, DTOptionsBuilder, DTColumnBuilder,PinService, url, envService,$state) {
    //envService.set('development');
	envService.set(url.host($location.host()));

    var loc = $location.path();
    $scope.loc = loc.substring(loc.lastIndexOf("/") + 1, loc.length);
   // alert($scope.loc); 

    var current_url = '/pins/get/all?api_key=' + localStorage.getItem('admin_token') + '&is_deleted=0&user_id='+$scope.loc;
    


    $scope.dtOptions = DTOptionsBuilder.fromSource(url.apiUrl(current_url))
    .withOption('stateSave', true)
        .withPaginationType('full_numbers').withDataProp('pins');
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('code').withTitle('Code'),
        DTColumnBuilder.newColumn('pin_type').withTitle('Pin Type'),
		DTColumnBuilder.newColumn('name').withTitle('Name'),
        DTColumnBuilder.newColumn('lat').withTitle('Latitude'),
        DTColumnBuilder.newColumn('long').withTitle('Longitude'),
        DTColumnBuilder.newColumn('address').withTitle('Address'),
        DTColumnBuilder.newColumn('landmark').withTitle('Landmark'),
        DTColumnBuilder.newColumn('date_created').withTitle('Created On'),
		DTColumnBuilder.newColumn(null).withTitle('Actions').notSortable()
                .renderWith(actionsHtml)
    ];
	function actionsHtml(data, type, full, meta) {
        console.log(data);
        var pin_id = data.pin_id;

        return '<a href="/#/pins/edit/' + pin_id + '" class="btn btn-warning pull-left ">' +
            '   <i class="fa fa-edit"></i>' +
            '</a>&nbsp;' +
            '<a href="/#/pins/delete/' + pin_id + '" class="btn btn-danger">' +
            '   <i class="fa fa-trash-o"></i>' +
            '</a>';
    }
	$scope.pinUpdated = false;
	var id = $stateParams.id;
    var editMode = id != undefined && parseInt(id) > 0;
	if(editMode){
        $rootScope.is_edit = 1;
        $scope.pin_id = id;
        PinService.GetPinById(id).then(function (data){
            $scope.pinData = data.pin[0];
			console.log($scope.pinData);
        });
		$scope.savePin = function (){
			console.log($scope.pinData);
			PinService.SavePin($scope.pinData).then(function (data){
            if(data.updated){
                $scope.messageClass = "success";
                $scope.message = "Success! The Pin was Updated successfully.";
                $scope.pinUpdated = true;
				console.log('Saved');
            }
            else{
                $scope.messageClass = "danger";
                $scope.message = "Error! There was an error. Please try again in a moment.";
                $scope.pinUpdated = true;
            }
        });
        }
    }else{

         if($rootScope.is_edit == 1){
            $rootScope.is_edit = 0;
           location.reload(); 
          // $route.reload();
        }
        $rootScope.is_edit = 0;
    }
	
	$scope.showDelete = false;
	var pid = $stateParams.pid;
	var deleteMode = pid != undefined && parseInt(pid) > 0;

    if(deleteMode){
        $scope.pin_id = pid;
		$scope.showDelete = true;
	}
	$scope.cancel = function(){
		$state.go('pins.all');
	}
	$scope.deleted = function(pin_id){
		PinService.DeletePin(pin_id).then(function (data){
			console.log('Deleted');
			$scope.showDelete = false;
			$state.go('pins.all');
			});
	}
}]);