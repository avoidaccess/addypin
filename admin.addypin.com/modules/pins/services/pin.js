angular.module('main').factory('PinService', function ($timeout, common, $http, url){
    return {
        GetLatestPin: function (){
            var promise = $http({ url: url.apiUrl('/pin/latest'), method: 'GET',})
                .then(function (response){
                    return response.data;
                });
            return promise;
        },
		GetPinById: function (pin_id){
            var promise = $http({ url: url.apiUrl('/pin/by/id/' + pin_id), method: 'GET',})
                .then(function (response){
				  console.log(response.data);
                    return response.data;
                });
            return promise;
        },
		SavePin: function (requestData){
            var promise = $http({ url: url.apiUrl('/pin/update-pin'), method: 'POST', data: requestData, })
                .then(function (response){
					return response.data;
					
                });
            return promise;
        },
		DeletePin: function (pin_id){
            var promise = $http({ url: url.apiUrl('/pin/delete/' + pin_id), method: 'GET' })
                .then(function (response){
                    //console.log(response.data);
					return response.data;
					
                });
            return promise;
        }

    };
});