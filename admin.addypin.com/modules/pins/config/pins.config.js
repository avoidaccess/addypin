angular.module('main.pins').config(function($stateProvider) {
    $stateProvider
        .state('pins.all', {
            url: '/all',
            templateUrl: 'modules/pins/views/pins.html',
            controller: 'PinsController'
        })
        .state('pins.view', {
            url: '/view/:id',
            templateUrl: 'modules/pins/views/pin.html',
            controller: 'PinsController'
        })
        .state('pins.edit', {
            url: '/edit/:id',
            templateUrl: 'modules/pins/views/pin-edit.html',
            controller: 'PinsController'
        })
		.state('pins.delete', {
            url: '/delete/:pid',
            templateUrl: 'modules/pins/views/pins.html',
            controller: 'PinsController'
        })
        .state('pins.users', {
           url: '/userPins/:uid',
           templateUrl: 'modules/pins/views/pins.html',
           controller: 'PinsController'
       })
    ;
});