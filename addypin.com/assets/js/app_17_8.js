angular.module('main', ['main.pin', 'main.lookup', 'ui.router', 'angular-jwt', 'httpPostFix', 'environment', 'ng.deviceDetector','720kb.socialshare'])
.run(['$rootScope', '$state','$http','url', function($rootScope, $state,$http,url) {
        
    $rootScope.showFeedback = false;
    $rootScope.submitText = "Submit";
    $rootScope.submittingFeedback = false;
    $rootScope.page_title = "Welcome to addypin – Get your e-address now!"
    
    // $rootScope.email_address = '';
    // $rootScope.text = '';

    $rootScope.slideToggle = function(){
        $rootScope.showFeedback = !$rootScope.showFeedback;
    }


    $rootScope.email_address = '';
    $rootScope.text = '';
    $rootScope.feedbackSuccess = false;
    

    $rootScope.sendContactUs = function (email, text){

	    $rootScope.submitText = "Submitting ...";
        $rootScope.submittingFeedback = true;
        $rootScope.emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/  
        ///^[a-z]+[a-z0-9._]+@[a-z]+.[a-z.]{2,5}$/ ; 
        // $scope.$apply()
        $http({ url: url.apiUrl('/contact_us'), method: 'POST', data: { email: email, text: text } })
                .then(function (response){
                    if (response.data[0].status == "sent"){
						
					   $('#feedbackForm')[0].reset();
	                   
					    $rootScope.feedbackSuccess = true;
                        $rootScope.submitText = "Submit";
                        $rootScope.submittingFeedback = true;
						
						
						setTimeout(function() { 
							$('#feedbackSuccess').hide();
					   }, 3000);
						
                        //alert("Feedback Submitted Successfully!")
                        // $rootScope.slideToggle();
                    }else{
                        $rootScope.submitText = "Submit";
                        $rootScope.submittingFeedback = true;
                        alert("There is some issue")
                    }
                    return response.data;
        }).then(function(){
            $rootScope.submitText = "Submit";
            $rootScope.submittingFeedback = true;   
        });
    };

    

}]).factory('httpInterceptor', function ($q, $rootScope, $injector) {
    return {
        request: function (config) {
            angular.element( document.querySelector( '.cssload-loader' ) ).removeClass('hide');
            return config || $q.when(config)
        },
        response: function (response) {
            angular.element( document.querySelector( '.cssload-loader' ) ).addClass('hide');
            return response || $q.when(response);
        },
        responseError: function (response) {
            angular.element( document.querySelector( '.cssload-loader' ) ).addClass('hide');
            if (response.status === 401) {
                console.log('401 Occurred');
                $injector.get('$state').transitionTo('main', { 'e': 1 });
            }
            return $q.reject(response);
        }
    };
})
.config(function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, jwtInterceptorProvider, /*common,*/ envServiceProvider/*, envService*/) {
    $stateProvider
    .state('main', {
        url: '/home',
        templateUrl: 'modules/shared/views/home-layout.html',
        controller: 'HomeController'
    })
    .state('register-continue', {
                //url: '/register-continue/:email/:type',
                url: '/register-continue/:type',
                //templateUrl: 'modules/home/views/register-continue.html',
                templateUrl: 'modules/home/views/register-continue-new.html',
                controller: 'UserController'
            })
    .state('terms', {
        url: '/terms',
                //templateUrl: 'modules/shared/views/dashboard.html',
                templateUrl: 'modules/shared/views/terms-layout.html',
                controller: 'TermsController'
            })
    .state('privacy', {
        url: '/privacy',
                //templateUrl: 'modules/shared/views/dashboard.html',
                templateUrl: 'modules/shared/views/privacy-layout.html',
                controller: 'PrivacyController'
            })
    .state('verify', {
        url: '/verify/:token/:id',
                //templateUrl: 'modules/shared/views/dashboard.html',
                templateUrl: 'modules/shared/views/loading-layout.html',
                controller: 'AuthController'
            })
    
    .state('verify-pin', {
        url: '/verify-pin/:token/:id/:guid/:pid',
                //templateUrl: 'modules/shared/views/dashboard.html',
                templateUrl: 'modules/shared/views/loading-layout.html',
                controller: 'AuthController'
            })
    .state('delete-pin', {
        url: '/delete-pin/:token/:id/:guid/:pid/:code',
                //templateUrl: 'modules/shared/views/dashboard.html',
                templateUrl: 'modules/shared/views/loading-layout.html',
                controller: 'AuthController'
            }) 
    .state('lookup', {
        url: '/lookup',
                //template: '<div ui-view></div>',
                templateUrl: 'modules/shared/views/layout.html',
                //controller: 'LookupController'
            })
    .state('pin', {
        url: '/pin',
                //template: '<div ui-view></div>',
                templateUrl: 'modules/shared/views/layout.html',
                //controller: 'PinController'
            });
    $urlRouterProvider.otherwise('/home');

    jwtInterceptorProvider.tokenGetter = ['common', function(common) {
        return localStorage.getItem('token');
    }];

    $httpProvider.interceptors.push('jwtInterceptor');
    $httpProvider.interceptors.push('httpInterceptor');
    envServiceProvider.config({
        domains: {
            development: ['admin.addypin'],
            staging: ['admin.addypin.digitalmeetings.net'],
            production: ['admin.addypin.com']
                // anotherStage: []
            },
            vars: {
                development: {
                    apiUrl: '//api.addypin/',
                    subdomainUrl: '.addypin',
                    //staticUrl: '//localhost/static'
                    // antoherCustomVar: ''
                },
                staging: {
                    apiUrl: '//api.addypin.digitalmeetings.net',
                    subdomainUrl: '.addypin.digitalmeetings.net',
                },
                production: {
                    apiUrl: '//api.addypin.com',
                    subdomainUrl: '.addypin.com',
                    //staticUrl: '//static.acme.com'
                    // antoherCustomVar: ''
                }
            }
        });
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.withCredentials = true;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
        //common.environmentConfig();
        //envService.set('development');
    })
.controller('HomeController', ['$scope', '$rootScope', '$location', '$timeout', '$stateParams', '$state', 'deviceDetector', 'HomeService', 'url', 'envService','$http', function ($scope, $rootScope, $location, $timeout, $stateParams, $state, deviceDetector, HomeService, url, envService,$http) {
    envService.set(url.host($location.host()));
    $scope.user = { type: "personal" };
    $scope.msg = $location.search().msg;
    $scope.email = $location.search().email;
    $scope.website = $location.search().web;
    $rootScope.page_title = "Welcome to addypin – Get your e-address now!"
   
  if($location.search().msg != '' && typeof($location.search().msg) != "undefined"){
	   setTimeout(function() { 
	    $scope.msg = '';
		window.location.href = 'http://addypin.com/#/home';
	   }, 10000);
   }

    $scope.registerRequestConfirmationMessage = "";
    $scope.register = function (pintype){
            /*HomeService.SendRegisterRequest($scope.user.email, $scope.user.type).then(function (data) {
                $scope.registerRequestConfirmationMessage = "An Email has been sent to " + $scope.user.email + " with instructions. Please check your inbox for more details.";
            });*/
        //console.log(teter+" " +$scope.teter + " scope user ");
        //console.log($scope.user);
        $state.go('register-continue', { type: pintype });
    };
    window.dv = deviceDetector;
    // setTimeout(function(){$('.alert-msg').addClass('ng-hide')},6000);

    console.log(deviceDetector);
}])
.controller('TermsController', ['$scope', '$rootScope', '$location', '$timeout', '$stateParams', 'url', 'envService', function ($scope, $rootScope, $location, $timeout, $stateParams, url, envService) {
    envService.set(url.host($location.host()));
}])
.controller('PrivacyController', ['$scope', '$rootScope', '$location', '$timeout', '$stateParams', 'url', 'envService', function ($scope, $rootScope, $location, $timeout, $stateParams, url, envService) {
    envService.set(url.host($location.host()));
}])
.controller('UserController', ['$scope', '$rootScope', '$location', '$timeout', '$stateParams', 'HomeService', '$state', 'jwtHelper', 'url', 'envService', 'PinService','$window', function ($scope, $rootScope, $location, $timeout, $stateParams, HomeService, $state, jwtHelper, url, envService, PinService, $window) {
    envService.set(url.host($location.host()));
    var guid = $stateParams.guid;
    var email = $stateParams.email;
    var type = $stateParams.type;
    
    
    var path = $location.path();
    $scope.type = type == "corporate" ? "business" : "personal";
    $scope.email = email;
    $scope.isFormOpen = false;
    $scope.isPinCodeValid = false;
    $scope.buttonClass = "disabled";
    $scope.nameCaption = type == "corporate" ? "Business Name" : "Full Name";
    $scope.nameCaptionHolder = type == "corporate" ? "\"Company Pvt. Ltd\"" : "\"Mark Anthony\"";
    $scope.typeFormatted = type == "corporate" ? "Business" : "Personal";
    $scope.showFooter = true;
    $scope.mapToolTip = 'No GPS: Not here? Enable your GPS and reload the Page'
    $scope.isGpsFound = false;


    

    $scope.reload = function(){
        $window.location.reload();
    }

    $scope.renderHTML = function(html_code)
        {

            var decoded = angular.element('<textarea />').html(html_code).text();
            return $sce.trustAsHtml(decoded);
        };

    $scope.suggestions = { pin1: '', pin2: '', pin3: '' };
        //console.log(guid + ": " + email + ": " + type);
        //console.log($location);

    $scope.user = {
        address: "",
        name: "",
        location: "",
        postal_code: "",
        landmark: "",
        code: "",
        lat: 0,
        is_gps: 0,
        lng: 0,
        email: email,
        type: type
    };



    $scope.VerifyPincodeAvailability = function (usercode){
        $('.pin-validator').first().children().first().removeClass('fa-check-circle-o');
        $('.pin-validator').first().children().first().addClass('fa-times-circle-o');
        $('.pin-validator').first().addClass('disabled');
        $scope.buttonClass = "disabled";

        //if($scope.user.code != null && $scope.user.code != undefined && $scope.user.code != '' && $scope.user.code.length == 6){
            if(usercode != null && usercode != undefined && usercode != '' && usercode.length == 6){
                
                $scope.user.code = usercode.toUpperCase();
                console.log($scope.user.code);
                console.log(usercode)
                console.log($scope.user.code.length);
                $('#duplicate-pin').hide();
                $scope.buttonClass = "disabled";
                HomeService.PincodeAvailability($scope.user.code,0).then(function (data){
                    $scope.isPinCodeValid = data.available;
                    if(!data.available && data.valid){
                    //alert("The pincode " + $scope.user.code + " is not available. Please try a different one.");
                    $('#duplicate-pin').show();
                }
                else
                    $('#duplicate-pin').hide();
                //$scope.buttonClass = (data.available && data.valid) ? "" : "disabled";
                if(data.available && data.valid){
                    $scope.buttonClass = "enabled";
                    $('.pin-validator').first().children().first().removeClass('fa-times-circle-o');
                    $('.pin-validator').first().removeClass('disabled');
                    $('.pin-validator').first().children().first().addClass('fa-check-circle-o');
                }
            });
            }else if(usercode != null && usercode != undefined && usercode != ''){
                $scope.user.code = usercode.toUpperCase();
                console.log($scope.user.code);
                console.log(usercode)

                console.log($scope.user.code.length);
                $('#duplicate-pin').hide();
                $scope.buttonClass = "disabled";
                HomeService.PincodeAvailability($scope.user.code,0).then(function (data){
                    $scope.isPinCodeValid = data.available;
                    if(!data.available && data.valid){
                    //alert("The pincode " + $scope.user.code + " is not available. Please try a different one.");
                    $('#duplicate-pin').show();
                }
                else
                    $('#duplicate-pin').hide();
                //$scope.buttonClass = (data.available && data.valid) ? "" : "disabled";
                if(data.available && data.valid){
                    /*$('.pin-validator').first().children().first().removeClass('fa-times-circle-o');
                    $('.pin-validator').first().children().first().removeClass('disabled');
                    $('.pin-validator').first().children().first().addClass('fa-check-circle-o');*/
                }
            });
            }
            
        };
        
        $scope.VerifyLatLng = function (){
        /*if($scope.user.lat != null && $scope.user.lat != 0 && $scope.user.lat != '' && $scope.user.lng != null && $scope.user.lng != 0 && $scope.user.lng != ''){
            // This means both lat / lng are available. Send map request to google.
            var myLatLng = {lat: parseFloat($scope.user.lat), lng: parseFloat($scope.user.lng)};
            var map = new google.maps.Map(document.getElementById('map-container'), {
                center: myLatLng,
                zoom: 15
            });
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map
                //title: 'Hello World!'
            });
            console.log($scope.user);
        }*/
    };
    $scope.InactivePins = false;
    
    $scope.RegisterPin = function (){

        $scope.buttonClass = "disabled";
        $scope.user.mode = $scope.isGpsFound == false ? "ip" : "gps";
		/*console.log('----');
		console.log($scope.user); return false;*/
		if($scope.user.mode == 'ip'){
		  $scope.user.address = $('#address').val();	
		  $scope.user.lat = $('#lat').val();
		  $scope.user.lng = $('#lng').val();
		}
		 
	
	
        HomeService.RegisterPin($scope.user).then(function (data) {
            console.log($scope.user);
            console.log(data);
			
            myuser = $scope.user.code;
            if(data.length > 0 && data[0].status == "sent"){
                $scope.messageClass = "success";
                $scope.message = "New addypin created, check your email";

                $scope.user = {
                    address: "",
                    name: "",
                    location: "",
                    postal_code: "",
                    landmark: "",
                    code: "",
                    lat: 0,
                    is_gps: 0,
                    lng: 0,
                    email: '',
                    type: ''
                };
                $scope.InactivePins = true;
                $location.path('/home').search({email: myuser+"@addypin.com",   
                                                web: myuser+".addypin.com",
                                                msg:"Congratulations! Your addypin "+myuser+".addypin.com is valid for 7 days without confirmation. We’ve emailed you the same on "+data[0].email});
            }
			 else{
                $scope.messageClass = "danger";
                $scope.message = "You have exceeded maximum of 5 personal addypins.";
                $scope.buttonClass = "";
                //$scope.message = "Error! Personal accounts can only have 5 pins.";
            }
            $scope.InactivePins = true;
        }).then(function(error){
            $scope.buttonClass = "";
        });
    };

    $scope.ToggleRegisterForm = function (){
        $scope.isFormOpen = $scope.isFormOpen == false;
    };

    $scope.SetCoordinates = function (lat, lng){
        $scope.user.lat = lat;
        $scope.user.lng = lng;
        console.log($scope.user);
    };
    $scope.getSuggestions = function (){
        email = $scope.user.email;
        console.log(email);
        if(typeof(email)!="undefined" && email.length >=3 ){
            PinService.GetPinSuggestions(email).then(function (data){
                $scope.suggestions.pin1 = data[0].toUpperCase();
                $scope.suggestions.pin2 = data[1].toUpperCase();
                $scope.suggestions.pin3 = data[2].toUpperCase();
                if($scope.user.code == "")
                    $scope.setCode(data[0].toUpperCase());
                //$scope.user.code = data[0].toUpperCase();
                console.log($scope.suggestions);
            });
        }
    }
    
    $scope.setCode = function (code){
        $scope.user.code = code;
        $scope.VerifyPincodeAvailability(code);
    };
    
    $scope.onGeoSuccess = function (location){
        $scope.user.lat = location.coords.latitude;
        $scope.user.lng = location.coords.longitude;
        console.log($scope.user);
        console.log(location);
    };
    $scope.onGeoError = function (error){
        console.log(error);
    };

    if(path.search('register-continue') != -1){
            $scope.showFooter = false;
            console.log($scope.showFooter);
            var html5Options = { enableHighAccuracy: true, timeout: 6000, maximumAge: 0 },
            ipFallbackIndex = /*chkFallback.checked ? cmbSource.selectedIndex :*/ -1;
            //geolocator.locate($scope.onGeoSuccess, $scope.onGeoError, ipFallbackIndex, html5Options, 'map-container');
            $(function(){
                var mylat = 31.544301306672725;
                var mylon = 74.34968769702152;
                var userdata;
                function getLocation(){

                    locallat = window.mylat;
                    locallon = window.mylon;
                    var geooptions = {
                      enableHighAccuracy: true,
                      timeout: 5000,
                      maximumAge: 0
                  };
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(locationsuccess,locationerr,geooptions);
                } 
                else{
                    console.log(locallat +"-"+  locallon);
                    initmap(locallat,locallon);
                }
                
            }

            function locationsuccess(position){
                //alert(position)
                $scope.isGpsFound = true;
                if(!position.coords){
                    finallat = locallat;
                    finallon = locallon;
                    initmap(finallat,finallon,16);
                }
                else{
                    finallat = position.coords.latitude;
                    finallon = position.coords.longitude;
                    initmap(finallat,finallon,16);
                }
                            /*locallat = position.coords.latitude;
                            locallon = position.coords.longitude;*/
                            
            }

            function locationerr(err){
                $scope.isGpsFound = false;

                $.getJSON('//freegeoip.net/json/?callback=?', function(data) {
                    window.userdata = data;
                    initmap(window.userdata.latitude,window.userdata.longitude,19);
                  });
                    //console.log("user data" + window.userdata)
            }
            var component = null;

                function initmap(finallat,finallon, zoom){
                    if (!zoom) {zoom = 19; }
                    console.log(finallat +" - "+ finallon);
                    $('#map-container').locationpicker({
                        location: {latitude: finallat, longitude: finallon},
                        radius: 0,
                        zoom: zoom,
                        
                        enableAutocomplete: true,
                        markerInCenter: true,
                        inputBinding:{
                            latitudeInput: $('#latitude'),
                            longitudeInput: $('#longitude'),
                            locationNameInput: $('#address')
                        },
                        onchanged: function (currentLocation, radius, isMarkerDropped) {
                            var addressComponents = $(this).locationpicker('map').location.addressComponents;
                            addressComponents.formattedAddress = $(this).locationpicker('map').location.formattedAddress;
                            updateControls(addressComponents,currentLocation);
                        },
                        oninitialized: function(component) {
                            var addressComponents = $(component).locationpicker('map').location.addressComponents;
                             component = component;       
                            //console.log($(component).locationpicker('map').location)
                            var currentLocation = {latitude : $(component).locationpicker('map').location.latitude, longitude:$(component).locationpicker('map').location.longitude};
                            addressComponents.formattedAddress = $(component).locationpicker('map').location.formattedAddress;
                            updateControls(addressComponents,currentLocation);
                            /*$('#latitude').val(finallat);
                            $('#longitude').val(finallon);*/
                            }
                        });
                }

                function updateControls(addressComponents,currentLocation) {
                   
                    $scope.user.address = addressComponents.formattedAddress;

                   if(currentLocation){
                        $scope.user.lat = currentLocation.latitude;
                        $scope.user.lng = currentLocation.longitude;
                     $('#lat').val(currentLocation.latitude);
					  $('#lng').val(currentLocation.longitude);
					    $scope.user.is_gps = 1;
                       // console.log('map started')
                       // console.log(addressComponents);
                    }
                    else{
                        $.getJSON('//freegeoip.net/json/?callback=?', function(data) {
                            $scope.user.lat = data.latitude;
                            $scope.user.lng = data.longitude;    
                            /*console.log('map started non gps')
                            console.log($scope.user); */
                          //window.userdata = data;
                          //console.log("user data 1 " + typeof(window.userdata))
                            //initmap(window.userdata.latitude,window.userdata.longitude);
                        });
                    }
                }

                getLocation();
            });

PinService.GetPinSuggestions($scope.user.email).then(function (data){
    $scope.suggestions.pin1 = data[0].toUpperCase();
    $scope.suggestions.pin2 = data[1].toUpperCase();
    $scope.suggestions.pin3 = data[2].toUpperCase();
                //$scope.user.code = data[0].toUpperCase();
                if($scope.user.code == "")
                    $scope.setCode(data[0].toUpperCase());
                //console.log($scope.suggestions);
            });
}

        /*if(guid != '' && email != '' && type != ''){
            HomeService.RegistrationActivation(guid, email, type).then(function (data) {
                if(data.token != null && data.token != undefined && data.token != "" && !jwtHelper.isTokenExpired(data.token)){
                    // If the token is valid and not expired then save that token in the local storage and redirect the user to the manage pins screen
                    localStorage.setItem('token', data.token);
                    localStorage.setItem('uid', data.uid);
                }
                else{
                    $state.go('main');
                }
            });
        }*/
    }])
.controller('AuthController', ['$scope', '$rootScope', '$location', '$timeout', '$stateParams', 'jwtHelper', '$state', 'envService', 'url', 'HomeService','PinService', function ($scope, $rootScope, $location, $timeout, $stateParams, jwtHelper, $state, envService, url, HomeService,PinService) {
    envService.set(url.host($location.host()));

        // Can be two possibilities.
        // 1) Verify Pin
        // 2) Verify
        

        var path = $location.path();
		
		if(path.search('verify-pin') != -1){
            var _token = $stateParams.token;
            var uid = $stateParams.id;
            var guid = $stateParams.guid;
            var pid = $stateParams.pid;
            if(_token != null && _token != undefined && _token != "" && !jwtHelper.isTokenExpired(_token)){

               	
                HomeService.PinActivation(uid, guid, pid).then(function (data){
					//alert(data.code);
					//console.log(data.code);  return false;
					//alert(json.stringify(data)); return false;
					//$state.go('pin.mypins');
                   $state.go('pin.mypins',{'any' : data.code});
				   
                });
                localStorage.setItem('token', _token);
                localStorage.setItem('uid', uid);
            }
            else
               $state.go('pin.mypins');
				//$state.go('terms');
        }

        if(path.search('delete-pin') != -1){

            var _token = $stateParams.token;
            var uid = $stateParams.id;
            var guid = $stateParams.guid;
            var pid = $stateParams.pid;
            var code = $stateParams.code;
            if(_token != null && _token != undefined && _token != "" && !jwtHelper.isTokenExpired(_token)){
                 HomeService.PinActivation(uid, guid, pid).then(function (data){
                    
                    localStorage.setItem('token', _token);
                    localStorage.setItem('uid', uid);
                    PinService.DeletePin(pid).then(function (data){
                        $location.path('/home').search({
                            msg:"Sorry for the inconvenience, addypin "+code+" has  been deleted"
                        });
                    });
                    
                    
                })
            }
            //else{
            //     $location.path('/home').search({
            //                 msg:"Invalid Token"
            //             });
            // }
            
        }
		
        else if(path.search('verify') != -1){
            var _token = $stateParams.token;
            var uid = $stateParams.id;
            console.log(jwtHelper.isTokenExpired(_token));
            if(_token != null && _token != undefined && _token != "" && !jwtHelper.isTokenExpired(_token)){
                // If the token is valid and not expired then save that token in the local storage and redirect the user to the manage pins screen
                localStorage.setItem('token', _token);
                localStorage.setItem('uid', uid);
                 $state.go('pin.mypins');
				
				
            }
        }

    }])
.directive('toggle', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            if (attrs.toggle=="tooltip"){
                $(element).tooltip();
            }
            if (attrs.toggle=="popover"){
                $(element).popover();
            }
        }
    };
})
;

function isAndroid(){
    return navigator.userAgent.match(/Android/i);
}

function isIOS(){
    return (navigator.userAgent.match(/iPhone/i)
 || navigator.userAgent.match(/iPad/i)
 || navigator.userAgent.match(/iPod/i));
}

function isBlackBerry(){
    return navigator.userAgent.match(/BlackBerry/i);
}

function isWindowPhone(){
    return navigator.userAgent.match(/Windows Phone/i);
}



function detectmob() { 
 if( navigator.userAgent.match(/Android/i)
 || navigator.userAgent.match(/webOS/i)
 || navigator.userAgent.match(/iPhone/i)
 || navigator.userAgent.match(/iPad/i)
 || navigator.userAgent.match(/iPod/i)
 || navigator.userAgent.match(/BlackBerry/i)
 || navigator.userAgent.match(/Windows Phone/i)
 ){
    return true;
  }
 else {
    return false;
  }
}