angular.module('main.lookup').config(function($stateProvider) {
    $stateProvider
        .state('lookup.index', {
            url: '/search-form',
            templateUrl: 'modules/lookup/views/form.html',
            controller: 'LookupController'
        })
        .state('lookup.results', {
            url: '/results/:keyword',
            templateUrl: 'modules/lookup/views/results.html',
            controller: 'LookupController'
        })
    ;
});