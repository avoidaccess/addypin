angular.module('main').factory('LookupService', function ($timeout, $http, url){
    return {
        LookupByKeyword: function (keyword){
            var token = localStorage.getItem('token');

            if(keyword != null && keyword != ""){

                // API KEY MUST BE CHECKED HERE

                var promise = $http({ url: url.apiUrl('/lookup/' + keyword + "?api_key=" + token), method: 'GET'})
                    .then(function (response){
                        return response.data;
                    });
                return promise;
            }
        }
    }
});