angular.module('main.lookup').controller('LookupController', ['$scope', '$rootScope', '$location', '$timeout', '$stateParams', 'url', 'envService', 'LookupService', '$state','PinService','HomeService', function ($scope, $rootScope, $location, $timeout, $stateParams, url, envService, LookupService, $state,PinService,HomeService) {
    envService.set(url.host($location.host()));
    var currentPath = $location.path();
    var parts = currentPath.substring(1).split('/');

    //console.log($stateParams);

    $scope.keyword = "";
    $scope.results = null;
    $scope.resultsTotal = 0;
    $scope.loadingMessage = "Searching...";
    $scope.pinLink = "";
    $scope.pinLinkDisplay = "";
    $rootScope.page_title = "addypin Lookup PINs"

    switch(parts[1]){

        case 'results':
            LookupService.LookupByKeyword($stateParams.keyword).then(function (data){
                $scope.results = data.pins;
                $scope.resultsTotal = data.rowsCount;
                if(data.rowsCount > 0){
                    $scope.loadingMessage = "Great! We have " + data.rowsCount + " result(s)";
                    //$scope.pinLink = "http://" + data.pins[0].code + ".addypin.co";
                    $scope.pinLink = url.subdomainUrl(data.pins[0].code);
                    $scope.pinLinkDisplay = url.subdomainUrlDisplay(data.pins[0].code);
                }
                else if(data.limit_reached){
                    $scope.loadingMessage = data.message;
                }
                else{
                    //$scope.loadingMessage = "No results found for \"" + $stateParams.keyword + "\"";
                    $scope.loadingMessage = "Sorry Nomad, no addypins found under \"" + $stateParams.keyword + "\""; 
                }
            });
        break;
    }

    function GetMyPins(){
        PinService.GetPinsByUserId().then(function (data){
            $scope.results = data.pins;
            $scope.totalPins = data.pins.length;

            $scope.userType = data.user.user_type_id == 1 ? "personal" : "business";
            $scope.totalPinsLimit = data.user.user_type_id == 1 ? 5 : 20;

            //console.log(data.pins);
        });
    }

    $scope.share = function (){
        PinService.SharePin($scope.currentPin.code, $scope.shareEmail).then(function (data){
            if(data.status == "shared"){
                $scope.messageClass = "success";
                $scope.message = "addypin shared";
                $scope.responseReturned = true;
                $scope.shareEmail = "";
            }
            else{
                $scope.messageClass = "danger";
                $scope.message = "Error! There was an error. Please try again in a moment.";
                $scope.responseReturned = false;
            }
            $scope.shareShown = false;
        });
    }

    $scope.showEdit = function (pin){
        $scope.currentPin = pin;
        //console.log(pin);
        $scope.editShown = true;
        
        //$(function(){

        $scope.getLocation($scope.currentPin.lat,$scope.currentPin.long);
        //});
    };

    $scope.lookup = function (){

	if (typeof($scope.keyword) != "undefined"){
		$state.go('lookup.results', {keyword: $scope.keyword});	
	}
	

    };

    $scope.hideEdit = function (){
        $scope.currentPin = null;
        $scope.editShown = false;
    };

    $scope.showDelete = function (pin){
        $scope.currentPin = pin;
        $scope.deleteShown = true;
    };

    $scope.hideDelete = function (){
        //$scope.currentPin = null;
        $scope.deleteShown = false;
    };

    $scope.confirmDelete = function (){
        PinService.DeletePin($scope.currentPin.pin_id).then(function (data){
            //$scope.pins = data.pins;
            GetMyPins();
            if(data.rowsAffected != null && data.rowsAffected != undefined && data.rowsAffected > 0){
                $scope.messageClass = "success";
                $scope.message = "addypin deleted";
                $scope.responseReturned = true;
            }
            else{
                $scope.messageClass = "danger";
                $scope.message = "Error! There was an error. Please try again in a moment.";
                $scope.responseReturned = true;
            }
        });

        $scope.editShown = false;
        $scope.deleteShown = false;
    };

    $scope.UpdatePin = function (){
        console.log($scope.currentPin);
        HomeService.PincodeAvailability($scope.currentPin.code,$scope.currentPin.pin_id).then(function (data){
            if(!data.available){
                $('#duplicate-pin').show();
                return false;
            }
            else{
                $('#duplicate-pin').hide();
                $scope.currentPin.code = $scope.currentPin.code.toUpperCase();
                PinService.UpdatePin($scope.currentPin).then(function (data) {
                if(data.updated){
                    $scope.messageClass = "success";
                    $scope.message = "addypin updated";
                    $scope.responseReturned = true;
                }
                else{
                    $scope.messageClass = "danger";
                    $scope.message = "Error! There was an error. Please try again in a moment.";
                    $scope.responseReturned = true;
                }
                $scope.currentPin = null;
                $scope.editShown = false;
            });
            }
        });
        
    };

    $scope.showShare = function (pin){
        $scope.currentPin = pin;
        $scope.shareEmail = "";
        $scope.shareShown = true;
        
    };

    $scope.hideShare = function (){
        //$scope.currentPin = null;
        $scope.shareShown = false;
    };

    $scope.confirmShare = function (){

    };

    var mylat = 31.544301306672725;
    var mylon = 74.34968769702152;
    var userdata;
    $scope.getLocation = function(lat,lng){
        locallat = lat;
        locallon = lng;
        var geooptions = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };
/*        if (navigator.geolocation) {
            //navigator.geolocation.getCurrentPosition(locationsuccess,locationerr,geooptions);
            console.log(locallat +"-"+  locallon);
            initmap(locallat,locallon);
        } 
        else{
*/            
            $scope.initmap(lat,lng);
        /*}*/

    }

    $scope.initmap = function (finallat,finallon){
        $('#map-container').locationpicker({
            location: {latitude: finallat, longitude: finallon},
            radius: 0,

            enableAutocomplete: true,
            markerInCenter: true,
            inputBinding:{
                latitudeInput: $('#latitude'),
                longitudeInput: $('#longitude'),
                locationNameInput: $('#address')
            },
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                var addressComponents = $(this).locationpicker('map').location.addressComponents;
                //console.log(currentLocation);
                console.log($(this).locationpicker('map').location)
                formattedAddress = $(this).locationpicker('map').location.formattedAddress;

                $scope.updateControls(addressComponents,currentLocation,formattedAddress);
            },
            oninitialized: function(component) {
                    var addressComponents = $(component).locationpicker('map').location.addressComponents;
                    console.log($(component).locationpicker('map').location)
                    var currentLocation = {latitude : finallat, longitude:finallon};
                    addressComponents.formattedAddress = $(component).locationpicker('map').location.formattedAddress;
                    $scope.updateControls(addressComponents,currentLocation,addressComponents.formattedAddress);
                    /*$('#latitude').val(finallat);
                    $('#longitude').val(finallon);*/
                }
            });
    }

    $scope.updateControls = function(addressComponents,currentLocation,formattedAddress) {
        /*                    console.log('addressComponents');
        console.log(addressComponents);
        */
        $scope.currentPin.address = formattedAddress;
        console.log(formattedAddress)

        if(currentLocation){
            $scope.currentPin.lat = currentLocation.latitude;
            $scope.currentPin.long = currentLocation.longitude;
            console.log('before save')
            console.log($scope.currentPin)
        }
        else{
            $.getJSON('//freegeoip.net/json/?callback=?', function(data) {
            $scope.currentPin.lat = data.latitude;
            $scope.currentPin.long = data.longitude;    
            });
         }
    }  

}]);