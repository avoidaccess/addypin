angular.module('main').factory('PinService', function ($timeout, $http, url){
    return {
        LoginUser: function (email){
            var promise = $http({ url: url.apiUrl('/authorize'), method: 'POST', data: email})
            .then(function (response){
                console.log(response);
                return response.data;
            });
            return promise;
        },

        GetPinsByUserId: function (){
            var uid = localStorage.getItem('uid');
            var token = localStorage.getItem('token');

            if(uid != null && uid != "" && parseInt(uid) > 0){

                // API KEY MUST BE CHECKED HERE

                var promise = $http({ url: url.apiUrl('/pins/by/uid/' + uid + "?api_key=" + token), method: 'GET'})
                .then(function (response){
                    return response.data;
                });
                return promise;
            }
        },

        SharePin: function (code, email){
            var token = localStorage.getItem('token');
            var uid = localStorage.getItem('uid');
            var promise = $http({ url: url.apiUrl('/pin/share/' + code + '/' + email + '/' + uid + "?api_key=" + token), method: 'GET'})
            .then(function (response){
                return response.data;
            });
            return promise;
        },

        DeletePin: function (pin_id){
            var token = localStorage.getItem('token');
            var promise = $http({ url: url.apiUrl('/pin/delete/' + pin_id + "?api_key=" + token), method: 'GET'})
            .then(function (response){
                return response.data;
            });
            return promise;
        },
        
		VerifyPin: function (pin_id){
		   var token = localStorage.getItem('token');
           var promise = $http({ url: url.apiUrl('/pin/verify/' + pin_id+ "?api_key=" + token), method: 'GET' })
               .then(function (response){
                   //console.log(response.data);
                   return response.data;
                   
               });
           return promise;
       },
	   
        UpdatePin: function (pin){
            var promise = $http({ url: url.apiUrl('/pin/save-pin'), method: 'POST', data: pin})
            .then(function (response){
                console.log(response);
                return response.data;
            });
            return promise;
        },
        
		
		UpdateAddressPin: function (pin){
            var promise = $http({ url: url.apiUrl('/pin/address/save-pin'), method: 'POST', data: pin})
            .then(function (response){
                console.log(response);
                return response.data;
            });
            return promise;
        },
		
		
        GetPinSuggestions: function (email){
            var promise = $http({ url: url.apiUrl('/code-suggestions/' + email), method: 'GET'})
            .then(function (response){
                console.log(response);
                return response.data;
            });
            return promise;
        },
        VerifyPincodeAvailability : function (usercode){
            $('.pin-validator').first().children().first().removeClass('fa-check-circle-o');
            $('.pin-validator').first().children().first().addClass('fa-times-circle-o');
            $('.pin-validator').first().addClass('disabled');
            $scope.buttonClass = "disabled";

    
            //if($scope.user.code != null && $scope.user.code != undefined && $scope.user.code != '' && $scope.user.code.length == 6){
                if(usercode != null && usercode != undefined && usercode != '' && usercode.length == 6){
                    
                    $scope.currentPin.code = usercode.toUpperCase();
                    console.log($scope.currentPin.code);
                    console.log(usercode)
                    console.log($scope.currentPin.code.length);
                    $('#duplicate-pin').hide();
                    $scope.buttonClass = "disabled";
                    HomeService.PincodeAvailability($scope.currentPin.code,0).then(function (data){
                        $scope.isPinCodeValid = data.available;
                        if(!data.available && data.valid){
                        //alert("The pincode " + $scope.user.code + " is not available. Please try a different one.");
                        $('#duplicate-pin').show();
                    }
                    else
                        $('#duplicate-pin').hide();
                    //$scope.buttonClass = (data.available && data.valid) ? "" : "disabled";
                    if(data.available && data.valid){
                        $scope.buttonClass = "enabled";
                        $('.pin-validator').first().children().first().removeClass('fa-times-circle-o');
                        $('.pin-validator').first().removeClass('disabled');
                        $('.pin-validator').first().children().first().addClass('fa-check-circle-o');
                    }
                });
                }else if(usercode != null && usercode != undefined && usercode != ''){
                    $scope.currentPin.code = usercode.toUpperCase();
                    console.log($scope.currentPin.code);
                    console.log(usercode)

                    console.log($scope.currentPin.code.length);
                    $('#duplicate-pin').hide();
                    $scope.buttonClass = "disabled";
                    HomeService.PincodeAvailability($scope.currentPin.code,0).then(function (data){
                        $scope.isPinCodeValid = data.available;
                        if(!data.available && data.valid){
                        //alert("The pincode " + $scope.user.code + " is not available. Please try a different one.");
                        $('#duplicate-pin').show();
                    }
                    else
                        $('#duplicate-pin').hide();
                    //$scope.buttonClass = (data.available && data.valid) ? "" : "disabled";
                    if(data.available && data.valid){
                        /*$('.pin-validator').first().children().first().removeClass('fa-times-circle-o');
                        $('.pin-validator').first().children().first().removeClass('disabled');
                        $('.pin-validator').first().children().first().addClass('fa-check-circle-o');*/
                    }
                });
                }
                
            }
        };
    });