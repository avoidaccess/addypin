angular.module('main.pin').controller('PinController', ['$scope', '$filter','$rootScope', '$location', '$timeout', '$stateParams', 'url', 'PinService', 'jwtHelper', '$state', 'envService','HomeService', function ($scope, $filter, $rootScope, $location, $timeout, $stateParams, url, PinService, jwtHelper, $state, envService,HomeService) {
   
    
    envService.set(url.host($location.host()));
    var currentPath = $location.path();
    var parts = currentPath.substring(1).split('/');
	
        $scope.shareWhatApps = function(codes){
 
        // var message = encodeURIComponent(text) + " - " + encodeURIComponent(url);

        var message = "Find me here "+codes+".addypin.com or email "+ codes +"@addypin.com"
       
        message =  encodeURIComponent(message);
       // alert (message)
        var whatsapp_url = "whatsapp://send?text=" + message;
       // alert(whatsapp_url);
        window.location.href = whatsapp_url;
    }

	
    var token = localStorage.getItem('token');
    $scope.token = token;
	
	if(parts[2] != "" && parts[2] != undefined){
		$scope.responseReturned = true;
		$scope.message = parts[2]+" is now verified";

       setTimeout(function() {
          $state.go('pin.manage');
        }, 5000);
     
	}else{
    	$scope.responseReturned = false;
		 $scope.message = "";
	}
    $scope.messageClass = "success";
   
    $scope.currentPin = null;
    $scope.totalPins = 0;
    $scope.shareEmail = "";
    $scope.userType = "";
    $scope.totalPinsLimit = 0;
    $rootScope.page_title = "addypin - Manage PINs"

    $scope.countPersonal = 0;
    $scope.countBusiness = 0;



    $rootScope.logout = function(){
        localStorage.removeItem('token');
        $scope.token = '';
        $state.go('pin.manage');
    }

    switch(parts[1]){
        case 'manage-pins':
        if(token != null && token != undefined && !jwtHelper.isTokenExpired(token)) {
            $state.go('pin.mypins');
        }
        break;

        case 'my-pins':
            GetMyPins();
        break;
    }

    $scope.shareWhatApp = function(pin){
        
        // var message = encodeURIComponent(text) + " - " + encodeURIComponent(url);

        var message = "https://"+pin.code+".addypin.com or retrieve map "+ pin.code +"@addypin.com"
        message =  encodeURIComponent(message);
       // alert (message)
        var whatsapp_url = "whatsapp://send?text=" + message;
        window.location.href = whatsapp_url;
    }

    $scope.user = {email: ""};
    $scope.editShown = false;
    $scope.deleteShown = false;
    $scope.login = function (){
        PinService.LoginUser($scope.user).then(function (data){
            if(data[0].status == "sent"){
                $scope.messageClass = "success";
                $scope.message = "Email has been sent";
            }
            else{
                $scope.messageClass = "danger";
                $scope.message = "No addypins associated with this email";
            }
            $scope.responseReturned = true;
        });
    };

      $scope.PersonalAddypin=true;
      $scope.BusinessAddypin=false;
    $scope.showPersonal=function(){
      $scope.PersonalAddypin=true;
      $scope.BusinessAddypin=false;
      $("#personal").removeAttr( "class" );
      $("#buissness").removeAttr( "class" );
      
      $("#personal").addClass("btn btn-primary");
      $("#buissness").addClass("btn btn-default");

       $scope.userType = "personal"; 
       $scope.totalPinsLimit = 5;

       $scope.totalPins = $scope.countPersonal.length;
    
    }
 $scope.showBuissness=function(){
       
      $scope.PersonalAddypin=false;
      $scope.BusinessAddypin=true;

      $("#personal").removeAttr( "class" );
      $("#buissness").removeAttr( "class" );

     
        $("#buissness").addClass("btn btn-primary");
        $("#personal").addClass("btn btn-default");


        $scope.userType = "business"; 
        $scope.totalPinsLimit = 20;


     $scope.totalPins = $scope.countBusiness.length;
    }



     

    $scope.share = function (){
        PinService.SharePin($scope.currentPin.code, $scope.shareEmail).then(function (data){
            if(data.status == "shared"){
                $scope.messageClass = "success";
                $scope.message = "addypin shared";
                $scope.responseReturned = true;
                $scope.shareEmail = "";
            }
            else{
                $scope.messageClass = "danger";
                $scope.message = "Error! There was an error. Please try again in a moment.";
                $scope.responseReturned = false;
            }
            $scope.shareShown = false;
        });
    }

    $scope.showEdit = function (pin){

        /* $('html, body').animate({
                    scrollTop: $(".windoweditpin").offset().top
                });*/
        

        $scope.currentPin = ($scope.currentPin  != null && $scope.currentPin.pin_id == pin.pin_id) ? $scope.currentPin:  angular.copy(pin)  ; 
        //console.log(pin);
        $scope.editShown = true;

        $(window).scrollTop($('#main-my-pins').position().top);
        $('html, body').animate({
                    scrollTop: $(".windoweditpin").offset().top - 120
        });
       // $('.windoweditpin').css('margin-top','10%');
        //$(function(){

        $scope.getLocation($scope.currentPin.lat,$scope.currentPin.long);
        //});
    };

    $scope.hideEdit = function (){
        //$scope.currentPin = null;
        $scope.editShown = false;
    };

    $scope.showDelete = function (pin){
        $scope.currentPin = pin;
        $scope.deleteShown = true;
    };

    $scope.hideDelete = function (){
        //$scope.currentPin = null;
        $scope.deleteShown = false;
    };

    $scope.confirmDelete = function (){
        PinService.DeletePin($scope.currentPin.pin_id).then(function (data){
            //$scope.pins = data.pins;
            GetMyPins();
            if(data.rowsAffected != null && data.rowsAffected != undefined && data.rowsAffected > 0){
                $scope.messageClass = "success";
                $scope.message = "addypin deleted";
                $scope.responseReturned = true;
            }
            else{
                $scope.messageClass = "danger";
                $scope.message = "Error! There was an error. Please try again in a moment.";
                $scope.responseReturned = true;
            }
        });

        $scope.editShown = false;
        $scope.deleteShown = false;
    };
    
	$scope.VerifyPin=function(pin){
$scope.currentPin=pin;
     PinService.VerifyPin($scope.currentPin.pin_id).then(function (data){
           //$scope.pins = data.pins;
           GetMyPins();
           if(data.rowsAffected != null && data.rowsAffected != undefined && data.rowsAffected > 0){
               $scope.currentPin.is_active = 1;
			   $scope.messageClass = "success";
               $scope.message = "addypin Activated successfully";
               $scope.responseReturned = true;
           }
           else{
               $scope.messageClass = "danger";
               $scope.message = "Error! There was an error. Please try again in a moment.";
               $scope.responseReturned = true;
           }
       });
}



	
    $scope.UpdatePin = function (){
        console.log(".img_"+$scope.currentPin.pin_id);
        HomeService.PincodeAvailability($scope.currentPin.code,$scope.currentPin.pin_id).then(function (data){
            if(!data.available){
                $('#duplicate-pin').show();
                return false;
            }
            else{
                $('#duplicate-pin').hide();
                $scope.currentPin.code = $scope.currentPin.code.toUpperCase();
                PinService.UpdatePin($scope.currentPin).then(function (data) {
                if(data.updated){

                  
                    $("#img_"+$scope.currentPin.pin_id).load();
                    $("#img_"+$scope.currentPin.pin_id).trigger('load');
                    $('.lazy').Lazy();

                    $scope.messageClass = "success";
                    $scope.message = "addypin updated";
                    $scope.responseReturned = true;
                    GetMyPins();
                }
                else{
                    $scope.messageClass = "danger";
                    $scope.message = "Error! There was an error. Please try again in a moment.";
                    $scope.responseReturned = true;
                }
                $scope.currentPin = null;
                $scope.editShown = false;
            });
            }
        });
        
    };

    $scope.showShare = function (pin){
        $scope.currentPin = pin;
        $scope.shareEmail = "";
        $scope.shareShown = true;
        
    };

    $scope.hideShare = function (){
        //$scope.currentPin = null;
        $scope.shareShown = false;
    };

    $scope.confirmShare = function (){

    };

    var token = localStorage.getItem('token');

    if(token != null && token != undefined && !jwtHelper.isTokenExpired(token)){
        var currentPath = $location.path();
        var parts = currentPath.substring(1).split('/');
        //console.log(parts);
        if(parts[1] == 'manage-pins')
            $state.go('pin.mypins');
    }

    function GetMyPins(){
        PinService.GetPinsByUserId().then(function (data){
            $scope.pins = data.pins;
           

           //$scope.failedSubjects = filterFilter($scope.pins, {'pin_type':'1'});
            //$scope.totalPins = ($scope.pins | filter : {pin_type:'1'}).length;

$scope.countPersonal = $scope.pins.filter(function(item){
    return (item.pin_type == 1);
});
$scope.countBusiness = $scope.pins.filter(function(item){
    return (item.pin_type == 2);
});


 


        $scope.totalPins = $scope.countPersonal.length;;
           // $scope.totalPins = data.pins.length;

            $scope.userType = data.user.user_type_id == 1 ? "personal" : "business";
            $scope.totalPinsLimit = data.user.user_type_id == 1 ? 5 : 20;

            //console.log(data.pins);
        });
    }

    var mylat = 31.544301306672725;
    var mylon = 74.34968769702152;
    var userdata;
    $scope.getLocation = function(lat,lng){
        locallat = lat;
        locallon = lng;
        var geooptions = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };
/*        if (navigator.geolocation) {
            //navigator.geolocation.getCurrentPosition(locationsuccess,locationerr,geooptions);
            console.log(locallat +"-"+  locallon);
            initmap(locallat,locallon);
        } 
        else{
*/            
           // $scope.initmap(lat,lng);
		initmap(lat,lng);
        /*}*/

    }

   /* $scope.initmap = function (finallat,finallon){ */
   function initmap(finallat,finallon){ 
	
	    $('#map-container').locationpicker({
            location: {latitude: finallat, longitude: finallon},
            radius: 0,
		    enableAutocomplete: true,
            markerInCenter: true,
			 inputBinding:{
                latitudeInput: $('#latitude'),
                longitudeInput: $('#longitude'),
                locationNameInput: $('#address')
            },
			onchanged: function (currentLocation, radius, isMarkerDropped) {
				
				$scope.messageClass = "";
                $scope.message = "";
				$scope.responseReturned = true;
					  
					  
                var addressComponents = $(this).locationpicker('map').location.addressComponents;
                formattedAddress = $(this).locationpicker('map').location.formattedAddress;
				addressComponents.formattedAddress = $(this).locationpicker('map').location.formattedAddress;
				

               /* $scope.updateControls(addressComponents,currentLocation,formattedAddress);
				$scope.updateCurrentPin($scope.currentPin.pin_id,currentLocation,formattedAddress);*/
				
				updateControls(addressComponents,currentLocation,formattedAddress);
				updateCurrentPin($scope.currentPin.pin_id,currentLocation,formattedAddress);
            },
            oninitialized: function(component) {
				  
                    var addressComponents = $(component).locationpicker('map').location.addressComponents;
                    
					 var currentLocation = {latitude : finallat, longitude:finallon};
                    addressComponents.formattedAddress = $(component).locationpicker('map').location.formattedAddress;
                    /*$scope.updateControls(addressComponents,currentLocation,addressComponents.formattedAddress);*/
					updateControls(addressComponents,currentLocation,addressComponents.formattedAddress);
                   
                }
				
            });
    }


 /* update current pin */
   /*$scope.updateCurrentPin = function(pin_id,currentLocation,formattedAddress){*/
   function updateCurrentPin(pin_id,currentLocation,formattedAddress){
	       if(currentLocation){
		    $scope.currentPin.lat = currentLocation.latitude;
            $scope.currentPin.long = currentLocation.longitude;
			
			$scope.currentPin.address = formattedAddress;
			
			
			  PinService.UpdateAddressPin($scope.currentPin).then(function (data) {
                if(data.updated){
                    $scope.messageClass = "success";
                    $scope.message = "addypin updated";
					  $scope.responseReturned = true;
                }else{
                    $scope.messageClass = "danger";
                    $scope.message = "Error! There was an error. Please try again in a moment.";
					  $scope.responseReturned = true;
                }
				
			  });
				
				
				
		   }
			
			
   };
	  
	  
    /*$scope.updateControls = function(addressComponents,currentLocation,formattedAddress) {*/
      function updateControls(addressComponents,currentLocation,formattedAddress){
        
        $scope.currentPin.address = formattedAddress;
        console.log(formattedAddress)

        if(currentLocation){
            $scope.currentPin.lat = currentLocation.latitude;
            $scope.currentPin.long = currentLocation.longitude;
            console.log('before save')
            console.log($scope.currentPin)
        }
        else{
            $.getJSON('//freegeoip.net/json/?callback=?', function(data) {
            $scope.currentPin.lat = data.latitude;
            $scope.currentPin.long = data.longitude;    
            });
         }
    }  


}]);