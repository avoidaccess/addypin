angular.module('main.pin').config(function($stateProvider) {
    $stateProvider
        .state('pin.manage', {
            url: '/manage-pins',
            templateUrl: 'modules/pin/views/manage.html',
            controller: 'PinController'
        })
        .state('pin.mypins', {
            url: '/my-pins/:any',
            templateUrl: 'modules/pin/views/my-pins.html',
            controller: 'PinController'
        })
    ;
});