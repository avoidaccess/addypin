angular.module('main').factory('url', function ($timeout, $http, envService){
    return {
        apiUrl: function (url){
            console.log('Env services: ' ,envService);
            var apiUrl = '//api.addypin.com';//envService.read('apiUrl');
            return apiUrl + url;
        },
        subdomainUrl: function (subdomain){
            var url = "//" + subdomain + envService.read('subdomainUrl');
            return url;
        },
        subdomainUrlDisplay: function (subdomain){
            var url = "https://" + subdomain + envService.read('subdomainUrl');
            return url;
        },
        host: function (h){
            switch(h){
                case 'localhost':
                case 'addypin':
                    return 'development';
                break;

                case 'addypin.digitalmeetings.net':
                    return 'staging';
                break;

                case 'addypin.com':
                case 'www.addypin.com':
                    return 'production';
                break;
            }
        }
    };
});