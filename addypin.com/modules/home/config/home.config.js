angular.module('main.home').config(function($stateProvider) {
    $stateProvider
        .state('home.index', {
            url: '/',
            templateUrl: 'modules/home/views/index.html',
            controller: 'HomeController'
        })
    ;
});