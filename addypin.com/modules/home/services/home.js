angular.module('main').factory('HomeService', function ($timeout, $http, url, deviceDetector){
    return {
        SendRegisterRequest: function (email, type){
            var dt = deviceDetector;
            var data = {
                email: email,
                type: type,
                browser: dt.browser,
                device: dt.device,
                os: dt.os,
                os_version: dt.os_version,
                is_desktop: dt.isDesktop(),
                is_mobile: dt.isMobile(),
                is_table: dt.isTablet(),
                user_agent: dt.raw.userAgent
            };
            var promise = $http({ url: url.apiUrl('/register'), method: 'POST', data: data})
                .then(function (response){
                    console.log(response);
                    return response.data;
                });
            return promise;
        },

        RegistrationActivation: function (guid, email, type){
            var promise = $http({ url: url.apiUrl('/registration-activation'), method: 'POST', data: { code: guid, email: email, type: type } })
                .then(function (response){
                    console.log(response);
                    return response.data;
                });
            return promise;
        },

        RegisterPin: function (user){
		console.log(user);
            //user.uid = localStorage.getItem('uid');
            var promise = $http({ url: url.apiUrl('/register-pin'), method: 'POST', data: user})
                .then(function (response){
                    console.log(response);
                    return response.data;
                });
            return promise;
        },

        PincodeAvailability: function (code,id){
            var promise = $http({ url: url.apiUrl('/pin-code-availability/' + code+ '/' + id), method: 'POST', data: null})
                .then(function (response){
                    console.log(response);
                    return response.data;
                });
            return promise;
        },
        

        ExpiredTimeCheck: function (id){
            var promise = $http({ url: url.apiUrl('/expired-time-check/'+ id), method: 'POST', data: null})
            //var promise = $http({ url: url.apiUrl('/expired-time-check/'+ id), method: 'POST', data: null})
                .then(function (response){
                    console.log(response);
                    return response.data;
                });
            return promise;
        },
        
         getAddypin: function (){
            var promise = $http({ url: url.apiUrl('/get/addypin/counts'), method: 'POST', data: null})
                .then(function (response){
                     console.log('---'+JSON.stringify(response));
                    return response.data;
                });
            return promise;
        },

        PinActivation: function (uid, guid, pid){
            var promise = $http({ url: url.apiUrl('/pin-activation/' + uid + '/' + guid + '/' + pid), method: 'POST', data: null})
                .then(function (response){
                    console.log(response);
                    return response.data;
                });
            return promise;
        }
    };
});