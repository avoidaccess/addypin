-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 04, 2017 at 05:24 AM
-- Server version: 5.5.55-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `addypin`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE IF NOT EXISTS `admin_users` (
  `au_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`au_id`),
  KEY `username` (`username`),
  KEY `is_active` (`is_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`au_id`, `username`, `password`, `is_active`, `is_deleted`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ip_table`
--

CREATE TABLE IF NOT EXISTS `ip_table` (
  `ip_id` int(100) NOT NULL AUTO_INCREMENT,
  `ip` varchar(100) NOT NULL,
  `count` int(100) NOT NULL,
  `date_time` varchar(100) NOT NULL,
  PRIMARY KEY (`ip_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `ip_table`
--

INSERT INTO `ip_table` (`ip_id`, `ip`, `count`, `date_time`) VALUES
(45, '98.207.101.29', 1, '1506374490'),
(46, '66.46.188.219', 0, '1505763805'),
(47, '111.118.252.146', 0, '1506321412'),
(48, '73.241.65.228', 0, '1506643466'),
(49, '172.58.33.216', 0, '1505959755');

-- --------------------------------------------------------

--
-- Table structure for table `lookup_limit`
--

CREATE TABLE IF NOT EXISTS `lookup_limit` (
  `lookup_id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(18) NOT NULL,
  `lookup_date` date NOT NULL,
  `lookup_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lookup_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `lookup_limit`
--

INSERT INTO `lookup_limit` (`lookup_id`, `ip`, `lookup_date`, `lookup_count`) VALUES
(40, '98.207.101.29', '2017-09-22', 3),
(41, '172.58.35.187', '2017-09-24', 1),
(42, '111.118.252.146', '2017-09-25', 23);

-- --------------------------------------------------------

--
-- Table structure for table `lookup_limit_invalid`
--

CREATE TABLE IF NOT EXISTS `lookup_limit_invalid` (
  `lookup_id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(18) NOT NULL,
  `lookup_date` date NOT NULL,
  `lookup_date_time` int(11) NOT NULL,
  `lookup_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lookup_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `lookup_limit_invalid`
--

INSERT INTO `lookup_limit_invalid` (`lookup_id`, `ip`, `lookup_date`, `lookup_date_time`, `lookup_count`) VALUES
(68, '111.118.252.146', '2017-09-22', 1506076333, 2),
(69, '98.207.101.29', '2017-09-22', 1506100609, 3),
(70, '66.102.6.156', '2017-09-22', 1506101885, 1),
(71, '66.102.6.155', '2017-09-22', 1506101887, 2),
(72, '73.241.65.228', '2017-09-29', 1506643289, 2);

-- --------------------------------------------------------

--
-- Table structure for table `pins`
--

CREATE TABLE IF NOT EXISTS `pins` (
  `pin_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `pin_type` int(11) NOT NULL DEFAULT '1',
  `name` varchar(100) DEFAULT NULL,
  `location_name` varchar(50) DEFAULT NULL,
  `code` varchar(10) DEFAULT NULL,
  `lat` varchar(20) DEFAULT NULL,
  `long` varchar(20) DEFAULT NULL,
  `address` text,
  `country` varchar(20) NOT NULL,
  `postal_code` varchar(20) DEFAULT NULL,
  `landmark` tinytext,
  `date_created` datetime DEFAULT NULL,
  `activation_code` varchar(15) DEFAULT NULL,
  `mode` varchar(255) DEFAULT 'ip',
  `is_gps` tinyint(1) DEFAULT '0',
  `is_active` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  `authorize_time` int(11) NOT NULL,
  `link_click` int(11) NOT NULL,
  `mail_send` int(11) NOT NULL,
  PRIMARY KEY (`pin_id`),
  KEY `user_id` (`user_id`),
  KEY `code` (`code`),
  KEY `lat` (`lat`),
  KEY `long` (`long`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=411 ;

--
-- Dumping data for table `pins`
--

INSERT INTO `pins` (`pin_id`, `user_id`, `pin_type`, `name`, `location_name`, `code`, `lat`, `long`, `address`, `country`, `postal_code`, `landmark`, `date_created`, `activation_code`, `mode`, `is_gps`, `is_active`, `is_deleted`, `authorize_time`, `link_click`, `mail_send`) VALUES
(1, 1, 1, 'Amr Samir', 'The Campus', '1MHERE', '37.9689567', '-121.6716356', '1360 Delta Rd, Knightsen, CA 94548, USA', 'QA', '94548', 'FGF', '2016-10-06 11:23:46', 'grmv8xwdvgjt78y', 'ip', 0, 1, 0, 0, 0, 0),
(2, 1, 1, '', 'Home', 'UNDN6F', '25.32938891483938', '51.52062344807132', 'United Nations St, Doha', '', '', '', '2016-10-06 11:28:28', 'rbhcz9xjmsb8bh6', 'ip', 0, 1, 1, 0, 0, 0),
(3, 1, 1, '', 'Home', 'FINDM3', '25.5', '51.25', 'Diplomatic St, Doha', '', '', '', '2016-10-06 11:47:46', 'yajuwhy3jpdnyya', 'ip', 0, 1, 1, 0, 0, 0),
(4, 1, 1, '', 'work', 'UND2WH', '25.5', '51.25', 'Diplomatic St, Doha', '', '', '', '2016-10-06 11:48:38', '5x8cvibe74137xi', 'ip', 0, 0, 1, 0, 0, 0),
(5, 1, 1, '', 'work', 'UNDR62', '25.5', '51.25', 'Diplomatic St, Doha', '', '', '', '2016-10-06 11:48:44', 'b1j4tlbsb2ln9qd', 'ip', 0, 0, 1, 0, 0, 0),
(6, 1, 1, '', 'home', 'UNDY2Z', '0', '0', 'Diplomatic St, Doha', '', '', '', '2016-10-06 11:48:52', 't37v228d3q12tar', 'ip', 0, 0, 1, 0, 0, 0),
(7, 1, 1, '', 'home', 'UND99P', '0', '0', 'Diplomatic St, Doha', '', '', '', '2016-10-06 11:48:59', '8hjvri63zc171s6', 'ip', 0, 1, 1, 0, 0, 0),
(8, 1, 1, '', 'home', 'UNDC8D', '25.5', '51.25', 'Diplomatic St, Doha', '', '', '', '2016-10-06 11:49:21', 'l97mch7t3cvbpxn', 'ip', 0, 1, 1, 0, 0, 0),
(9, 2, 1, '', 'work', 'WORK12', '31.4888', '74.3686', 'Walton Road, Lahore', '', '', '', '2016-10-06 11:53:14', 'zndtv41lr2b2lr6', 'ip', 0, 1, 1, 0, 0, 0),
(10, 1, 1, '', 'Home', 'UNDW21', '25.5', '51.25', 'Diplomatic St, Doha', '', '', '', '2016-10-06 11:55:57', '9x4sz7yj2gd8lhz', 'ip', 0, 1, 1, 0, 0, 0),
(11, 1, 1, '', 'Home', 'UNDM2T', '25.5', '51.25', 'Diplomatic St, Doha', '', '', '', '2016-10-06 11:56:34', 'bb6njs6yi5q14zq', 'ip', 0, 1, 1, 0, 0, 0),
(12, 1, 1, '', 'home', 'UND52T', '0', '0', 'Diplomatic St, Doha', '', '', '', '2016-10-06 11:57:26', 'dsw2uaeztxxvnbu', 'ip', 0, 1, 1, 0, 0, 0),
(13, 1, 1, '', 'Home', '1MHERE', '25.5', '51.25', 'Diplomatic St, Doha', '', '', '', '2016-10-06 12:01:23', '31kqtij5n1w2cfe', 'ip', 0, 1, 1, 0, 0, 0),
(14, 1, 1, '', 'Home', 'imher3', '25.5', '51.25', 'Diplomatic St, Doha', '', '', '', '2016-10-07 04:55:00', '25233cegkmncw4f', 'ip', 0, 1, 1, 0, 0, 0),
(15, 1, 1, '', 'Home', '1MHERE', '25.5', '51.25', 'Diplomatic St, Doha', '', '', '', '2016-10-07 05:02:13', 'hew3eatnhucveb3', 'ip', 0, 1, 1, 0, 0, 0),
(16, 1, 1, '', 'Ø¹Ù?Ù?', 'UNDR2Y', '25.2867', '51.5333', 'Slope Roundabout, Ar-Rayyan', '', '', '', '2016-10-07 10:32:44', 'f1m88k73l9htaib', 'ip', 0, 0, 1, 0, 0, 0),
(17, 1, 1, '', 'Geork', 'AVO4QL', '25.2867', '51.5333', 'Khalifa St, Doha', '', '', '', '2016-10-07 10:37:38', 'yw4evaqqqh3g4ws', 'ip', 0, 0, 1, 0, 0, 0),
(18, 1, 1, '', 'Katara', '2MHERE', '25.2867', '51.5333', 'Al Moasses St, Doha', '', '', '', '2016-10-07 11:41:12', 'p3fvltiyfu4sxmf', 'ip', 0, 1, 1, 0, 0, 0),
(19, 3, 1, '', 'Work', 'FAIZ91', '31.5497', '74.3436', 'Mall Rd, Lahore', '', '', '', '2016-10-07 12:42:44', 'kh3eqq76pijvzaf', 'ip', 0, 1, 1, 0, 0, 0),
(20, 1, 1, '', 'Home', '3MHERE', '25.5', '51.25', 'Diplomatic St, Doha', '', '', '', '2016-10-07 12:53:30', 'nyxk6tsg6jieeyf', 'ip', 0, 1, 1, 0, 0, 0),
(21, 3, 1, '', 'Work', 'FAI65M', '31.550189147637994', '74.34191020832054', 'Mall Rd, Lahore', '', '', '', '2016-10-07 12:54:33', 'u49klx6s2mw78i1', 'ip', 0, 1, 1, 0, 0, 0),
(22, 1, 1, '', 'Home', '4MHERE', '25.3303507510996', '51.521023501513696', 'United Nations St, Doha', '', '', '', '2016-10-07 13:46:37', 'e6hkpz2grkaqgmj', 'ip', 0, 1, 1, 0, 0, 0),
(23, 1, 1, '', 'Mezo', '5MHERE', '25.2867', '51.5333', 'Al Ahmed sreet, Doha', '', '', '', '2016-10-07 19:12:49', '8eyjtngzm1uri2x', 'ip', 0, 0, 1, 0, 0, 0),
(24, 1, 1, '', 'home', '5MHERE', '25.475827079723842', '51.45942687988281', 'Unnamed Road, undefined', '', '', '', '2016-10-08 11:19:08', '77a3mmzceci6vqn', 'ip', 0, 0, 1, 0, 0, 0),
(25, 4, 1, '', 'Work', 'AHM001', '30.0771', '31.2859', '492 Krarah Ibn Shrek, undefined', '', '', '', '2016-10-09 12:51:50', '95vl9tyvwwg53wn', 'ip', 0, 1, 1, 0, 0, 0),
(26, 5, 1, '', 'Home', 'MEOW22', '25.2867', '51.5333', 'Lena''s Crib, Doha, Qatar', '', '', '', '2016-10-09 19:05:18', 'kc6s5bn41s86rru', 'ip', 0, 1, 1, 0, 0, 0),
(27, 3, 1, '', 'work', 'FAIZ91', '31.4888', '74.3686', ', Lahore', '', '', '', '2016-10-15 07:55:55', 'kcz1y3er7gm5rzg', 'ip', 0, 1, 1, 0, 0, 0),
(28, 3, 1, '', 'yolo', 'UND9B1', '31.4888', '74.3686', ', Lahore', '', '', '', '2016-10-15 08:55:16', 'pfnz55wsgv6ipqk', 'ip', 0, 1, 1, 0, 0, 0),
(29, 3, 1, '', 'holo', 'UNDQ2J', '31.4888', '74.3686', ', Lahore', '', '', '', '2016-10-15 10:01:42', 'njiilkd4mg7cqg5', 'ip', 0, 1, 1, 0, 0, 0),
(30, 6, 1, '', 'worko', 'FAIZ91', '31.4888', '74.3686', ', Lahore', '', '', '', '2016-10-15 10:20:36', 'b89qg4tdmzhetut', 'ip', 0, 1, 1, 0, 0, 0),
(31, 6, 1, '', 'workolo', 'UND848', '31.4888', '74.3686', 'Walton Road, Lahore', '', '', '', '2016-10-15 10:49:13', 'jnfumaqcb18axfe', 'ip', 0, 0, 1, 0, 0, 0),
(32, 1, 1, '', 'Home', 'UNDBA3', '25.5', '51.25', 'Diplomatic St, Doha', '', '', '', '2016-10-17 09:57:04', 'rc6nfbb9pykxph2', 'ip', 0, 1, 1, 0, 0, 0),
(33, 1, 1, '', 'Home', 'UND3Q2', '25.5', '51.25', 'Diplomatic St, Doha', '', '', '', '2016-10-18 07:42:29', 'g45vs7t34g7d3sj', 'ip', 0, 1, 1, 0, 0, 0),
(34, 1, 1, '', 'Home', '4MHERE', '25.5', '51.25', 'Diplomatic St, Doha', '', '', '', '2016-10-19 07:26:36', '1b3eqt6vyfhts1r', 'ip', 0, 1, 1, 0, 0, 0),
(35, 1, 1, '', 'Home', '5MHERE', '25.5', '51.25', 'Diplomatic St, Doha', '', '', '', '2016-10-20 06:17:58', 'cyswpxy91x4ji8d', 'ip', 0, 0, 1, 0, 0, 0),
(36, 7, 1, '', 'Home', 'HR3HOM', '25.2582', '55.3047', 'Unnamed Road, Dubai', '', '', '', '2016-10-20 06:36:39', 'bagzlhpkz3r5gga', 'ip', 0, 1, 1, 0, 0, 0),
(37, 8, 1, '', 'Starbucks', 'SUB123', '25.2867', '51.5333', 'Departure Terminal Rd, Doha', '', '', '', '2016-10-20 08:48:37', '9h2g9dtxbduuytz', 'ip', 0, 0, 1, 0, 0, 0),
(38, 9, 1, '', 'Outing', 'SUB123', '25.2867', '51.5333', ', Doha', '', '', '', '2016-10-20 09:02:35', 'nstzsn88ele9nky', 'ip', 0, 1, 1, 0, 0, 0),
(39, 10, 1, '', 'Work', '6MHERE', '25.266151684302752', '51.55429751054885', ', Doha', '', '', '', '2016-10-20 09:12:08', 't4hk3e47jyudsp7', 'ip', 0, 1, 1, 0, 0, 0),
(40, 1, 1, '', 'Home', 'AVO99J', '25.5', '51.25', 'Diplomatic St, Doha', '', '', '', '2016-10-20 10:08:56', 'khq7kfzmllxqb7v', 'ip', 0, 1, 1, 0, 0, 0),
(41, 1, 1, '', 'Home', '3MHERE', '25.5', '51.25', 'Diplomatic St, Doha', '', '', '', '2016-10-20 10:12:41', 'nmqaffw5rlyvcr9', 'ip', 0, 0, 1, 0, 0, 0),
(42, 1, 1, '', 'Jome', '3MHERE', '25.5', '51.25', 'Diplomatic St, Doha', '', '', '', '2016-10-20 10:15:21', 'yxebx53n4n53cs2', 'ip', 0, 0, 1, 0, 0, 0),
(43, 11, 1, '', 'home', 'AHMD45', '43.4456228', '-80.4770102', '31 Fairview Ave, Kitchener', '', '', '', '2016-10-21 05:15:01', 'tjg5rmvujudn2jc', 'ip', 0, 0, 1, 0, 0, 0),
(44, 5, 1, '', 'city center', 'LENA22', '25.2867', '51.5333', 'Al Ahmed sreet, Doha', '', '', '', '2016-10-21 09:27:08', 'etcd9ahhnxilihv', 'ip', 0, 1, 1, 0, 0, 0),
(45, 5, 1, '', 'work', 'LENQ2A', '25.2867', '51.5333', 'Conference Centre, Doha', '', '', '', '2016-10-21 09:31:24', '3ht2sp1v9k87juh', 'ip', 0, 0, 1, 0, 0, 0),
(46, 5, 1, '', 'no gps', 'LENA23', '25.2867', '51.5333', 'Al Ahmed sreet, Doha', '', '', '', '2016-10-21 09:42:55', '2wdnex7ebjbn479', 'ip', 0, 1, 1, 0, 0, 0),
(47, 5, 1, '', 'gps on', 'LENA24', '25.2867', '51.5333', 'Mohammed Bin Thani St, Doha', '', '', '', '2016-10-21 09:43:42', 'k8u4ppwbxcck9y1', 'ip', 0, 1, 1, 0, 0, 0),
(48, 12, 1, '', 'work', 'UND41L', '31.4888', '74.3686', 'Jail Road, Lahore', '', '', '', '2016-10-22 08:38:22', '9hbwy4wq3v6nmab', 'ip', 0, 1, 1, 0, 0, 0),
(49, 13, 1, '', 'work', 'UNDAF3', '31.4888', '74.3686', ', Lahore', '', '', '', '2016-10-22 08:43:14', 'm8gleufi9rnyqxe', 'ip', 0, 1, 1, 0, 0, 0),
(50, 1, 1, '', 'Gps on', 'FINdm3', '25.329023943995082', '51.530615435594996', 'Diplomatic St, Doha, Qatar', '', '', '', '2016-10-22 10:18:39', 'x4djbc3av7j1dnk', 'ip', 1, 1, 1, 0, 0, 0),
(51, 1, 1, '', 'No gps', '3MHERE', '25.5', '51.25', 'Unnamed Road, Qatar', '', '', '', '2016-10-22 10:20:13', '87yetlwlgmpr4rc', 'ip', 1, 1, 1, 0, 0, 0),
(52, 5, 1, '', 'no gps', 'LENA20', '25.2867', '51.533300000000054', 'Al Ahmed sreet, Doha, Qatar', '', '', '', '2016-10-22 10:45:50', 'r8x28w68h3tr3l4', 'ip', 1, 1, 1, 0, 0, 0),
(53, 5, 1, '', 'With GPS', 'LENA21', '25.2623008', '51.556250699999964', 'D Ring Rd, Doha, Qatar', '', '', '', '2016-10-22 10:47:30', 'ehpe74akkmewi9b', 'ip', 1, 1, 1, 0, 0, 0),
(54, 14, 1, '', 'worko', '7MHERE', '31.57221115829927', '74.31885835859987', 'Chamberlain Rd, Lahore, Pakistan', '', '', '', '2016-10-24 10:14:22', 'dxx74xqt4fsf28v', 'ip', 1, 1, 1, 0, 0, 0),
(55, 14, 1, '', 'fff', '1mhere', '31.531338078140802', '74.3036823543091', '86 Ghazali Rd, Lahore 54500, Pakistan', '', '', '', '2016-10-24 10:16:38', 'izec5nvvkzxrn42', 'ip', 1, 1, 1, 0, 0, 0),
(56, 5, 1, '', 'gps', 'LENA22', '25.3287567', '51.53073440000003', 'Diplomatic St, Doha, Qatar', '', '', '', '2016-10-24 11:01:29', 'rrt6nze7b4db1vy', 'ip', 1, 1, 1, 0, 0, 0),
(57, 10, 1, '', 'Gps', 'FINDM1', '25.323313912400344', '51.527560232255155', 'Omar Al Mukhtar St, Doha, Qatar', '', '', '', '2016-10-24 11:46:28', 'jjul1bhs1j1q122', 'ip', 1, 1, 1, 0, 0, 0),
(58, 10, 1, '', 'No gps', 'FINDM2', '25.2867', '51.533300000000054', 'Al Ahmed sreet, Doha, Qatar', '', '', '', '2016-10-24 11:51:57', 'uhe2w2yujvvbe4l', 'ip', 1, 1, 1, 0, 0, 0),
(59, 10, 1, '', 'Gps on', 'FINDM3', '25.315981629999992', '51.523761690000015', 'Al Corniche St, Doha, Qatar', '', '', '', '2016-10-24 11:54:52', 'lne4nuzh8sdgicz', 'ip', 1, 1, 1, 0, 0, 0),
(60, 10, 1, '', 'Gps', 'FINDM1', '25.32287981718117', '51.510397058203694', 'Al Markhiya St, Doha, Qatar', '', '', '', '2016-10-24 12:06:30', 'brbm4gg4q1wut2c', 'ip', 1, 1, 1, 0, 0, 0),
(61, 15, 1, '', 'Gps q tire', 'FINDM4', '25.335961410609407', '51.46389806191746', 'Doha Expy, Doha, Qatar', '', '', '', '2016-10-24 12:28:33', 'ecnaqqukp6vuf5x', 'ip', 1, 0, 1, 0, 0, 0),
(62, 10, 1, '', 'Gps in q tire', 'FINDM4', '25.335840990944135', '51.463713979507475', 'Doha Expy, Doha, Qatar', '', '', '', '2016-10-24 12:42:52', 'nsz1pkrbjw99cqs', 'ip', 1, 1, 1, 0, 0, 0),
(63, 10, 1, '', 'Gps', 'FINDM5', '25.32852977041394', '51.530759760911906', '', '', '', '', '2016-10-24 17:57:03', 'h4wcf54gcigialx', 'ip', 1, 1, 1, 0, 0, 0),
(64, 5, 1, '', 'Chez Amr', 'MEOW23', '25.3287578', '51.5307722', 'Diplomatic St, Doha, Qatar', '', '', '', '2016-10-24 18:15:35', 'l5449jz4jxpjs3w', 'ip', 1, 1, 1, 0, 0, 0),
(65, 5, 1, '', 'gps', 'MEOW22', '25.327387238927873', '51.536154202685566', '', '', '', '', '2016-10-24 18:18:17', 'tygrt2tyun65n17', 'ip', 1, 0, 1, 0, 0, 0),
(66, 5, 1, '', 'gps', 'MEOW22', '25.324893859867935', '51.53122995239255', '', '', '', '', '2016-10-24 18:19:56', '157fkwbcembr8hm', 'ip', 1, 0, 1, 0, 0, 0),
(67, 5, 1, '', 'gps', 'MEOW25', '25.329690851692035', '51.51974404118653', '', '', '', '', '2016-10-24 18:21:04', '3sem1ehdgqfrlnc', 'ip', 1, 0, 1, 0, 0, 0),
(68, 16, 1, '', 'Workout', 'DLJ570', '25.321903983755682', '51.52770426418783', '', '', '', '', '2016-10-25 14:33:33', '72tepuipqpvpqd8', 'ip', 1, 1, 1, 0, 0, 0),
(69, 17, 1, 'Home', 'home', 'SA3M1B', '0', '0', '32 Faiview Avenue , Kitchener', '', 'N2H3E7', 'ON', '2016-10-25 17:46:43', 'y1dfb3n1t3d985h', 'ip', 0, 1, 1, 0, 0, 0),
(70, 1, 1, '', 'home', '4MHERE', '25.328702999999997', '51.530764999999974', 'Diplomatic St, Doha, Qatar', '', '', '', '2016-10-25 18:09:12', 'fnqj34se3ijf5iu', 'ip', 1, 0, 1, 0, 0, 0),
(71, 5, 1, '', 'gps on', 'MEOW27', '25.3588792', '51.52539819999993', 'Al Moasses St, Doha, Qatar', '', '', '', '2016-10-25 19:47:42', 'mqni7a14qfjv4p5', 'ip', 1, 1, 1, 0, 0, 0),
(72, 18, 1, '', 'Cafe', 'HATEM1', '31.2156', '29.955299999999966', 'Nady Somoha Al Ryadi, Ezbet Saad, Qism Sidi Gabir, Alexandria Governorate, Egypt', '', '', '', '2016-11-02 21:52:23', 'v6xem7wnupqcggn', 'ip', 1, 1, 1, 0, 0, 0),
(73, 18, 1, '', 'Home', 'HATE23', '30.067351091315373', '31.28815305557248', '', '', '', '', '2016-11-02 22:20:56', 'e249u3vp87dy9y7', 'ip', 1, 0, 1, 0, 0, 0),
(74, 6, 1, '', 'workolo', 'CHUAZ9', '31.4888', '74.36860000000001', 'Walton Road, Lahore, Pakistan', '', '', '', '2016-11-03 20:05:11', 't9tpv3xufi1e767', 'ip', 1, 0, 1, 0, 0, 0),
(75, 10, 1, '', 'Home gps', 'FINDM4', '30.07396664018093', '31.31833801640437', 'El-Shafei, Ash Sharekat, Nasr City, Cairo Governorate, Egypt', '', '', '', '2016-11-03 23:43:48', '26in8e9igdru9z1', 'ip', 1, 1, 1, 0, 0, 0),
(76, 19, 1, '', 'Home', 'GEES81', '30.07402828784054', '31.318032535991506', 'El-Shafei, Ash Sharekat, Nasr City, Cairo Governorate, Egypt', '', '', '', '2016-11-04 08:25:29', '91x4b8kmgu2m5uz', 'ip', 1, 1, 1, 0, 0, 0),
(77, 6, 1, '', 'Workolo', 'CHU5GB', '31.5497', '74.34359999999992', 'Mall Rd, Lahore, Pakistan', '', '', '', '2016-11-04 10:39:14', 'si2s1uaptdchz7i', 'ip', 1, 1, 1, 0, 0, 0),
(78, 6, 1, '', 'worlko', 'CHU201', '33.69', '73.05510000000004', 'Aiwan-e-Sanat-o-Tijarat, Islamabad, Pakistan', '', '', '', '2016-11-04 14:10:48', 'knrbfg17u7el3qq', 'ip', 1, 0, 1, 0, 0, 0),
(79, 20, 1, '', 'Ù?ØªÙ?Ù?Øª', 'MOH123', '30.065183004890116', '31.333399149737488', '', '', '', '', '2016-11-05 20:59:09', 'z53mqf3hxt929ew', 'ip', 1, 1, 1, 0, 0, 0),
(80, 19, 1, '', 'Zouba', 'KIKI12', '30.08103530852765', '31.31714977330921', 'Salah Salem St, Al Golf, Nasr City, Cairo Governorate, Egypt', '', '', '', '2016-11-06 18:44:52', '5w2trnklzzt6ww2', 'ip', 1, 1, 1, 0, 0, 0),
(81, 21, 1, '', 'Cavallo cafe', 'CAVAL1', '30.0575607', '31.33877259999997', 'Mohammed Hassanein Haykal, Al Manteqah as Sadesah, Nasr City, Cairo Governorate, Egypt', '', '', '', '2016-11-07 19:43:24', 'faagtvmaqtddi6t', 'ip', 1, 1, 1, 0, 0, 0),
(82, 22, 1, '', 'Some', 'WAQW57', '31.4888', '74.36860000000001', 'Walton Road, Lahore, Pakistan', '', '', '', '2016-11-09 16:04:57', 'mluz1mwbezp51cr', 'ip', 1, 0, 1, 0, 0, 0),
(83, 23, 1, '', 'Home', 'SAM123', '30.064656218942645', '31.328916288885466', '', '', '', '', '2016-11-09 19:00:46', '2unbfygj9bqgf9k', 'ip', 1, 0, 1, 0, 0, 0),
(84, 1, 1, '', 'Home', '1mhere', '25.328760799999998', '51.53077529999996', 'Diplomatic St, Doha, Qatar', '', '', '', '2016-11-16 12:53:06', 'qnbztp5bgjt2cgk', 'ip', 1, 1, 1, 0, 0, 0),
(85, 1, 1, '', 'GPS', '2MHERE', '25.32319832024056', '51.513437470239296', '27, Attia El Kholy St.', '', '', '', '2016-11-17 09:19:10', 'r52yqpjlqnhnh3i', 'ip', 1, 1, 1, 0, 0, 0),
(86, 1, 1, '', 'home', '6MHERE', '40.7828647', '-73.96535510000001', 'cairo', '', '', '', '2016-11-28 06:38:54', 'xj32n1kn1iakma6', 'ip', 1, 1, 1, 0, 0, 0),
(87, 1, 1, '', 'Home', '2MHERE', '42.2724541', '-71.73228130000001', '28 Stone Ave, Shrewsbury, MA 01545, USA', '', '', '', '2016-12-10 18:53:24', 'kha7k32du42l4nf', 'ip', 1, 1, 1, 0, 0, 0),
(88, 11, 1, '', 'home', 'AHM458', '43.4456272', '-80.4771518', '31 Fairview Ave, Kitchener, ON N2H 3E7, Canada', '', '', '', '2016-12-14 03:00:07', '22itnvlnp1qv9vh', 'ip', 1, 0, 1, 0, 0, 0),
(89, 1, 1, '', 'Home', '1MHERE', '42.2748816', '-71.73080379999999', '', '', '', '', '2016-12-15 13:06:49', '6aniexqe1hkbym3', 'ip', 1, 1, 1, 0, 0, 0),
(90, 1, 1, '', 'Another place', '2MHERE', '42.27436793985936', '-71.73582837357179', '', '', '', '', '2016-12-15 13:10:25', 'usqeqtlrguh2tex', 'ip', 1, 1, 1, 0, 0, 0),
(91, 1, 1, '', 'Home', '1MHERE', '42.27443703988293', '-71.73076088465575', '', '', '', '', '2016-12-15 13:15:24', '3qjtw3stppjcaxs', 'ip', 1, 1, 1, 0, 0, 0),
(92, 1, 1, '', 'Grocery', '3MHERE', '42.2824393', '-71.70899029999998', '27 Melody Ln, Shrewsbury, MA 01545, USA', '', '', '', '2016-12-15 14:28:35', '23xv97yb1lb2mzy', 'ip', 1, 0, 1, 0, 0, 0),
(93, 1, 1, '', 'Home', '2MHERE', '42.2724662', '-71.73229040000001', '28 Stone Ave, Shrewsbury, MA 01545, USA', '', '', '', '2016-12-17 16:37:14', '1psbgrrza4znpee', 'ip', 1, 0, 1, 0, 0, 0),
(94, 10, 1, '', 'Home', '4MHERE', '42.272456000000005', '-71.73229040000001', '28 Stone Ave, Shrewsbury, MA 01545, USA', '', '', '', '2016-12-17 16:37:54', '6d4cnzf8t964qck', 'ip', 1, 0, 1, 0, 0, 0),
(95, 24, 1, '', 'Home', 'UND4M8', '1.3113272999999999', '103.86103720000006', '20 Hamilton Rd, Singapore 209190', '', '', '', '2016-12-18 12:32:37', 'zw49dsmdcayb8b2', 'ip', 1, 1, 0, 0, 0, 0),
(96, 24, 1, '', 'Work', 'UNDH85', '1.3113263', '103.86101719999999', '20 Hamilton Rd, Singapore 209190', '', '', '', '2016-12-18 12:33:41', 'balymupe9d3hs3w', 'ip', 1, 0, 0, 0, 0, 0),
(97, 14, 1, '', 'Home', 'UNDYK1', '1.3113408', '103.86102800000003', '20 Hamilton Rd, Singapore 209190', '', '', '', '2016-12-18 12:35:55', 'eh4s12z6k2tivfa', 'ip', 1, 1, 0, 0, 0, 0),
(98, 11, 1, '', 'work', 'ENG458', '43.412312899999996', '-80.29280449999999', '55 Fleming Dr, Cambridge, ON N1T 2A9, Canada', '', '', '', '2016-12-19 17:59:22', 'ksxgxsvdcmlujqc', 'ip', 1, 1, 0, 0, 0, 0),
(99, 1, 1, '', 'Home', '5MHERE', '42.2724792', '-71.73217369999998', '28 Stone Ave, Shrewsbury, MA 01545, USA', '', '', '', '2016-12-26 11:29:33', 'yiaj4s3y7vhdhq5', 'ip', 1, 0, 1, 0, 0, 0),
(100, 1, 1, '', 'Work', 'UND84W', '42.2724145', '-71.73225309999998', '2 Marie St, Shrewsbury, MA 01545, USA', '', '', '', '2016-12-26 13:47:18', 'nb8xaj8eyyymh23', 'ip', 1, 0, 1, 0, 0, 0),
(101, 25, 1, '', 'efuture', 'INF328', '31.4607359', '74.28783799999997', 'Wattoo St, Lahore, Pakistan', '', '', '', '2016-12-26 14:54:14', 'vwq74agsemfmcuq', 'ip', 1, 0, 0, 0, 0, 0),
(102, 26, 1, '', 'itfaq town', 'UNDW1H', '31.4888', '74.36860000000001', 'Walton Road, Lahore, Pakistan', '', '', '', '2016-12-28 11:25:15', 'y8w5p27avhi1s7l', 'ip', 1, 0, 0, 0, 0, 0),
(103, 27, 1, '', 'workolo', 'UND9E5', '31.4649506', '74.28288520000001', '5 Chanadin Rd, Lahore, Pakistan', '', '', '', '2016-12-28 16:52:15', '4zj2vxlvzwu9kdv', 'ip', 1, 1, 0, 0, 0, 0),
(104, 27, 1, '', 'bolo', 'UND9ZS', '31.4649492', '74.28288900000007', '5 Chanadin Rd, Lahore, Pakistan', '', '', '', '2016-12-28 16:52:57', 'w6itw4auncu7k21', 'ip', 1, 0, 1, 0, 0, 0),
(105, 28, 1, '', 'woko1', 'SABDE4', '31.4888', '74.36860000000001', 'Walton Road, Lahore, Pakistan', '', '', '', '2016-12-28 20:30:39', '1nz5tge3c25tlm5', 'ip', 1, 1, 0, 0, 0, 0),
(106, 28, 1, '', 'woko2', 'SABAT8', '31.554606099999994', '74.35715809999999', '132 Allama Iqbal Rd, Lahore, Pakistan', '', '', '', '2016-12-28 20:30:54', '5fasll7ckfigbf6', 'ip', 1, 0, 1, 0, 0, 0),
(107, 1, 1, '', 'Home', '5MHERR', '42.2724944', '-71.73226640000001', '28 Stone Ave, Shrewsbury, MA 01545, USA', '', '', '', '2016-12-31 11:41:34', '8838l34c254adeq', 'ip', 1, 0, 1, 0, 0, 0),
(108, 1, 1, '', 'Home', '1MHERE', '42.2724944', '-71.73226640000001', '28 Stone Ave, Shrewsbury, MA 01545, USA', '', '', '', '2016-12-31 11:48:26', '9g8rfbvg6s5scdn', 'ip', 1, 0, 1, 0, 0, 0),
(109, 1, 1, '', 'Home', '1MHERR', '42.27025403864863', '-71.7347699746216', '256 Oak St, Shrewsbury, MA 01545, USA', '', '', '', '2017-01-04 15:17:01', 'uftkqzcn6xyartv', 'ip', 1, 1, 1, 0, 0, 0),
(110, 1, 1, '', 'Home', 'AVOP6H', '42.2724198', '-71.73226640000001', '2 Marie St, Shrewsbury, MA 01545, USA', '', '', '', '2017-01-04 15:21:50', 'ektr6vr59h7rhca', 'ip', 1, 0, 1, 0, 0, 0),
(111, 29, 1, '', 'Work', 'DAV9MU', '43.663183499999995', '-79.41073310000002', '720 Bathurst St, Toronto, ON M5S 2R4', '', '', '', '2017-01-16 22:10:29', 'jckwste2kwgwis3', 'ip', 1, 1, 0, 0, 0, 0),
(112, 30, 1, '', 'Home', 'UND8FE', '1.297528892672775', '103.8541717549715', '100 Victoria St, Singapore 188064', '', '', '', '2017-01-30 10:07:41', '6g7ug6zfgkeim1e', 'ip', 1, 1, 0, 0, 0, 0),
(113, 31, 1, '', 'work', 'UNDZ7H', '42.04049949999999', '-71.30511890000002', '999 Boston Providence Hwy, Wrentham, MA 02093, USA', '', '', '', '2017-01-31 16:22:23', 'e2wsjrbrflec879', 'ip', 1, 0, 0, 0, 0, 0),
(114, 32, 1, '', 'work', 'WAS3V4', '42.04049949999999', '-71.30511890000002', '999 Boston Providence Hwy, Wrentham, MA 02093, USA', '', '', '', '2017-01-31 16:22:41', 'xq5gku4hluwt42z', 'ip', 1, 0, 0, 0, 0, 0),
(115, 33, 1, '', 'work', 'AAAD1D', '42.04049949999999', '-71.30511890000002', '999 Boston Providence Hwy, Wrentham, MA 02093, USA', '', '', '', '2017-01-31 16:22:53', 'w3mam6v96ycuhjx', 'ip', 1, 0, 1, 0, 0, 0),
(116, 33, 1, '', 'work', 'AAAR1E', '42.04049949999999', '-71.30511890000002', '999 Boston Providence Hwy, Wrentham, MA 02093, USA', '', '', '', '2017-01-31 16:23:05', '954y1fgl8f9l5ei', 'ip', 1, 0, 0, 0, 0, 0),
(117, 33, 1, '', 'work', 'AAA9E4', '42.04049949999999', '-71.30511890000002', '999 Boston Providence Hwy, Wrentham, MA 02093, USA', '', '', '', '2017-01-31 16:23:28', 'nmzauzlbbluguje', 'ip', 1, 0, 1, 0, 0, 0),
(118, 34, 1, '', 'work', 'AAA69Y', '42.04049949999999', '-71.30511890000002', '999 Boston Providence Hwy, Wrentham, MA 02093, USA', '', '', '', '2017-01-31 16:23:37', 'paxu367ckmmhzsr', 'ip', 1, 0, 1, 0, 0, 0),
(119, 1, 1, '', 'Home', 'UNDDC1', '42.272113334229836', '-71.7311291433777', '212 Oak St, Shrewsbury, MA 01545, USA', '', '', '', '2017-01-31 22:31:00', 'xrpxp6h43zbkc45', 'ip', 1, 1, 1, 0, 0, 0),
(120, 24, 1, '', 'Home', 'UND1WA', '1.311388', '103.86100299999998', '20 Hamilton Rd, Singapore 209190', '', '', '', '2017-02-01 16:37:48', 'ruwlpla4rxp4tr5', 'ip', 1, 0, 0, 0, 0, 0),
(121, 30, 1, '', 'Home', 'UND41Y', '1.2975632611007641', '103.85417960354607', '100 Victoria St, Singapore 188064', '', '', '', '2017-02-04 10:01:26', 't6kdtb2gvqmva9x', 'ip', 1, 1, 0, 0, 0, 0),
(122, 30, 1, '', 'Work', 'UZA123', '1.2981861849636418', '103.84610952016283', '10 Penang Rd, Singapore 238469', '', '', '', '2017-02-04 10:04:11', 'xtl7p6tc96s7ru2', 'ip', 1, 0, 1, 0, 0, 0),
(123, 1, 1, '', 'Work', '5MHERE', '42.2724944', '-71.73226640000001', '28 Stone Ave, Shrewsbury, MA 01545, USA', '', '', '', '2017-02-04 12:36:22', 'nrih1v65x3qgsst', 'ip', 1, 0, 1, 0, 0, 0),
(124, 1, 1, '', 'Home', 'UND77K', '42.2724419', '-71.7323212', '2 Marie St, Shrewsbury, MA 01545, USA', '', '', '', '2017-02-04 16:44:56', 'ixsp7rzla7edse5', 'ip', 1, 1, 1, 0, 0, 0),
(125, 14, 1, '', 'home', 'FAI78T', '31.464574899999995', '74.2835086', '644 Service Rd, Lahore 54000, Pakistan', '', '', '', '2017-02-04 16:55:03', 'zhubwpee56sjhab', 'ip', 1, 0, 1, 0, 0, 0),
(126, 24, 1, '', 'Home', 'UND9YY', '1.2855', '103.85649999999998', '1 Fullerton Rd, Singapore 049213', '', '', '', '2017-02-06 14:20:47', '47rgz68649za8u5', 'ip', 1, 0, 0, 0, 0, 0),
(127, 24, 1, '', 'Home', 'UNDSX7', '1.2855', '103.85649999999998', '1 Fullerton Rd, Singapore 049213', '', '', '', '2017-02-06 14:21:24', 's1ssrhhtsew6t8m', 'ip', 1, 0, 0, 0, 0, 0),
(128, 35, 1, '', 'Home', 'UNDP1R', '1.311354', '103.86101069999995', '20 Hamilton Rd, Singapore 209190', '', '', '', '2017-02-06 14:21:57', 'wwmzb4dxawbb6f9', 'ip', 1, 0, 0, 0, 0, 0),
(129, 10, 1, '', 'Home', 'AVO8DV', '42.27263080860029', '-71.73272626445817', '26 Stone Ave, Shrewsbury, MA 01545, USA', '', '', '', '2017-02-08 03:41:18', 'f8p1v9gpx6ka6l8', 'ip', 1, 0, 1, 0, 0, 0),
(130, 36, 1, '', 'Home', 'UNDNJ4', '1.3113161', '103.86099739999997', '20 Hamilton Rd, Singapore 209190', '', '', '', '2017-02-08 16:19:44', 'i3zmzd1hgjbb93m', 'ip', 1, 0, 0, 0, 0, 0),
(131, 1, 1, '', 'Home', 'UND5SL', '42.272441', '-71.73230219999999', '2 Marie St, Shrewsbury, MA 01545, USA', '', '', '', '2017-02-08 16:34:56', '63h76kjyy1k78v3', 'ip', 1, 0, 1, 0, 0, 0),
(132, 1, 1, '', 'home', 'UNDUM5', '42.2724543', '-71.73227780000002', '28 Stone Ave, Shrewsbury, MA 01545, USA', '', '', '', '2017-02-08 21:12:51', 'fiz9w7d2jki7kdc', 'ip', 1, 0, 1, 0, 0, 0),
(133, 14, 1, '', 'home', 'UND4CH', '31.464663899999998', '74.28346540000007', '644 Service Rd, Lahore 54000, Pakistan', '', '', '', '2017-02-09 17:27:18', 'rjfvyi17u9pr8uz', 'ip', 1, 0, 0, 0, 0, 0),
(134, 14, 1, '', 'workkkk', 'UND1XM', '31.464664199999998', '74.28346550000003', '644 Service Rd, Lahore 54000, Pakistan', '', '', '', '2017-02-09 17:31:22', '6f6aq2bqmc58acb', 'ip', 1, 1, 1, 0, 0, 0),
(135, 37, 2, '', 'Home', 'UND181', '1.3113294', '103.86100620000002', '20 Hamilton Rd, Singapore 209190', '', '', '', '2017-02-11 14:32:55', 'aupfe421tk5cc4x', 'ip', 1, 1, 0, 0, 0, 0),
(136, 38, 1, '', 'work', 'RAMR5E', '31.4888', '74.36860000000001', 'Walton Road, Lahore, Pakistan', '', '', '', '2017-02-14 07:40:06', 'p93xshwmyap95wj', 'ip', 1, 0, 0, 0, 0, 0),
(137, 38, 1, '', 'work', 'RAM7SY', '0', '0', 'Johar town', '', '', '', '2017-02-14 07:55:23', 'aeeaidy673ek67f', 'ip', 0, 0, 1, 0, 0, 0),
(138, 14, 1, '', 'work', 'FAIA52', '31.4888', '74.36860000000001', 'Walton Road, Lahore, Pakistan', '', '', '', '2017-02-14 07:56:45', '69qiuq4vsqs13hb', 'ip', 1, 1, 1, 0, 0, 0),
(139, 39, 1, '', 'olo', 'UNDEY4', '31.4888', '74.36860000000001', 'Walton Road, Lahore, Pakistan', '', '', '', '2017-02-14 07:58:35', 'cwp2jgy3mx19lkd', 'ip', 1, 1, 0, 0, 0, 0),
(140, 38, 1, '', 'work', 'UNDL2H', '31.464652500000003', '74.28348540000002', '644 Service Rd, Lahore 54000, Pakistan', '', '', '', '2017-02-14 10:42:10', '4hlvus37n81t3a7', 'ip', 1, 0, 0, 0, 0, 0),
(141, 38, 1, '', 'work', 'RAM4H6', '31.464663399999996', '74.2834679', '644 Service Rd, Lahore 54000, Pakistan', '', '', '', '2017-02-14 10:50:09', 'bptuvfj46i29ntk', 'ip', 1, 0, 0, 0, 0, 0),
(142, 40, 1, '', 'Home', 'UND75L', '1.2855', '103.85649999999998', '1 Fullerton Rd, Singapore 049213', '', '', '', '2017-02-15 16:59:48', 'gr2cnuf9qkpgwk5', 'ip', 1, 0, 0, 0, 0, 0),
(143, 1, 1, '', 'Home', 'AVOID5', '42.27279202442584', '-71.73116815170249', '17 Sheridan Ave, Shrewsbury, MA 01545, USA', '', '01545', 'MA', '2017-02-16 12:13:20', 'xjhtc1m91jlfjnk', 'gps', 1, 1, 1, 0, 0, 0),
(144, 41, 1, '', 'work place', 'UNDTN7', '31.4646509', '74.2834355', '644 Service Rd, Lahore 54000, Pakistan', '', '', '', '2017-02-16 13:52:12', '4uje2uidgqbkjw2', 'gps', 1, 1, 0, 0, 0, 0),
(145, 36, 1, '', 'Library', 'UND16Q', '1.2977303', '103.85406839999996', '100 Victoria St, Singapore 188064', '', '', '', '2017-02-18 11:16:33', 'akcpxazpv1mr2b7', 'gps', 1, 1, 0, 0, 0, 0),
(146, 30, 1, '', 'Home', 'UNDU5X', '1.2977166000000002', '103.85409770000001', '100 Victoria St, Singapore 188064', '', '', '', '2017-02-18 12:26:30', '8xqfyzyjy122erg', 'gps', 1, 1, 1, 0, 0, 0),
(147, 30, 1, '', 'Home', 'UND661', '1.2976638', '103.8540878', '100 Victoria St, Singapore 188064', '', '', '', '2017-02-18 12:57:21', '3p39duz8emlmwhj', 'gps', 1, 1, 1, 0, 0, 0),
(148, 42, 1, '', 'home', 'UNDNF2', '31.464652500000003', '74.28347899999994', '644 Service Rd, Lahore 54000, Pakistan', '', '', '', '2017-02-18 15:12:10', '2rrx2vx6rp58l2a', 'gps', 1, 1, 1, 0, 0, 0),
(149, 43, 1, '', 'ppp', 'UND43H', '31.4086', '74.39190000000008', 'Unnamed Road, Bahler, Pakistan', '', '', '', '2017-02-18 15:25:18', 'bqss8vuatvxmhz3', 'ip', 1, 1, 1, 0, 0, 0),
(150, 30, 1, '', 'Home', 'UNDXA4', '1.2855', '103.85649999999998', '1 Fullerton Rd, Singapore 049213', '', '', '', '2017-02-18 15:28:22', 'p3iuzgwbgwruwtc', 'ip', 1, 0, 1, 0, 0, 0),
(151, 30, 1, '', 'Home', 'UND8B8', '1.3113191000000002', '103.8610099', '20 Hamilton Rd, Singapore 209190', '', '', '', '2017-02-18 15:30:47', 'zfkmhnvymuj9h55', 'gps', 1, 0, 1, 0, 0, 0),
(152, 44, 1, '', 'Home', 'UND1IS', '1.3113017', '103.86100420000002', '18 Hamilton Rd, Singapore 209188', '', '', '', '2017-02-18 15:51:54', 'tpz971denttda7k', 'gps', 1, 1, 1, 0, 0, 0),
(153, 1, 1, '', 'Pomio', 'AFGH12', '42.27279202442584', '-71.73116815170249', '1500 Arch St, Philadelphia, PA 19102, USA', '', 'O', '', '2017-02-19 02:41:27', 'iq6ljdmvw2i789j', 'ip', 1, 1, 1, 0, 0, 0),
(154, 45, 1, '', 'Afghan', 'AFGH11', '42.26402731555255', '-71.78918731163935', '122 Shrewsbury St, Worcester, MA 01605, USA', '', '', '', '2017-02-19 02:48:12', 'cs5acpi19awqzx8', 'gps', 1, 0, 1, 0, 0, 0),
(155, 1, 1, '', 'Home', 'UND64E', '42.27242669', '-71.73228684000003', '2 Marie St, Shrewsbury, MA 01545, USA', '', '', '', '2017-02-19 03:47:57', 'eif9z18fvdsq25i', 'gps', 1, 1, 1, 0, 0, 0),
(156, 10, 1, '', 'Starbucks', 'DERS12', '42.28433587', '-71.60996076000004', '140 Turnpike Rd, Westborough, MA 01581, USA', '', '', '', '2017-02-20 01:11:54', 'yhnqul8gr77wr6r', 'gps', 1, 1, 1, 0, 0, 0),
(157, 11, 1, '', 'home', 'UNDH3P', '43.44562049', '-80.47696776999999', '31 Fairview Ave, Kitchener, ON N2H 3E7, Canada', '', '', '', '2017-02-20 06:00:22', 'ru578jrhlzse6xg', 'gps', 1, 0, 1, 0, 0, 0),
(161, 48, 1, '', 'Home', 'HERE21', '42.2865', '-71.7147', '28 Kenilworth Rd, Shrewsbury, MA 01545, USA', '', '01547', 'MA', '2017-02-23 16:18:58', '5envbcjxyfvlf41', 'ip', 1, 1, 0, 0, 0, 0),
(164, 49, 1, '', 'Work', 'WAQ9AM', '31.464645100000002', '74.28343910000001', '644 Service Rd, Lahore 54000, Pakistan', '', '', '', '2017-02-24 07:38:43', '351jv5np4uhzhif', 'gps', 1, 1, 0, 0, 0, 0),
(167, 50, 1, '', 'asdasd', 'UND7W2', '31.4215', '73.08429999999998', 'Zila Council Rd, Faisalabad, Pakistan', '', '', '', '2017-02-27 14:00:54', 'maw4tznp7zuf8c1', 'ip', 1, 0, 0, 0, 0, 0),
(168, 50, 1, '', 'cccc', 'UNDKR4', '31.554606099999994', '74.35715809999999', '132 Allama Iqbal Rd, Lahore, Pakistan', '', '', '', '2017-02-27 14:01:55', 'gmxg6wjw22ni6xa', 'gps', 1, 0, 0, 0, 0, 0),
(169, 50, 1, '', 'dadas', 'UNDM2M', '31.554606099999994', '74.35715809999999', '132 Allama Iqbal Rd, Lahore, Pakistan', '', '', '', '2017-02-27 14:03:37', 'tpmgbm6wx7pl7xy', 'gps', 1, 0, 0, 0, 0, 0),
(170, 51, 1, '', '5555', 'UND15Y', '31.554606099999994', '74.35715809999999', '132 Allama Iqbal Rd, Lahore, Pakistan', '', '', '', '2017-02-27 14:04:20', 'jdca12fww5fv36w', 'gps', 1, 0, 1, 0, 0, 0),
(171, 51, 1, '', 'xzczx', 'UNDH37', '31.554606099999994', '74.35715809999999', '132 Allama Iqbal Rd, Lahore, Pakistan', '', '', '', '2017-02-27 14:06:38', 'ji4tqrseaf4lmi9', 'gps', 1, 1, 0, 0, 0, 0),
(172, 48, 1, '', 'Home', 'AFGH23', '42.27450054866311', '-71.73161919154052', '15 Sheridan Dr, Shrewsbury, MA 01545, USA', '', '', '', '2017-02-27 16:02:01', 't9r9wuqk4pimjvb', 'ip', 1, 1, 1, 0, 0, 0),
(173, 48, 1, '', 'Home', 'UND1XC', '0', '0', '', '', '', '', '2017-02-28 03:02:38', 'qrc9qw6fjaz762y', 'ip', 0, 0, 1, 0, 0, 0),
(175, 52, 1, '', 'Olo', 'UNDI36', '31.4086', '74.39190000000008', 'Unnamed Road, Bahler, Pakistan', '', '', '', '2017-03-01 07:56:12', 'yuty93j2uplgbnx', 'ip', 1, 0, 0, 0, 0, 0),
(176, 53, 1, '', 'Library', 'BUCK66', '42.48665902597482', '-71.43372711076262', '486 Main St, Acton, MA 01720, USA', '', '', '', '2017-03-01 19:25:02', '94h1365pt6xptfz', 'gps', 1, 1, 1, 0, 0, 0),
(177, 1, 1, '', 'Acton Library', 'ACTO12', '42.486643199999996', '-71.43367820000003', '486 Main St, Acton, MA 01720, USA', '', '', '', '2017-03-01 19:31:47', 'mz3meti49h82mb3', 'gps', 1, 1, 1, 0, 0, 0),
(178, 1, 1, '', 'Home', 'UNDSX8', '42.27251616', '-71.73223033', '28 Stone Ave, Shrewsbury, MA 01545, USA', '', '', '', '2017-03-02 15:34:52', 'ie9qkvwp8ljvzqe', 'gps', 1, 0, 1, 0, 0, 0),
(179, 1, 1, '', 'Home', 'UND889', '42.2865', '-71.7147', '28 Kenilworth Rd, Shrewsbury, MA 01545, USA', '', '', '', '2017-03-03 15:45:17', 'n132mr1c65favdj', 'ip', 1, 0, 1, 0, 0, 0),
(180, 54, 1, '', 'Lahore', 'UND72E', '31.464617099999998', '74.28346959999999', '644 Service Rd, Lahore 54000, Pakistan', '', '', '', '2017-03-08 12:21:34', '518gti9s4ffmf2r', 'gps', 1, 1, 1, 0, 0, 0),
(181, 55, 1, '', 'lahore', 'SABB2J', '0', '0', 'johar town , lahore', '', '', '', '2017-03-09 06:52:53', 'yrivcjcu4g3577p', 'ip', 0, 0, 1, 0, 0, 0),
(182, 55, 1, '', 'lahore', 'UNDY1G', '0', '0', 'johar town , lahore', '', '', '', '2017-03-09 07:04:08', 'asep5sxxh8fk6j5', 'ip', 0, 0, 1, 0, 0, 0),
(183, 55, 1, '', 'lahore', 'SABV8A', '0', '0', 'johar town , lahore', '', '', '', '2017-03-09 07:06:11', 'fdxt4a87tq2z2e6', 'ip', 0, 0, 0, 0, 0, 0),
(184, 55, 1, '', 'lahore', 'SABJ2I', '0', '0', 'johar town , lahore', '', '', '', '2017-03-09 07:09:04', 'nim4pucgsbtapzc', 'ip', 0, 0, 0, 0, 0, 0),
(185, 55, 1, '', 'lahore', 'UND16Y', '0', '0', 'faisal town', '', '', '', '2017-03-09 07:10:58', '77am2a4rrkyhudn', 'ip', 0, 0, 1, 0, 0, 0),
(186, 54, 1, '', 'lahore', 'ABEH43', '0', '0', 'wapda town', '', '', '', '2017-03-09 07:29:45', 'z31a2kyj6gta3d6', 'ip', 0, 1, 1, 0, 0, 0),
(187, 11, 1, '', 'home', 'UNDZZ1', '43.44548517', '-80.47707743000001', '31 Fairview Ave, Kitchener, ON N2H 3E7, Canada', '', '', '', '2017-05-28 05:22:03', '85kdgale9fq146g', 'gps', 1, 0, 1, 0, 0, 0),
(188, 10, 1, '', 'FGF', 'FGF223', '37.9690446', '-121.67164560000003', '1360 Delta Rd, Knightsen, CA 94548, USA', '', '', '', '2017-05-31 01:03:15', '7fy5lj7bwgdjvae', 'gps', 1, 0, 1, 0, 0, 0),
(189, 10, 1, '', 'Martinez farmers market', 'MAR223', '38.0174233', '-122.13636020000001', '701 Main St, Martinez, CA 94553, USA', '', '', '', '2017-07-09 18:17:44', 'p3fcypqml86eer3', 'gps', 1, 1, 1, 0, 0, 0),
(190, 56, 1, '', 'Home', 'VIPQ64', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', '', '', '', '2017-07-21 07:37:01', 'ju1kjq2x597jbf2', 'gps', 1, 1, 0, 0, 0, 0),
(197, 10, 1, '', 'Starbucks', 'UNDAJ8', '37.9267947', '-121.69100909999997', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', '', '', '', '2017-07-26 02:55:48', 'bjdvqfbe4mr922g', 'gps', 1, 0, 1, 0, 0, 0),
(198, 59, 1, '', 'palasia', 'UND4R3', '22.726282691810592', '75.88519957594337', '6/4, New Palasia, Indore, Madhya Pradesh 452001, India', '', '', '', '2017-07-27 13:28:42', 'lbky1ruipg6k9ad', 'gps', 1, 0, 0, 0, 0, 0),
(201, 60, 1, '', 'indore', 'UNDRB6', '22.726750199999998', '75.88422860000003', 'Gold Stone Building, Lala Banarasilal Dawar Marg, New Palasia Extension, Indore, Madhya Pradesh 452001, India', '', '', '', '2017-07-31 13:12:00', '45wgy86gq9qahn2', 'gps', 1, 0, 0, 0, 0, 0),
(202, 61, 1, '', 'indore', 'UNDK81', '22.726743799999998', '75.88426690000006', 'Gold Stone Building, 2/1, Lala Banarasilal Dawar Marg, Near UCO Bank, Near Palasia, 56 Dukan, New Palasia, Indore, Madhya Pradesh 452001, India', '', '', '', '2017-07-31 13:15:59', 'ckr3u83pyx6kmx2', 'gps', 1, 1, 1, 0, 0, 0),
(208, 56, 1, '', 'indore', 'UND6TE', '22.722365766386623', '75.8853070529176', 'Devdarshan Building, 20/1,101, Agra Bombay Rd, Opposite Sanghi Motors, Near Palasia Square, South Tukoganj, Indore, Madhya Pradesh 452001, India', '', '', '', '2017-08-02 06:37:04', 'sd5absw1pi6g1qf', 'gps', 1, 1, 0, 0, 0, 0),
(212, 1, 2, '', 'Starbucks Brentwood', '3MHERE', '37.92671524', '-121.69095926', '8640 Brentwood Blvd, Brentwood, CA 94513, USA', '', '', '', '2017-08-04 22:34:32', '9sfl17furibkwyw', 'gps', 1, 1, 1, 0, 0, 0),
(213, 62, 1, '', 'Home', 'UNDQ3D', '22.5', '75.5', '2328 Grant St, Berkeley, CA 94703, USA', '', '', '', '2017-08-04 23:40:51', 'we9227ufjfcyu6k', 'ip', 1, 1, 1, 0, 0, 0),
(214, 1, 1, 'Amr Samir', 'Starbucks', '123CBA', '38.0193657', '-122.13413209999999', '600-698 Pine St, Martinez, CA 94553, USA', '', '', 'Near to the gas station', '2017-08-06 01:09:31', 'uizy3mc63xgzkmm', 'gps', 1, 1, 0, 0, 0, 0),
(215, 1, 1, '', 'FGF', 'UTUTR1', '49.96452011773824', '-9.14288367528809', '1230 Delta Road, Knightsen', '', '94548', 'CA', '2017-08-06 01:35:47', '43dvenz4ua4qnsj', 'gps', 1, 1, 1, 0, 0, 0),
(220, 63, 1, '', 'home', 'UND93V', '22.72193', '75.90669000000002', 'Service Rd, Shri Ram Nagar, Indore, Madhya Pradesh 452016, India', '', '', '', '2017-08-09 05:39:03', '39ybd6z7xyid8t5', 'gps', 1, 1, 0, 0, 0, 0),
(221, 56, 1, '', 'home', 'UND7QD', '22.72672', '75.88432', 'Gold Stone Building, 2/1, Lala Banarasilal Dawar Marg, Near UCO Bank, Near Palasia, 56 Dukan, New Palasia, Indore, Madhya Pradesh 452001, India', '', '', '', '2017-08-09 06:58:41', 'mswqihu9rn2w9ti', 'gps', 1, 1, 0, 0, 0, 0),
(222, 56, 1, '', 'Home', 'UNDUJ3', '22.5', '75.5', 'Street2, Hukumchand Colony, Indore, Madhya Pradesh 452006, India', '', '', '', '2017-08-22 07:00:48', 'a22g3hinfeugjm3', 'ip', 1, 1, 0, 0, 0, 0),
(227, 64, 1, '', 'dfdf', 'DEMGX1', 'NaN', 'NaN', 'DEVENDRA SHARMA, Jaipur, Rajasthan, India', '', '', '', '2017-08-11 14:31:21', 'bfyzqxlcpxc9416', 'ip', 1, 0, 1, 0, 0, 0),
(228, 1, 1, '', 'Fgf', '1MHERE', '37.9326443', '-121.6943776', '238 Oak St, Brentwood, CA 94513, USA', '', '', '', '2017-08-11 20:31:28', 'sf776dsf5elthrc', 'gps', 1, 1, 1, 0, 0, 0),
(229, 65, 2, '', 'Home', 'DEM1JV', 'NaN', 'NaN', 'Palasia, Indore, Madhya Pradesh, India', '', '', '', '2017-08-14 10:07:37', 'ttnkl364llzuyz2', 'ip', 1, 0, 1, 0, 0, 0),
(230, 66, 1, '', 'dfdf', 'FD@M27', 'NaN', 'NaN', 'Dubai - United Arab Emirates', '', '', '', '2017-08-14 10:12:25', '84f86lcnzivbl2f', 'ip', 1, 0, 0, 0, 0, 0),
(231, 61, 1, '', '56 shop', 'UND37Z', '23.0333', '72.6167', '42/41, Anil Starch Rd, Dhobi Ghat, Kailash Nagar, Bapunagar, Ahmedabad, Gujarat 380038, India', '', '', '', '2017-08-16 09:58:39', 'q8la4jaf9repyjl', 'ip', 0, 1, 1, 0, 0, 0),
(233, 67, 2, '', 'indore', 'UNDM48', '22.731538081923375', '75.8737189851197', '10, Yeshwant Niwas Rd, Shivaji Nagar, Indore, Madhya Pradesh 452003, India', '', '', '', '2017-08-21 11:24:57', 'v8n4m7u5uniq73v', 'gps', 1, 1, 0, 0, 0, 0),
(235, 68, 2, '', 'Microsoft', 'UND4Y7', '22.7167', '75.83330000000001', 'Street2, Hukumchand Colony, Indore, Madhya Pradesh 452006, India', '', '', '', '2017-08-21 13:21:05', '8586nrphvn4rh1h', 'gps', 1, 0, 1, 0, 0, 0),
(236, 1, 1, '', 'Hone', 'UND2X3', '37.96913246', '-121.67156635000003', '1360 Delta Rd, Knightsen, CA 94548, USA', '', '', '', '2017-08-21 13:33:35', 'bwci3ismmyryn2z', 'gps', 1, 1, 1, 0, 0, 0),
(237, 1, 1, '', 'Home', 'UND764', '37.96913233', '-121.67155680000002', '1360 Delta Rd, Knightsen, CA 94548, USA', '', '', '', '2017-08-21 13:34:34', 'itzpiengauyfmpl', 'gps', 1, 1, 1, 0, 0, 0),
(238, 10, 1, '', 'Hi', 'UND6B9', '37.9685788', '-121.67130029999998', '1401 Delta Rd, Knightsen, CA 94548, USA', '', '', '', '2017-08-21 13:36:03', '3nwjaw5eem4dmpe', 'gps', 1, 1, 1, 0, 0, 0),
(239, 69, 1, '', 'home', 'UNDU68', '22.7167', '75.83330000000001', 'Street2, Hukumchand Colony, Indore, Madhya Pradesh 452006, India', '', '', '', '2017-08-21 13:36:41', 'hmwqit6qu94va2a', 'gps', 1, 0, 1, 0, 0, 0),
(240, 10, 1, '', 'H', 'UND57R', '37.9685788', '-121.67130029999998', '1401 Delta Rd, Knightsen, CA 94548, USA', '', '', '', '2017-08-21 13:36:57', '9sq25wcvpj57y5g', 'gps', 1, 0, 1, 0, 0, 0),
(241, 70, 1, '', 'Home', 'UND9N8', '22.7167', '75.83330000000001', 'Street2, Hukumchand Colony, Indore, Madhya Pradesh 452006, India', '', '', '', '2017-08-21 13:45:42', '5wvbnae8mtnu81z', 'ip', 1, 1, 0, 0, 0, 0),
(242, 70, 1, '', 'work', 'UNDX4B', '22.7167', '75.8333', 'Street2, Hukumchand Colony, Indore, Madhya Pradesh 452006, India', '', '', '', '2017-08-21 13:48:06', '27t9cstx2znpjut', 'ip', 0, 1, 0, 0, 0, 0),
(243, 67, 1, '', 'dfdf', 'VISL94', 'NaN', 'NaN', 'Virginia Beach, VA, United States', '', '', '', '2017-08-21 13:55:09', 'zawkl7j23rkum4g', 'ip', 1, 0, 1, 0, 0, 0),
(245, 59, 1, '', 'my home', 'UND1W3', '22.726864801420245', '75.88424537360629', 'Gold Stone Building, 302, 3/5, Lala Banarasilal Dawar Marg, Above Divine Jewellers, Near 56 Dukan, New Palasia, Indore, Madhya Pradesh 452001, India', '', '', '', '2017-08-21 13:56:37', 'f7ndpbnr7k83nwm', 'gps', 1, 0, 0, 0, 0, 0),
(246, 71, 1, '', 'tt uu', 'UNDWP2', '22.726745', '75.88429999999994', 'Gold Stone Building, 2/1, Lala Banarasilal Dawar Marg, Near UCO Bank, Near Palasia, 56 Dukan, New Palasia, Indore, Madhya Pradesh 452001, India', '', '', '', '2017-08-21 14:00:21', 'sfi74sjwk53alwp', 'gps', 1, 0, 0, 0, 0, 0),
(247, 64, 1, '', 'work', 'DEM2S8', 'NaN', 'NaN', 'Detroit, MI, United States', '', '', '', '2017-08-21 14:01:52', 'jz67pk467r18jcr', 'ip', 1, 0, 1, 0, 0, 0),
(248, 64, 1, '', 'Demo', 'DEMA9K', '20', '77', 'Bhagwat - Gugal Pimpari Rd, Gugal Pimpari, Maharashtra 431703, India', '', '', '', '2017-08-21 14:03:27', '1hpmcyeqfl66igp', 'ip', 1, 0, 0, 0, 0, 0),
(249, 64, 1, '', 'Dhjs', 'DEM1A4', '20', '77', 'Bhagwat - Gugal Pimpari Rd, Gugal Pimpari, Maharashtra 431703, India', '', '', '', '2017-08-21 14:05:37', 'ykpwt7apl91c8ea', 'ip', 1, 0, 1, 0, 0, 0),
(250, 72, 1, '', 'Dhjs', 'DEM1A4', '20', '77', 'Bhagwat - Gugal Pimpari Rd, Gugal Pimpari, Maharashtra 431703, India', '', '', '', '2017-08-21 14:05:50', 'apy3j5ag9je8n1k', 'ip', 1, 0, 1, 0, 0, 0),
(252, 73, 1, '', 'demo', 'DEM1FU', 'NaN', 'NaN', 'Dubai - United Arab Emirates', '', '', '', '2017-08-21 14:20:38', 'g27crji9lv6truv', 'ip', 1, 0, 1, 0, 0, 0),
(253, 74, 2, '', 'indore', 'UNDU2H', '22.7167', '75.83330000000001', 'Street2, Hukumchand Colony, Indore, Madhya Pradesh 452006, India', '', '', '', '2017-08-21 14:21:01', 'fswde3nr5r6uc5f', 'ip', 1, 0, 0, 0, 0, 0),
(254, 75, 1, '', 'home', 'UNDU2Z', '22.7167', '75.83330000000001', 'Street2, Hukumchand Colony, Indore, Madhya Pradesh 452006, India', '', '', '', '2017-08-21 14:22:14', 'zuif6k95dnetmir', 'gps', 1, 1, 0, 0, 0, 0),
(255, 76, 1, '', 'Hss', 'UNDX5C', '20', '77', 'Bhagwat - Gugal Pimpari Rd, Gugal Pimpari, Maharashtra 431703, India', '', '', '', '2017-08-21 14:27:01', 'eya2b4sjjjk9pyc', 'ip', 1, 0, 0, 0, 0, 0),
(256, 1, 2, '', 'Work', 'UNDUH7', '37.9690404', '-121.67169849999999', '1360 Delta Rd, Knightsen, CA 94548, USA', '', '', '', '2017-08-21 18:44:18', 'g1cy68ysyxx4fwz', 'gps', 1, 1, 1, 0, 0, 0),
(257, 77, 2, '', 'Home', 'UND72S', '22.7167', '75.83330000000001', 'Street2, Hukumchand Colony, Indore, Madhya Pradesh 452006, India', '', '', '', '2017-08-22 04:57:30', 'gb1fdycbetel9jn', 'gps', 1, 1, 1, 0, 0, 0),
(259, 78, 1, '', 'My Test Pin', 'UNDD2J', '22.7167', '75.83330000000001', 'Street2, Hukumchand Colony, Indore, Madhya Pradesh 452006, India', '', '', '', '2017-08-22 12:43:50', 'ca5bc57xs7x3cxl', 'gps', 1, 0, 0, 0, 0, 0),
(262, 1, 1, '', 'Bethel Island market', 'UND31G', '38.01505234', '-121.64011899000002', '3124-3134 Ranch Ln, Bethel Island, CA 94511, USA', 'US', '', '', '2017-08-23 22:22:44', 'ndudk7spvgg9taw', 'gps', 1, 1, 1, 0, 0, 0),
(263, 79, 1, '', 'Lisa and Will''s home', 'LENZ13', '37.97067200000001', '-121.67309599999999', '1230 Delta Rd, Brentwood, CA 94513, USA', 'US', '', '', '2017-08-24 21:19:50', 'vrx9usyhm31ux3a', 'ip', 1, 1, 0, 0, 0, 0),
(264, 80, 1, '', 'Home', 'UND31P', '37.96891277659495', '-121.6716315877826', '1360 Delta Rd, Knightsen, CA 94548, USA', 'US', '', '', '2017-08-24 23:05:21', '1zqm6a5gctp83x3', 'gps', 1, 0, 1, 0, 0, 0),
(265, 10, 1, '', 'Ho', '12345O', '37.96876594191456', '-121.67221596486058', '1355-1401 Delta Rd, Knightsen, CA 94548, USA', 'US', '', '', '2017-08-24 23:11:13', 'mhiqf4qdjud1lay', 'gps', 1, 1, 1, 0, 0, 0),
(266, 1, 1, '', 'Test', 'UNDRU3', '37.9685788', '-121.67130029999998', '1401 Delta Rd, Knightsen, CA 94548, USA', 'US', '', '', '2017-08-25 00:03:07', 'z1brh317efvgj65', 'gps', 1, 1, 1, 0, 0, 0),
(268, 69, 1, '', 'Test 1q23444', 'UND929', '22.7167', '75.83330000000001', 'Street2, Hukumchand Colony, Indore, Madhya Pradesh 452006, India', 'IN', '', '', '2017-08-25 11:26:40', 'rkr25ertahpwgle', 'gps', 1, 0, 1, 0, 0, 0),
(269, 69, 1, '', 'testttttt', 'UND4I2', '22.7167', '75.83330000000001', 'Street2, Hukumchand Colony, Indore, Madhya Pradesh 452006, India', 'IN', '', '', '2017-08-25 11:27:39', 'vshtrfc3h832hv5', 'gps', 1, 1, 0, 0, 0, 0),
(270, 81, 2, '', 'indore', 'UND2HA', '22.732290136290526', '75.88311744550788', '63, Ambedkar Nagar, New Palasia, Indore, Madhya Pradesh 452001, India', 'IN', '', '', '2017-08-25 12:15:45', '4y8z1x8qgewfqpf', 'gps', 1, 1, 0, 0, 0, 0),
(271, 1, 1, '', 'Home', 'UND999', '37.9687152', '-121.67116390000001', '1401 Delta Rd, Knightsen, CA 94548, USA', 'US', '', '', '2017-08-25 17:32:19', 'rd8ul29lkex8qq2', 'gps', 1, 1, 1, 0, 0, 0),
(272, 1, 1, '', 'Home', 'UND911', '37.95672431024803', '-121.65230906440422', '2340 Sunset Rd, Brentwood, CA 94513, USA', 'US', '', '', '2017-08-25 17:41:10', 'neusmyim1xlf5ch', 'gps', 1, 1, 1, 0, 0, 0),
(273, 10, 1, '', 'Home', 'UND888', '37.96903146434386', '-121.67171766992817', '1360 Delta Rd, Knightsen, CA 94548, USA', 'US', '', '', '2017-08-25 18:10:31', 's7ga4vhptjgmtjc', 'gps', 1, 1, 1, 0, 0, 0),
(274, 1, 1, 'Amr Samir', 'home', 'UNDAR2', '37.9682531', '-121.669578', '1479 Delta Rd, Knightsen, CA 94548, USA', 'US', '94548', 'CA', '2017-08-25 18:21:20', 'bkt357h61f9369z', 'gps', 1, 1, 1, 0, 0, 0),
(275, 82, 1, '', 'Home', 'DEV33M', 'NaN', 'NaN', 'Vijay Nagar, Indore, Madhya Pradesh, India', '', '', '', '2017-08-28 05:25:31', '7bjnln9zewmsmaz', 'ip', 1, 0, 0, 0, 0, 0),
(279, 61, 2, '', 'indore', 'UNDZ4B', '22.7167', '75.83330000000001', 'Street2, Hukumchand Colony, Indore, Madhya Pradesh 452006, India', 'IN', '', '', '2017-08-28 11:34:48', '7rfgfcxkhfeezr4', 'ip', 1, 1, 1, 0, 0, 0),
(280, 83, 2, '', 'Indore', 'UND7EJ', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-08-28 11:34:54', '8dls8b1v6nbaggj', 'gps', 1, 1, 0, 0, 0, 0),
(281, 61, 2, '', 'indore', 'UND4SM', '22.7167', '75.83330000000001', 'Street2, Hukumchand Colony, Indore, Madhya Pradesh 452006, India', 'IN', '', '', '2017-08-28 11:38:06', 'tstdba76qvte735', 'ip', 1, 1, 0, 0, 0, 0),
(287, 61, 2, '', 'indore', 'UND7GN', '22.7167', '75.83330000000001', 'Street2, Hukumchand Colony, Indore, Madhya Pradesh 452006, India', 'IN', '', '', '2017-08-28 14:43:18', 'ltu1ajbgbfkhm37', 'ip', 1, 1, 0, 0, 0, 0),
(313, 84, 1, 'bhopal Pin', 'bhopal', 'UNDR31', '22.71790610389554', '75.86309021802981', '162, Ranipura Main Rd, Nayi Bagad, Siyaganj, Indore, Madhya Pradesh 452007, India', 'IN', '462045', 'Bangrasia', '2017-08-31 10:02:43', 'ncpkwah3dzi6ysc', 'ip', 1, 1, 0, 1504173763, 0, 0),
(314, 84, 1, '', 'bhopal', 'UNDD91', '22.7167', '75.8333', 'Street2, Hukumchand Colony, Indore, Madhya Pradesh 452006, India', 'IN', '', '', '2017-08-31 13:37:46', '7e4r2mcnw952iev', 'ip', 0, 1, 0, 1504186666, 0, 0),
(315, 84, 2, '', 'Home', 'PRA2E7', '23.2677604', '77.4140013', 'Cloak Room, Navbahar Colony, Bhopal, Madhya Pradesh 462001, India', 'IN', '', '', '2017-08-31 14:03:05', '6dw7gdzwr5rbnw7', 'ip', 1, 0, 1, 1504188185, 0, 0),
(316, 1, 1, '', 'Home', 'UNDAR1', '34.049485', '-118.18990200000002', '1100 N Ditman Ave, Los Angeles, CA 90063, USA', 'US', '', '', '2017-08-31 19:12:25', 'hrrhjh65986ctxj', 'gps', 1, 1, 1, 1504206745, 0, 0),
(317, 10, 1, '', 'Hometown', 'UNDV28', '37.926672429856374', '-121.6910038418726', '8640 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-08-31 23:13:10', 'yqphuw75bnrrpx3', 'gps', 1, 1, 1, 1504221190, 0, 0),
(318, 1, 1, '', 'Home again', 'UND17R', '37.9267774', '-121.69094230000002', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-08-31 23:54:54', 'h3ksxb5m14ip8t4', 'gps', 1, 1, 1, 1504223694, 0, 0),
(319, 62, 1, '', 'Starbucks', 'UND12W', '37.926793', '-121.6909789', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-01 00:05:15', '1b5kgzv5ndell2y', 'gps', 1, 1, 1, 1504224314, 0, 0),
(320, 10, 1, '', 'Home', 'UND67D', '37.9267894', '-121.69096460000003', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-01 00:12:27', 'hmeqp2gzr3ye2uw', 'gps', 1, 0, 1, 1504224747, 0, 0),
(321, 1, 1, '', 'Home', 'UNDD37', '37.926793', '-121.6909789', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-01 00:12:43', 'i3rny6j82j13543', 'gps', 1, 0, 1, 1504224763, 0, 0),
(323, 1, 1, '', 'Retreat', 'AVO927', '37.9267774', '-121.69094230000002', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-04 23:48:48', 'pf4mnr9f7ru1jwd', 'gps', 1, 1, 1, 1504568928, 0, 0),
(324, 1, 1, '', 'Homes2Rent', 'AVOL44', '37.9267454', '-121.69095800000002', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-07 02:20:28', '38srnfa5j7e33vw', 'gps', 1, 1, 1, 1504750828, 0, 0),
(325, 85, 2, '', 'Indore', 'UND7U2', '22.719052922637484', '75.88339725449214', '126, Kanchan Bagh, South Tukoganj, Tukoganj, Indore, Madhya Pradesh 452001, India', 'IN', '', '', '2017-09-07 06:44:01', '62bgifwbswmyxhd', 'gps', 1, 1, 0, 1504766641, 0, 0),
(326, 1, 1, '', 'HSBC FC', 'UND222', '37.8006891', '-122.27109339999998', '388 9th St, Oakland, CA 94607, USA', 'US', '', '', '2017-09-07 20:53:09', '6w1kk3hzt1ns8lg', 'gps', 1, 0, 1, 1504817589, 0, 0),
(327, 86, 1, '', 'Starbucks', 'DOREE1', '37.9267774', '-121.69094230000002', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-10 01:41:49', '3qnze6wegkrnz4f', 'gps', 1, 0, 0, 1505007708, 0, 0),
(328, 87, 1, '', 'Homey', 'NATE89', '37.93466867369506', '-121.68859219995193', '302 Oak St, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-10 22:56:51', '6npu5aujt7ix2nf', 'gps', 1, 1, 0, 1505084211, 0, 0),
(329, 48, 1, '', 'Starbucks', 'UND333', '37.9267467', '-121.69092009999997', '8640 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-10 23:05:52', 'z6d286b1rjmb7a2', 'gps', 1, 0, 0, 1505084752, 0, 0),
(330, 67, 2, '', 'indore', 'UNDF4P', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-11 06:22:21', 'duvx7nf4ds6zcjs', 'gps', 1, 1, 0, 1505110941, 0, 0),
(331, 10, 1, '', 'Home', 'DHL111', '37.9690404', '-121.67169849999999', '1360 Delta Rd, Knightsen, CA 94548, USA', 'US', '', '', '2017-09-11 17:23:51', '4vnpq2vxnnt2h3l', 'gps', 1, 0, 1, 1505150631, 0, 0),
(332, 88, 1, '', 'Home', 'UND888', '37.926736674306845', '-121.69101775635193', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-11 22:47:28', 'j75bgkpa7j52njm', 'gps', 1, 0, 0, 1505170048, 0, 0),
(333, 10, 1, '', 'Home', 'UND777', '37.92664941869483', '-121.69087434198866', '8640 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-11 22:48:22', '3xd3d9zb2dqse8a', 'gps', 1, 1, 1, 1505170102, 0, 0),
(334, 10, 1, '', 'Home', 'UND788', '37.9267521', '-121.69096460000003', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-11 22:58:45', 'ukj9vka4rxv5eda', 'gps', 1, 0, 1, 1505170725, 0, 0),
(335, 11, 1, '', 'home', 'UNDE6V', '45.31236293', '-75.92594179999998', '104 Goldridge Dr, Kanata, ON K2T 1G1, Canada', 'CA', '', '', '2017-09-12 03:14:45', 's5ty9s16z47367z', 'gps', 1, 0, 1, 1505186085, 0, 0),
(338, 89, 1, '', 'Home', 'UNDJ1B', '37.9690419', '-121.6717013', '1360 Delta Rd, Knightsen, CA 94548, USA', 'US', '', '', '2017-09-13 06:11:33', 'wmzckd1vpl7xlrb', 'gps', 1, 0, 0, 1505283093, 0, 0),
(341, 1, 2, '', 'work', 'UNDR88', '37.9697592', '-121.67296679999998', '1230 Delta Rd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-15 22:22:08', 'fdi1ymrqs7i6yrs', 'gps', 1, 1, 1, 1505514128, 0, 0),
(342, 1, 2, 'Vodafone', 'Roxy', 'UND311', '37.9697588', '-121.67296699999997', '1230 Delta Rd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-15 22:24:01', 'dwud546h7q9zhqf', 'gps', 1, 1, 1, 1505514241, 0, 0);
INSERT INTO `pins` (`pin_id`, `user_id`, `pin_type`, `name`, `location_name`, `code`, `lat`, `long`, `address`, `country`, `postal_code`, `landmark`, `date_created`, `activation_code`, `mode`, `is_gps`, `is_active`, `is_deleted`, `authorize_time`, `link_click`, `mail_send`) VALUES
(343, 1, 2, 'Vodafone', 'Helio', 'UNDR22', '-7.5360639', '112.23840169999994', 'Jl. Kemuning, Candi Mulyo, Kec. Jombang, Kabupaten Jombang, Jawa Timur 61419, Indonesia', 'ID', '', '', '2017-09-15 22:27:17', 'u7sdnefsi5chqtl', 'gps', 1, 1, 0, 1505514437, 0, 0),
(344, 1, 2, 'Vodafone', 'Home', 'UNDR33', '29.914002', '31.1940429', 'Attia khattab, Abu Sir, Al Badrashin, Giza Governorate, Egypt', 'EG', '', '', '2017-09-15 22:29:18', 'vu1ybmvbbhtun64', 'gps', 1, 0, 1, 1505514558, 0, 0),
(345, 1, 2, 'Orange', 'home', 'UNDR44', '37.969759499999995', '-121.6729689', '1230 Delta Rd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-15 22:36:19', 'vkj8ljsypj9yutl', 'gps', 1, 1, 1, 1505514979, 0, 0),
(346, 10, 1, '', 'Uncle''s home', 'BEN222', '40.6985596', '-73.99400000000003', '45 Pineapple St, Brooklyn, NY 11201, USA', 'US', '', '', '2017-09-16 01:06:44', '9hbhhek9ew3ym68', 'gps', 1, 1, 0, 1505524004, 0, 0),
(347, 1, 1, '', 'indore', 'UNDEX6', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-18 09:40:46', 'nbd5mhwza917nli', 'gps', 1, 1, 0, 1505727646, 0, 0),
(354, 57, 1, '', 'indore', 'UND4LF', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-18 10:02:16', 'n7eulenn1vdyzkq', 'gps', 1, 0, 1, 1505728936, 0, 0),
(355, 57, 1, '', 'fas', 'UND7M4', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-18 10:05:38', 'j6bapfdcy2zgz23', 'gps', 1, 0, 1, 1505729138, 0, 0),
(356, 57, 1, '', 'sdfsdf', 'UNDD6F', '22.729242311431953', '75.85951400617682', 'B-50, Shivalaya Marg, DRP Line, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-18 10:05:49', 'x8fi74x8czhxhpb', 'gps', 1, 1, 0, 1505729149, 0, 0),
(357, 57, 1, '', 'sdf', 'UND1L8', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-18 10:06:00', 'cdvbk3qyppm14mf', 'gps', 1, 0, 1, 1505729160, 0, 0),
(358, 57, 1, '', 'sdfsd', 'UND6TC', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-18 10:06:40', '98if5f5w8wcrp6l', 'gps', 1, 0, 1, 1505729200, 0, 0),
(359, 57, 2, 'Vodafone', 'df', 'UNDU52', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-18 10:17:03', '9vk48zbt4xgi3xx', 'gps', 1, 1, 0, 1505729823, 0, 0),
(360, 57, 2, '', 'sdf', 'UND4QK', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-18 10:18:31', '8hfy4jpvvkf7id3', 'gps', 1, 0, 1, 1505729911, 0, 0),
(361, 1, 2, 'Vodafone', 'Work', 'UND6WN', '42.4405783', '-71.2139163', '1295 Massachusetts Ave, Lexington, MA 02420, USA', 'US', '', 'Next to the phone booth', '2017-09-18 17:59:30', 'w2jsxhqqh68qsv2', 'gps', 1, 1, 0, 1505757570, 0, 0),
(362, 1, 1, '', 'Home', 'UNDTK4', '37.9690404', '-121.67169849999999', '1360 Delta Rd, Knightsen, CA 94548, USA', 'US', '', '', '2017-09-18 18:37:26', '76pt2xenvyrkupy', 'gps', 1, 1, 0, 1505759846, 0, 0),
(363, 1, 1, '', 'Home', 'UNDKJ3', '37.9690404', '-121.67169849999999', '1360 Delta Rd, Knightsen, CA 94548, USA', 'US', '', '', '2017-09-18 18:37:40', '3h9nwug4mjexlzs', 'gps', 1, 0, 1, 1505759860, 0, 0),
(364, 1, 2, '', 'Home', 'UNDZ6T', '37.9690404', '-121.67169849999999', '1360 Delta Rd, Knightsen, CA 94548, USA', 'US', '', '', '2017-09-18 18:38:05', '3v15945gxjezmgu', 'gps', 1, 0, 1, 1505759885, 0, 0),
(365, 1, 2, '', 'Home', 'UNDRX2', '37.9690404', '-121.67169849999999', '1360 Delta Rd, Knightsen, CA 94548, USA', 'US', '', '', '2017-09-18 18:38:18', 'z8unz1pwm5mggjm', 'gps', 1, 0, 1, 1505759898, 0, 0),
(366, 1, 2, '', 'Home', 'UND8EF', '37.9690404', '-121.67169849999999', '1360 Delta Rd, Knightsen, CA 94548, USA', 'US', '', '', '2017-09-18 18:38:46', 'jjm4wr73eth47f6', 'gps', 1, 0, 1, 1505759926, 0, 0),
(367, 87, 1, '', 'Home', 'UND444', '37.96891131483623', '-121.6717074532707', '1360 Delta Rd, Knightsen, CA 94548, USA', 'US', '', '', '2017-09-18 19:05:44', 'g1jm1uegkvk63ww', 'gps', 1, 1, 0, 1505761543, 0, 0),
(368, 90, 1, '', 'Work', 'FIDUS1', '45.3484481', '-75.92526350000003', '375 Terry Fox Dr, Kanata, ON K2K 2M5, Canada', 'CA', '', '', '2017-09-18 19:43:25', 'k4q7f192eekssi3', 'gps', 1, 1, 0, 1505763805, 0, 0),
(369, 91, 1, '', 'home1', 'DEV673', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-19 06:24:44', 'v28vlufmqr1ep3z', 'gps', 1, 1, 0, 1505802284, 0, 0),
(370, 91, 1, '', 'home2', 'DEVKX8', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-19 06:25:07', 'xemh3hxzlvqapl6', 'gps', 1, 0, 0, 1505802307, 0, 0),
(371, 91, 1, '', 'home3', 'DEVI44', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-19 06:25:23', 'ly37cya1jb2lnwh', 'gps', 1, 0, 0, 1505802323, 0, 0),
(372, 91, 1, '', 'home4', 'DEV275', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-19 06:25:40', '9e8ya86sjmkkze2', 'gps', 1, 0, 0, 1505802340, 0, 0),
(373, 91, 1, '', 'home5', 'DEV15K', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-19 06:26:02', 'e583mnjkcgjedar', 'gps', 1, 0, 0, 1505802362, 0, 0),
(374, 91, 2, '', 'dfdf', 'DEVF54', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-19 06:37:49', 'n1j3ed54pscyvzq', 'gps', 1, 1, 0, 1505803069, 0, 0),
(375, 91, 2, '', 'home6', 'DEV815', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-19 06:38:11', 'tufmeub166cz24h', 'gps', 1, 1, 0, 1505803091, 0, 0),
(376, 91, 2, '', 'buisness 5', 'DEVCV7', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-19 06:41:28', 'zr8kgzjcryaqr2s', 'gps', 1, 1, 0, 1505803288, 0, 0),
(377, 91, 2, '', 'buisness', 'DEV1NF', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-19 06:53:03', 'd8imtnlrps36k1x', 'gps', 1, 0, 0, 1505803983, 0, 0),
(378, 91, 2, '', 'dfdf', 'DEVBK4', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-19 06:53:25', 'tit697hbbqr84sh', 'gps', 1, 0, 0, 1505804005, 0, 0),
(379, 91, 2, '', 'fdf', 'DEV7LY', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-19 06:53:51', 'rci8sjwgrg8n1wn', 'gps', 1, 1, 0, 1505804031, 0, 0),
(380, 91, 2, '', 'hh', 'DEVL71', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-19 06:54:20', 'ra3c7rtb4j7qa6d', 'gps', 1, 0, 0, 1505804060, 0, 0),
(381, 91, 2, '', 'ghghg', 'DEV5MH', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-19 06:54:56', 'z1qx9dcywwzvpnz', 'gps', 1, 0, 0, 1505804096, 0, 0),
(382, 91, 2, '', 'ghghg', 'DEV5MH', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-19 06:55:12', '6j8x6xwdqin6zpm', 'gps', 1, 0, 0, 1505804112, 0, 0),
(383, 91, 2, '', 'dfdf', 'DEV5VZ', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-19 06:58:30', 'c4dipb8rgv6eu8v', 'gps', 1, 0, 0, 1505804310, 0, 0),
(384, 91, 2, '', 'dfdf', 'DEV5VZ', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-19 06:59:46', 'u5iahn96tgm3u3j', 'gps', 1, 0, 0, 1505804386, 0, 0),
(385, 91, 2, '', 'dfdf', 'DEVK1E', '22.7314985', '75.88243080000007', 'Atal Dwar Main Rd, New Palasia, Indore, Madhya Pradesh 452003, India', 'IN', '', '', '2017-09-19 06:59:50', 'c4m9i7d14ngtntg', 'gps', 1, 0, 0, 1505804390, 0, 0),
(386, 10, 1, '', 'Home', 'UND222', '37.96975264329274', '-121.67281100537787', '1350 Delta Rd, Knightsen, CA 94548, USA', 'US', '', '', '2017-09-19 18:54:20', '6gn2mn3yxdf1hsk', 'gps', 1, 1, 0, 1505847260, 0, 0),
(387, 1, 2, '', 'Home', 'UND56E', '37.9267774', '-121.69094230000002', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-21 01:35:54', '5k67cdz3sqkt7cl', 'gps', 1, 0, 1, 1505957754, 0, 0),
(388, 1, 2, '', 'Home', 'UNDK4M', '37.9267767', '-121.69097569999997', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-21 01:36:11', 'vvw345qwia8vnqr', 'gps', 1, 0, 1, 1505957771, 0, 0),
(389, 1, 2, '', 'Home', 'UND6Y1', '37.9267891', '-121.69098129999998', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-21 01:36:24', 'eze25qjh2leyfdj', 'gps', 1, 0, 1, 1505957783, 0, 0),
(390, 1, 2, '', 'Home', 'UND32R', '37.9267894', '-121.69096460000003', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-21 01:36:35', '5f94ldthkjy2i7z', 'gps', 1, 0, 1, 1505957795, 0, 0),
(391, 1, 2, '', 'Home', 'UNDD64', '37.9267809', '-121.69097199999999', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-21 01:36:48', 'm3rw1dd8vuwq52g', '', 1, 0, 1, 1505957808, 0, 0),
(392, 1, 2, '', 'Home', 'UND457', '37.9267767', '-121.69097569999997', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-21 01:37:11', 'czrl1gyxmatycwt', 'gps', 1, 0, 1, 1505957831, 0, 0),
(393, 1, 2, '', 'Home', 'UNDQ67', '37.9267932', '-121.6909832', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-21 01:37:24', 'ztmxyptml21lt1p', 'gps', 1, 0, 1, 1505957844, 0, 0),
(394, 1, 2, '', 'Homestead', 'UND5ZI', '37.9267932', '-121.6909832', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-21 01:37:40', 'vap3c5n7m79gf7u', 'gps', 1, 0, 1, 1505957860, 0, 0),
(395, 1, 2, '', 'Homestead', 'UNDM2N', '37.9268014', '-121.69098689999998', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-21 01:37:53', 'mqdgw3298ld6qp7', 'gps', 1, 0, 1, 1505957873, 0, 0),
(396, 1, 2, 'Vodafone', 'Home', 'UNDHL8', '51.615786', '-0.26203399999997146', '16 Grange Hill, Edgware HA8 9PE, UK', 'GB', '', '', '2017-09-21 01:39:25', 'znmewf5t7qydxe2', 'gps', 1, 1, 0, 1505957965, 0, 0),
(397, 1, 2, '', 'Home', 'UNDBH5', '37.9268014', '-121.69098689999998', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-21 01:39:39', 'x2fndzsg2be39bg', 'gps', 1, 0, 1, 1505957979, 0, 0),
(398, 1, 2, 'Vodafone', 'Home', 'UND565', '38.0193657', '-122.13413209999999', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '94548', 'CA', '2017-09-21 01:39:51', 'nzlcnrr5skgaqjt', 'gps', 1, 1, 0, 1505957991, 0, 0),
(399, 1, 2, '', 'Home', 'UNDS7H', '37.9267947', '-121.69098099999997', '8610 Brentwood Blvd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-21 01:40:02', 'i2jyzpzhpcv3ccx', 'gps', 1, 1, 0, 1505958002, 0, 0),
(400, 1, 2, '', 'Home', 'UND152', '30.0566104', '31.33010760000002', 'Mohammed Fahmi Al Mahdar, Al Manteqah Al Oula, Nasr City, Cairo Governorate, Egypt', 'EG', '', '', '2017-09-21 02:09:15', 'rsx7894ulyk3j8v', 'gps', 1, 0, 1, 1505959755, 0, 0),
(401, 92, 1, '', 'home', 'DEV6CQ', '35.18268330000001', '-90.05091540000001', '1389-1517 N Mud Island Rd, Memphis, TN 38103, USA', 'KR', '', '', '2017-09-22 07:22:09', 'q2g4wf7t7cwwk5w', 'gps', 1, 1, 0, 1506064929, 0, 0),
(402, 1, 2, '', 'Vodafone', 'UNDP4Z', '37.9690012', '-121.67166070000002', '1360 Delta Rd, Knightsen, CA 94548, USA', 'US', '', '', '2017-09-22 17:12:06', '586ej3ihhqydd1k', 'gps', 1, 0, 1, 1506100326, 0, 0),
(403, 1, 2, '', 'Japan', 'UNDU92', '35.6894875', '139.69170639999993', 'Tokyo Metropolitan Government Building, 2 Chome Nishishinjuku, Shinjuku-ku, TÅkyÅ-to 160-0023, Japan', 'JP', '', '', '2017-09-22 17:12:44', 'e5c49xv1d9pis55', 'gps', 1, 0, 1, 1506100364, 0, 1),
(404, 1, 2, '', 'Sri Lanka', 'UNDB53', '7.873053999999999', '80.77179699999999', 'Unnamed Road, Dambulla, Sri Lanka', '', '', '', '2017-09-22 17:13:58', 'km6i4y8zkbqzes1', 'gps', 1, 0, 1, 1506100438, 0, 1),
(405, 92, 1, '', 'Home', 'DEV56S', '6.9034424', '79.86448730000006', 'Sri Lanka Foundation Ave, Colombo 00700, Sri Lanka', 'LK', '', '', '2017-09-25 05:46:43', 'a6ha8dz3gqi7pc9', 'gps', 1, 1, 0, 1506318403, 0, 0),
(406, 92, 1, '', 'Home', 'DEV8W2', '35.6811673', '139.76705160000006', '1 Chome-9-1 Marunouchi, Chiyoda-ku, TÅkyÅ-to 100-0005, Japan', 'JP', '', '', '2017-09-25 05:49:49', '4kwrpfyz83misic', 'gps', 1, 0, 1, 1506318589, 0, 1),
(407, 92, 1, 'vodafone', 'Vodafone', 'DEV1CW', '51.49872299999999', '-0.07733699999994315', '58 Riley Rd, London SE1 3DG, UK', 'GB', '', '', '2017-09-25 06:36:52', '4pw7svhhd53k2ic', 'gps', 1, 1, 0, 1506321412, 0, 0),
(408, 1, 2, '', 'home', 'UNDC64', '29.375859', '47.97740520000002', '27 Ahmad Al Jaber St, Kuwait City, Kuwait', 'KW', '', '', '2017-09-25 21:20:56', '6m8r7dw5kehw3th', 'gps', 1, 0, 1, 1506374455, 0, 1),
(409, 1, 2, '', 'home', 'UND2K7', '-37.8038534', '176.07403950000003', '16 Jensen Rd, Tauriko, Omanawa 3171, New Zealand', 'NZ', '', '', '2017-09-25 21:21:30', 'ey1fw4sng6caiie', 'gps', 1, 0, 1, 1506374490, 0, 1),
(410, 62, 1, '', 'Farm', 'EMILY1', '37.9698434', '-121.67282610000001', '1230 Delta Rd, Brentwood, CA 94513, USA', 'US', '', '', '2017-09-29 00:04:26', 'ufsr7i1q2uudylp', 'gps', 1, 1, 0, 1506643466, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pins_meta`
--

CREATE TABLE IF NOT EXISTS `pins_meta` (
  `pin_id` int(11) NOT NULL,
  `browser` varchar(50) DEFAULT NULL,
  `device` varchar(50) DEFAULT NULL,
  `os` varchar(50) DEFAULT NULL,
  `os_version` varchar(50) DEFAULT NULL,
  `is_desktop` tinyint(1) DEFAULT '0',
  `is_mobile` tinyint(1) DEFAULT '0',
  `is_tablet` tinyint(1) DEFAULT '0',
  `user_agent` text,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`pin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pin_user_view`
--

CREATE TABLE IF NOT EXISTS `pin_user_view` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ipaddress` varchar(20) NOT NULL,
  `start_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mt` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pin_user_view`
--

INSERT INTO `pin_user_view` (`id`, `ipaddress`, `start_dt`, `mt`) VALUES
(1, '80.76.166.97', '2017-09-11 11:28:07', 0),
(2, '80.76.166.97', '2017-09-10 11:28:06', 0),
(3, '80.76.166.97', '2017-09-11 11:32:58', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pin_view_history`
--

CREATE TABLE IF NOT EXISTS `pin_view_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `device_type` varchar(50) NOT NULL,
  `ipaddress` varchar(20) NOT NULL,
  `country_name` varchar(50) NOT NULL,
  `country_code` varchar(20) NOT NULL,
  `region_name` varchar(50) NOT NULL,
  `region_code` varchar(50) NOT NULL,
  `city` varchar(100) NOT NULL,
  `zip_code` varchar(20) NOT NULL,
  `date_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `pin_view_history`
--

INSERT INTO `pin_view_history` (`id`, `code`, `device_type`, `ipaddress`, `country_name`, `country_code`, `region_name`, `region_code`, `city`, `zip_code`, `date_dt`) VALUES
(1, 'und4lf', 'Desktop', '111.118.252.146', 'India', 'IN', 'Madhya Pradesh', 'MP', 'Indore', '452001', '2017-09-25 05:42:05'),
(2, 'und7m4', 'Desktop', '111.118.252.146', 'India', 'IN', 'Madhya Pradesh', 'MP', 'Indore', '452001', '2017-09-25 05:42:10'),
(3, 'undhl8', 'Desktop', '111.118.252.146', 'India', 'IN', 'Madhya Pradesh', 'MP', 'Indore', '452001', '2017-09-25 06:50:40'),
(4, '1mhere', 'iOS', '98.207.101.29', 'United States', 'US', 'California', 'CA', 'West Point', '95255', '2017-09-26 00:45:49'),
(5, 'undkj3', 'Desktop', '73.241.65.228', 'United States', 'US', 'California', 'CA', 'Discovery Bay', '94505', '2017-09-29 00:01:29'),
(6, 'undkj3', 'Desktop', '73.241.65.228', 'United States', 'US', 'California', 'CA', 'Discovery Bay', '94505', '2017-09-29 00:01:29'),
(7, '1mhere', 'iOS', '107.77.214.225', 'United States', 'US', 'California', 'CA', 'Daly City', '94015', '2017-09-29 16:32:33'),
(8, '1mhere', 'Desktop', '192.147.231.54', 'Saint Lucia', 'LC', 'Castries', '02', 'Castries', '', '2017-09-29 17:55:23'),
(9, '1mhere', 'iOS', '98.207.101.29', 'United States', 'US', 'California', 'CA', 'West Point', '95255', '2017-09-30 16:23:15');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mesage` longtext NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `mesage`, `date_created`) VALUES
(4, '[{"event":"inbound","ts":1504181884,"msg":{"raw_msg":"Received: from mail-lf0-f49.google.com (unknown [209.85.215.49])\\n\\tby relay-5.us-west-2.relay-prod (Postfix) with ESMTPS id 7C38860424\\n\\tfor <asd23e@addypin.com>; Thu, 31 Aug 2017 12:18:03 +0000 (UTC)\\nReceived: by mail-lf0-f49.google.com with SMTP id d202so1983920lfd.5\\n        for <asd23e@addypin.com>; Thu, 31 Aug 2017 05:18:03 -0700 (PDT)\\nDKIM-Signature: v=1; a=rsa-sha256; c=relaxed\\/relaxed;\\n        d=consagous-com.20150623.gappssmtp.com; s=20150623;\\n        h=mime-version:from:date:message-id:subject:to;\\n        bh=VvQ88wRDqKAYNR0MfbrXaGG8Ib5QgIftMZsy\\/IGZzUA=;\\n        b=ALv\\/z1wIrt74FTJ7igHqeyXnmif\\/5zEUJ7L4GV2Jt2l0atbWe7fbqYCzjp2bdLTqHU\\n         H7guvGp5xkqX3kmNmG598UVimciN5oIf2OEcq4JCg3fA3uU\\/7qhZ7l+Jx\\/lbxba\\/oxrA\\n         JEg1JETCp98v\\/I9zCh2N\\/uoB1rQhtj65Gs4Cfasbcr\\/Vo7w3DZBA\\/LPcCOLi655S6IdA\\n         z\\/9wTsb6GVpk3Qh9KPMmSYu4iw+09kUgLDJdgMWOtj\\/5RI\\/DM5+bkZPoIof4MnMJdWfV\\n         ZSapCslkxMdEIYxL3KkGwJ4Ju58p\\/ROjOXU0yucG2NFOxhuD9lqcqqeWNVMD0isKFrEy\\n         jAiQ==\\nX-Google-DKIM-Signature: v=1; a=rsa-sha256; c=relaxed\\/relaxed;\\n        d=1e100.net; s=20161025;\\n        h=x-gm-message-state:mime-version:from:date:message-id:subject:to;\\n        bh=VvQ88wRDqKAYNR0MfbrXaGG8Ib5QgIftMZsy\\/IGZzUA=;\\n        b=YJqwVX5AGcXZjEm7uW\\/fXAvu8biKOGDvlGhBoBm6Gh42Z4vfApbPEVhMIK2UZ+OLRJ\\n         jTnUxhZBRc6euiS1F2p4wpYXUK08LGT5hWZn3hY3RFrrhkVara2t6PKrBOeixNYd4aWy\\n         uzcjzRLWBCkEYKxkK\\/lHSJKmcxPGkyeIvZeNeUWDRwbv\\/Ws0ygkXGPdqBTrZDCC\\/ao29\\n         sh7vASQvOTWhuIYpHvahYnuyhiQ0U7EewmQRs5\\/5cXlfo+n3UMaoMvVfON7MtPa\\/MVq0\\n         sTYniDSHXgRTwm93pTQe0ZlTLqEheN2K0Eog4I+tCLpz9ZVL+O2mt9+6ZhaPM2jRuUy2\\n         sZBA==\\nX-Gm-Message-State: AHPjjUjTaXigffwvgTSdwJ4H0XHBYZgyHA09dXctJZMioYm\\/S1CJZYCJ\\n\\t8PGnw0AzKkOCz4hi6I1iAKvL5fWRgwyY\\nX-Received: by 10.25.59.135 with SMTP id d7mr2273846lfl.0.1504181881744; Thu,\\n 31 Aug 2017 05:18:01 -0700 (PDT)\\nMIME-Version: 1.0\\nReceived: by 10.46.81.25 with HTTP; Thu, 31 Aug 2017 05:18:01 -0700 (PDT)\\nX-Originating-IP: [111.118.252.146]\\nFrom: Pramod Jain <pramod.jain@consagous.com>\\nDate: Thu, 31 Aug 2017 17:48:01 +0530\\nMessage-ID: <CABkosFAoU9v7RynK=2JGOSSZwM_vJELHoA2LgN3c9ZtduvFMXQ@mail.gmail.com>\\nSubject: test\\nTo: asd23e@addypin.com\\nContent-Type: multipart\\/alternative; boundary=\\"94eb2c14a3c8bd4d3d05580ba298\\"\\n\\n--94eb2c14a3c8bd4d3d05580ba298\\nContent-Type: text\\/plain; charset=\\"UTF-8\\"\\n\\ntest\\n\\n--94eb2c14a3c8bd4d3d05580ba298\\nContent-Type: text\\/html; charset=\\"UTF-8\\"\\n\\n<div dir=\\"ltr\\">test<br><\\/div>\\n\\n--94eb2c14a3c8bd4d3d05580ba298--","headers":{"Received":["from mail-lf0-f49.google.com (unknown [209.85.215.49]) by relay-5.us-west-2.relay-prod (Postfix) with ESMTPS id 7C38860424 for <asd23e@addypin.com>; Thu, 31 Aug 2017 12:18:03 +0000 (UTC)","by mail-lf0-f49.google.com with SMTP id d202so1983920lfd.5 for <asd23e@addypin.com>; Thu, 31 Aug 2017 05:18:03 -0700 (PDT)","by 10.46.81.25 with HTTP; Thu, 31 Aug 2017 05:18:01 -0700 (PDT)"],"Dkim-Signature":"v=1; a=rsa-sha256; c=relaxed\\/relaxed; d=consagous-com.20150623.gappssmtp.com; s=20150623; h=mime-version:from:date:message-id:subject:to; bh=VvQ88wRDqKAYNR0MfbrXaGG8Ib5QgIftMZsy\\/IGZzUA=; b=ALv\\/z1wIrt74FTJ7igHqeyXnmif\\/5zEUJ7L4GV2Jt2l0atbWe7fbqYCzjp2bdLTqHU H7guvGp5xkqX3kmNmG598UVimciN5oIf2OEcq4JCg3fA3uU\\/7qhZ7l+Jx\\/lbxba\\/oxrA JEg1JETCp98v\\/I9zCh2N\\/uoB1rQhtj65Gs4Cfasbcr\\/Vo7w3DZBA\\/LPcCOLi655S6IdA z\\/9wTsb6GVpk3Qh9KPMmSYu4iw+09kUgLDJdgMWOtj\\/5RI\\/DM5+bkZPoIof4MnMJdWfV ZSapCslkxMdEIYxL3KkGwJ4Ju58p\\/ROjOXU0yucG2NFOxhuD9lqcqqeWNVMD0isKFrEy jAiQ==","X-Google-Dkim-Signature":"v=1; a=rsa-sha256; c=relaxed\\/relaxed; d=1e100.net; s=20161025; h=x-gm-message-state:mime-version:from:date:message-id:subject:to; bh=VvQ88wRDqKAYNR0MfbrXaGG8Ib5QgIftMZsy\\/IGZzUA=; b=YJqwVX5AGcXZjEm7uW\\/fXAvu8biKOGDvlGhBoBm6Gh42Z4vfApbPEVhMIK2UZ+OLRJ jTnUxhZBRc6euiS1F2p4wpYXUK08LGT5hWZn3hY3RFrrhkVara2t6PKrBOeixNYd4aWy uzcjzRLWBCkEYKxkK\\/lHSJKmcxPGkyeIvZeNeUWDRwbv\\/Ws0ygkXGPdqBTrZDCC\\/ao29 sh7vASQvOTWhuIYpHvahYnuyhiQ0U7EewmQRs5\\/5cXlfo+n3UMaoMvVfON7MtPa\\/MVq0 sTYniDSHXgRTwm93pTQe0ZlTLqEheN2K0Eog4I+tCLpz9ZVL+O2mt9+6ZhaPM2jRuUy2 sZBA==","X-Gm-Message-State":"AHPjjUjTaXigffwvgTSdwJ4H0XHBYZgyHA09dXctJZMioYm\\/S1CJZYCJ 8PGnw0AzKkOCz4hi6I1iAKvL5fWRgwyY","X-Received":"by 10.25.59.135 with SMTP id d7mr2273846lfl.0.1504181881744; Thu, 31 Aug 2017 05:18:01 -0700 (PDT)","Mime-Version":"1.0","X-Originating-Ip":"[111.118.252.146]","From":"Pramod Jain <pramod.jain@consagous.com>","Date":"Thu, 31 Aug 2017 17:48:01 +0530","Message-Id":"<CABkosFAoU9v7RynK=2JGOSSZwM_vJELHoA2LgN3c9ZtduvFMXQ@mail.gmail.com>","Subject":"test","To":"asd23e@addypin.com","Content-Type":"multipart\\/alternative; boundary=\\"94eb2c14a3c8bd4d3d05580ba298\\""},"text":"test\\n\\n","text_flowed":false,"html":"<div dir=\\"ltr\\">test<br><\\/div>\\n\\n","from_email":"pramod.jain@consagous.com","from_name":"Pramod Jain","to":[["asd23e@addypin.com",null]],"subject":"test","spf":{"result":"none","detail":""},"spam_report":{"score":3.3,"matched_rules":[{"name":"RCVD_IN_SORBS_SPAM","score":0.5,"description":"RBL: SORBS: sender is a spam source"},{"name":"listed","score":0,"description":"in list.dnswl.org]"},{"name":"URIBL_BLOCKED","score":0,"description":"ADMINISTRATOR NOTICE: The query to URIBL was blocked."},{"name":null,"score":0,"description":null},{"name":"more","score":0,"description":"information."},{"name":"gappssmtp.com]","score":0,"description":null},{"name":"RCVD_IN_SORBS_WEB","score":1.5,"description":"RBL: SORBS: sender is an abusable web server"},{"name":"RCVD_IN_DNSWL_NONE","score":-0,"description":"RBL: Sender listed at http:\\/\\/www.dnswl.org\\/, no"},{"name":"HTML_MESSAGE","score":0,"description":"BODY: HTML included in message"},{"name":"DKIM_SIGNED","score":0.1,"description":"Message has a DKIM or DK signature, not necessarily valid"},{"name":"DKIM_VALID","score":-0.1,"description":"Message has at least one valid DKIM or DK signature"},{"name":"RDNS_NONE","score":1.3,"description":"Delivered to internal network by a host with no rDNS"}]},"dkim":{"signed":true,"valid":true},"email":"asd23e@addypin.com","tags":[],"sender":null,"template":null}}]', '2017-08-31 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `user_type_id` int(11) DEFAULT NULL,
  `date_registered` datetime DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  `is_banned` tinyint(1) DEFAULT '0',
  `user_agent` text,
  `activation_code` varchar(20) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `authorize_time` int(11) NOT NULL,
  `link_click` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `email` (`email`),
  KEY `user_type_id` (`user_type_id`),
  KEY `is_active` (`is_active`),
  KEY `is_deleted` (`is_deleted`),
  KEY `is_banned` (`is_banned`),
  KEY `ip` (`ip`),
  KEY `activation_code` (`activation_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=93 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `user_type_id`, `date_registered`, `is_active`, `is_deleted`, `is_banned`, `user_agent`, `activation_code`, `ip`, `authorize_time`, `link_click`) VALUES
(1, NULL, NULL, 'avoidaccess@msn.com', 1, '2016-10-06 11:23:46', 1, 0, 0, '', NULL, '80.76.166.97', 1506374490, 0),
(2, NULL, NULL, 'work@work.com', 1, '2016-10-06 11:53:14', 0, 0, 0, '', NULL, '203.124.28.218', 0, 0),
(3, NULL, NULL, 'faizan@skylinxtesch.com', 1, '2016-10-07 12:42:44', 0, 0, 0, '', NULL, '39.59.29.64', 0, 0),
(4, NULL, NULL, 'ahmedmarzouk2050@gmail.com', 1, '2016-10-09 12:51:50', 0, 1, 0, '', NULL, '81.10.64.60', 0, 0),
(5, NULL, NULL, 'lenaliew.doha@gmail.com', 1, '2016-10-09 19:05:18', 1, 0, 0, '', NULL, '78.101.72.130', 0, 0),
(6, NULL, NULL, 'chughtaifaizan@gmail.com', 1, '2016-10-15 10:20:36', 1, 1, 0, '', NULL, '39.59.107.166', 0, 0),
(7, NULL, NULL, 's.rasha@gmail.com', 1, '2016-10-20 06:36:39', 1, 0, 0, '', NULL, '94.201.160.230', 0, 0),
(8, NULL, NULL, 'subinanton@icloud.com', 1, '2016-10-20 08:48:37', 0, 0, 0, '', NULL, '37.211.120.110', 0, 0),
(9, NULL, NULL, 'subin.anton09@gmail.com', 1, '2016-10-20 09:02:35', 1, 0, 0, '', NULL, '37.211.120.110', 0, 0),
(10, '', '', 'avoidaccess@gmail.com', 1, '2016-10-20 09:12:08', 1, 1, 0, '', NULL, '178.152.231.169', 1505847987, 1),
(11, NULL, NULL, 'eng.afathy@gmail.com', 1, '2016-10-21 05:15:01', 1, 0, 0, '', NULL, '199.7.157.87', 1505243230, 1),
(12, NULL, NULL, 'worko@wod.com', 1, '2016-10-22 08:38:22', 0, 0, 0, '', NULL, '39.59.2.138', 0, 0),
(13, NULL, NULL, 'wo@wo.com', 1, '2016-10-22 08:43:14', 0, 0, 0, '', NULL, '39.59.2.138', 0, 0),
(14, NULL, NULL, 'faizan@skylinxtech.com', 1, '2016-10-24 10:14:22', 1, 1, 0, '', NULL, '39.59.99.79', 0, 0),
(15, NULL, NULL, 'aboidaccess@gmail.com', 1, '2016-10-24 12:28:33', 0, 1, 0, '', NULL, '37.211.228.157', 0, 0),
(16, NULL, NULL, 'dljdlj46@hotmail.com', 1, '2016-10-25 14:33:33', 1, 0, 0, '', NULL, '37.211.112.83', 0, 0),
(17, NULL, NULL, 'sa3591ca@gmail.com', 1, '2016-10-25 17:46:43', 1, 1, 0, '', NULL, '198.91.200.198', 0, 0),
(18, NULL, NULL, 'hatem479@yahoo.com', 1, '2016-11-02 21:52:23', 1, 0, 0, '', NULL, '197.161.12.142', 0, 0),
(19, '', '', 'engy.samir@gmail.com', 1, '2016-11-04 08:25:29', 0, 0, 0, '', NULL, '41.68.118.212', 0, 0),
(20, NULL, NULL, 'Mohamed.Abo-easa@Vodafone.com', 1, '2016-11-05 20:59:09', 1, 0, 0, '', NULL, '196.144.84.244', 0, 0),
(21, NULL, NULL, 'waleed.shawky@yahoo.com', 1, '2016-11-07 19:43:23', 1, 0, 0, '', NULL, '197.122.146.155', 0, 0),
(22, NULL, NULL, 'waqas7041@gmail.com', 1, '2016-11-09 16:04:57', 0, 1, 0, '', NULL, '39.59.59.7', 0, 0),
(23, NULL, NULL, 'elhadysameh@gmail.com', 1, '2016-11-09 19:00:46', 0, 0, 0, '', NULL, '156.205.253.79', 0, 0),
(24, NULL, NULL, 'uzair@skylinxtech.com', 1, '2016-12-18 12:32:37', 1, 1, 0, '', NULL, '116.15.169.4', 0, 0),
(25, NULL, NULL, 'info@efutureshop.com', 1, '2016-12-26 14:54:14', 0, 0, 0, '', NULL, '39.55.17.95', 0, 0),
(26, NULL, NULL, 'mraza8040@gmail.com', 1, '2016-12-28 11:25:15', 0, 0, 0, '', NULL, '182.189.228.134', 0, 0),
(27, NULL, NULL, 'mua33749@dsiay.com', 1, '2016-12-28 16:52:15', 1, 0, 0, '', NULL, '39.55.142.166', 0, 0),
(28, NULL, NULL, 'sab30761@dsiay.com', 1, '2016-12-28 20:30:39', 1, 0, 0, '', NULL, '182.184.89.117', 0, 0),
(29, NULL, NULL, 'david+addypin@siterocket.com', 1, '2017-01-16 22:10:29', 1, 1, 0, '', NULL, '107.161.4.22', 0, 0),
(30, NULL, NULL, 'syeduzairahmad@live.com', 1, '2017-01-30 10:07:41', 1, 0, 0, '', NULL, '14.100.132.187', 0, 0),
(31, NULL, NULL, 'wash@ington', 1, '2017-01-31 16:22:23', 0, 1, 0, '', NULL, '39.55.41.92', 0, 0),
(32, NULL, NULL, 'wash@ingt', 1, '2017-01-31 16:22:41', 0, 1, 0, '', NULL, '39.55.41.92', 0, 0),
(33, NULL, NULL, 'aaawash@msn', 1, '2017-01-31 16:22:53', 0, 1, 0, '', NULL, '39.55.41.92', 0, 0),
(34, NULL, NULL, 'aaawash@msn.com', 1, '2017-01-31 16:23:37', 0, 1, 0, '', NULL, '39.55.41.92', 0, 0),
(35, NULL, NULL, 'uzair@student.com', 1, '2017-02-06 14:21:57', 0, 1, 0, '', NULL, '116.15.169.4', 0, 0),
(36, NULL, NULL, 'uzair+1@skylinxtech.com', 1, '2017-02-08 16:19:44', 1, 1, 0, '', NULL, '116.15.169.4', 0, 0),
(37, NULL, NULL, 'uzair+2@skylinxtech.com', 1, '2017-02-11 14:32:55', 1, 1, 0, '', NULL, '116.15.169.4', 0, 0),
(38, NULL, NULL, 'ramsha.skylinxtech@gmail.com', 1, '2017-02-14 07:40:06', 0, 0, 0, '', NULL, '39.55.252.80', 0, 0),
(39, NULL, NULL, 'nd3x113m.nxp@20minute.email', 1, '2017-02-14 07:58:35', 1, 0, 0, '', NULL, '39.55.252.80', 0, 0),
(40, NULL, NULL, 'uzair+12@skylinxtech.com', 1, '2017-02-15 16:59:48', 0, 1, 0, '', NULL, '116.15.169.4', 0, 0),
(41, NULL, NULL, 'ditto@gmail.com', 1, '2017-02-16 13:52:12', 0, 0, 0, '', NULL, '39.55.117.191', 0, 0),
(42, NULL, NULL, 'ijq76833@psoxs.com', 1, '2017-02-18 15:12:10', 1, 0, 0, '', NULL, '39.55.199.163', 0, 0),
(43, NULL, NULL, 'm1mt53hk.rya@20minute.email', 1, '2017-02-18 15:25:18', 1, 0, 0, '', NULL, '39.55.199.163', 0, 0),
(44, '', '', 'shopfuture10@gmail.com', 1, '2017-02-18 15:51:54', 0, 0, 1, '', NULL, '116.15.169.4', 0, 0),
(45, NULL, NULL, 'avoodaccess@msn.com', 1, '2017-02-19 02:48:12', 0, 1, 0, '', NULL, '166.216.159.55', 0, 0),
(48, NULL, NULL, 'amr.samir@cairenes.co', 1, '2017-02-23 16:18:58', 1, 1, 0, '', NULL, '64.30.81.84', 1505084752, 0),
(49, NULL, NULL, 'waqas.skylinxtech@gmail.com', 1, '2017-02-24 07:38:43', 1, 1, 0, '', NULL, '39.55.102.223', 0, 0),
(50, NULL, NULL, 'hcn3tqbx.asn@20minute.email', 1, '2017-02-27 14:00:54', 0, 0, 0, '', NULL, '119.160.99.32', 0, 0),
(51, NULL, NULL, 'cvkg1afr.wqn@20mail.eu', 2, '2017-02-27 14:04:20', 1, 0, 0, '', NULL, '119.160.99.32', 0, 0),
(52, NULL, NULL, 'qobffsjx.2wd@20minute.email', 2, '2017-03-01 07:56:12', 0, 0, 0, '', NULL, '119.160.100.131', 0, 0),
(53, NULL, NULL, 'stephenherronbuck@gmail.com', 1, '2017-03-01 19:25:02', 1, 1, 0, '', NULL, '73.253.113.78', 0, 0),
(54, NULL, NULL, 'abeera.skylinxtech@gmail.com', 1, '2017-03-08 12:21:34', 1, 1, 0, '', NULL, '39.55.71.158', 0, 0),
(55, NULL, NULL, 'saba.skylinxtech@gmail.com', 1, '2017-03-09 06:52:53', 0, 1, 0, '', NULL, '39.55.190.153', 0, 0),
(56, NULL, NULL, 'vipin.sharma@consagous.com', 1, '2017-07-21 07:37:01', 1, 0, 0, '', NULL, '111.118.252.146', 1505456690, 1),
(57, '', '', 'pramod.jain@consagous.com', 1, '2017-07-21 07:49:33', 1, 0, 0, '', NULL, '111.118.252.146', 1506598140, 1),
(59, NULL, NULL, 'test@yopmail.com', 1, '2017-07-27 13:28:42', 0, 1, 0, '', NULL, '111.118.252.146', 0, 0),
(60, NULL, NULL, 'bhanuprahash.jaiswal@consagous.com', 1, '2017-07-31 13:12:00', 0, 0, 0, '', NULL, '111.118.252.146', 0, 0),
(61, NULL, NULL, 'bhanuprakash.jaiswal@consagous.com', 1, '2017-07-31 13:15:59', 1, 0, 0, '', NULL, '111.118.252.146', 1503931446, 0),
(62, NULL, NULL, 'ejmorrocco@gmail.com', 1, '2017-08-04 23:40:51', 1, 0, 0, '', NULL, '50.242.82.83', 1506643466, 0),
(63, NULL, NULL, 'vipin.shm@gmail.com', 1, '2017-08-09 05:39:03', 1, 0, 0, '', NULL, '111.118.252.146', 0, 0),
(64, NULL, NULL, 'demo@gmail.com', 1, '2017-08-11 14:31:21', 0, 1, 0, '', NULL, '51.15.70.13', 0, 0),
(65, NULL, NULL, 'demo@demos.com', 1, '2017-08-14 10:07:37', 0, 1, 0, '', NULL, '93.115.95.207', 0, 0),
(66, NULL, NULL, 'fd@df.fcd', 1, '2017-08-14 10:12:25', 0, 0, 0, '', NULL, '93.115.95.207', 0, 0),
(67, '', '', 'vishal.dubey@consagous.com', 2, '2017-08-21 11:24:57', 1, 1, 0, '', NULL, '111.118.252.146', 1505110941, 0),
(68, NULL, NULL, 'vipinshm@outlook.com', 2, '2017-08-21 13:21:05', 1, 0, 0, '', NULL, '111.118.252.146', 0, 0),
(69, NULL, NULL, 'vipin@yopmail.com', 1, '2017-08-21 13:36:41', 1, 0, 0, '', NULL, '111.118.252.146', 0, 0),
(75, NULL, NULL, 'pramod.jain@yopmail.com', 1, '2017-08-21 14:22:14', 1, 0, 0, '', NULL, '111.118.252.146', 0, 0),
(76, NULL, NULL, 'demoss@gmail.com', 1, '2017-08-21 14:27:01', 0, 1, 0, '', NULL, '47.247.14.216', 1503325621, 0),
(77, NULL, NULL, 'myemail@yopmail.com', 2, '2017-08-22 04:57:30', 1, 0, 0, '', NULL, '111.118.252.146', 1503377884, 0),
(78, NULL, NULL, 'testping@yopmail.com', 1, '2017-08-22 12:43:50', 0, 0, 0, '', NULL, '111.118.252.146', 1503405830, 0),
(79, '', '', 'lenz.will@gmail.com', 1, '2017-08-24 21:19:49', 0, 1, 0, '', NULL, '107.77.212.51', 1503609589, 0),
(80, NULL, NULL, 'avodaccess@msn.com', 1, '2017-08-24 23:05:21', 0, 1, 0, '', NULL, '98.207.101.29', 1503615921, 0),
(83, '', '', 'anuj@consagous.com', 2, '2017-08-28 11:34:54', 0, 0, 1, '', NULL, '111.118.252.146', 0, 1),
(84, NULL, NULL, 'pramod.consagous@yopmail.com', 1, '2017-08-29 12:07:13', 1, 0, 0, '', NULL, '111.118.252.146', 1504190492, 1),
(85, NULL, NULL, 'vishal.dubey009@gmail.com', 2, '2017-09-07 06:44:01', 1, 0, 0, '', NULL, '111.118.252.146', 1504768783, 1),
(86, NULL, NULL, 'doreenschober@gmx.de', 1, '2017-09-10 01:41:48', 0, 1, 0, '', NULL, '50.242.82.83', 1505007708, 0),
(87, NULL, NULL, 'doctorcruben@gmail.com', 1, '2017-09-10 22:56:51', 1, 1, 0, '', NULL, '50.242.82.83', 1505761543, 0),
(88, NULL, NULL, 'avoidacceass@gmail.com', 1, '2017-09-11 22:47:28', 0, 1, 0, '', NULL, '50.242.82.83', 1505170048, 0),
(89, NULL, NULL, 'avoidmee@yahoo.com', 1, '2017-09-13 06:11:33', 0, 1, 0, '', NULL, '98.207.101.29', 1505283093, 0),
(90, NULL, NULL, 'ahmed.abdelrazek@fidus.com', 1, '2017-09-18 19:43:25', 1, 0, 0, '', NULL, '66.46.188.219', 1505763805, 0),
(92, NULL, NULL, 'devendra.sharma@consagous.com', 1, '2017-09-22 07:22:09', 1, 0, 0, '', NULL, '111.118.252.146', 1506597950, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_meta`
--

CREATE TABLE IF NOT EXISTS `users_meta` (
  `user_id` int(11) NOT NULL,
  `browser` varchar(50) DEFAULT NULL,
  `device` varchar(50) DEFAULT NULL,
  `os` varchar(50) DEFAULT NULL,
  `os_version` varchar(50) DEFAULT NULL,
  `is_desktop` tinyint(1) DEFAULT '0',
  `is_mobile` tinyint(1) DEFAULT '0',
  `is_tablet` tinyint(1) DEFAULT '0',
  `user_agent` text,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE IF NOT EXISTS `user_types` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` enum('individual','corporate') DEFAULT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`user_type_id`, `user_type`) VALUES
(1, 'individual'),
(2, 'corporate');

-- --------------------------------------------------------

--
-- Table structure for table `view_limit`
--

CREATE TABLE IF NOT EXISTS `view_limit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(18) NOT NULL,
  `view_date` date NOT NULL,
  `view_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=163 ;

--
-- Dumping data for table `view_limit`
--

INSERT INTO `view_limit` (`id`, `ip`, `view_date`, `view_count`) VALUES
(153, '111.118.252.146', '2017-09-22', 6),
(154, '98.207.101.29', '2017-09-22', 10),
(155, '66.102.6.156', '2017-09-22', 3),
(156, '66.102.6.155', '2017-09-22', 2),
(157, '66.87.118.134', '2017-09-22', 2),
(158, '111.118.252.146', '2017-09-25', 3),
(159, '98.207.101.29', '2017-09-26', 1),
(160, '107.77.214.225', '2017-09-29', 1),
(161, '192.147.231.54', '2017-09-29', 1),
(162, '98.207.101.29', '2017-09-30', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
