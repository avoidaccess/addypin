<?php
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
class UserHelper{
    public static function CreateUser($email, $type){
        $user_agent = "";

        // Check in database if a user exists with this email address and is active.

        $where = array();
        $where['email'] = $email;
        $sql = "SELECT * FROM users WHERE email = :email";
        $result = Query($sql, $where);

        $guid = Functions::generateRandomString();
        $ip = $_SERVER['REMOTE_ADDR'];

        // If there were no users found, add one
        if($result->count == 0){
            $where = array();
            $where['email'] = $email;
            //$where['user_type_id'] = $type == "personal" ? 1 : 2;
            $where['user_type_id'] = $type;
            $where['user_agent'] = $user_agent;
            $where['ip'] = $ip;

            $time = time();
            $where['authorize_time'] = $time;

           
            

            $sql = "INSERT INTO users (email, user_type_id, date_registered, user_agent, ip, authorize_time) VALUES (:email, :user_type_id, NOW(), :user_agent, :ip, :authorize_time)";
            $result = Query($sql, $where);
            $id = $result->db->lastInsertId();
            return $id;
        }
        else {



            $id = $result->data[0]->user_id;
            
            $where1 = array();
            $time = time();
           
            
            $link_click = 0;
            $where1['user_id'] = $id;
           // echo  $sql1 = "UPDATE users SET `authorize_time`='$authorize_time', `link_click` = '$link_click' WHERE `user_id` = '$user_id'";
             $sql1 = "UPDATE users SET authorize_time = $time, link_click = $link_click WHERE user_id = :user_id";
            Query($sql1, $where1);
           

            return $id;
        }
    }

    public static function AdminLogin($username){

        $where['username'] = $username;

        $sql = "SELECT * FROM admin_users WHERE username = :username";
        $result = Query($sql, $where);
        if(isset($result->data[0])){
        $password = $result->data[0]->password;
        
        return $password;
        } else {
            return false;
        }
    }

    public static function PinLimit($id,$type){
    	$where = array();
    	$isAllowed = false;
    	$ip = $_SERVER['REMOTE_ADDR'];

        /* U.user_type_id  P.is_active=1 AND */

    	$sql = "SELECT COUNT(pin_id) As Total FROM users U JOIN pins P ON U.user_id = P.user_id WHERE U.user_id ='$id' AND  P.pin_type = '$type' AND  P.is_deleted=0";
    	$result = Query($sql, $where);
    	$Count = $result->data[0]->Total;
    	
        if($type == 1){
            if ($type == 1 && $Count < 5) {
            $isAllowed = true;
           //echo '--1';
            }

        }else if($type == 2){

    	  if ($type == 2 && $Count < 20) {
    		$isAllowed = true;
            // echo '--2';
          }
        }

    	return $isAllowed;
    }

    public static function IP_Sapaming(){
    	$where = array();
 		$isAllowed = false;
    	$ip = $_SERVER['REMOTE_ADDR'];
    	$where['ip'] = $ip;
    	
    	$sql = "SELECT * FROM ip_table WHERE ip =:ip";
        $result = Query($sql, $where);
        $count = $result->data[0]->count;
        $timer = $result->data[0]->date_time;
        $creat_event = (int)$timer;
        $reset_event = $creat_event + 300;

        $current_time = time();

      

        
        if($result->count == 1){
            if($count < 5) {
                $where = array();
                $counter = ++$count;
                $date_time = (string)time();
                $sql = "UPDATE ip_table SET `count`='$counter', `date_time` = '$date_time' WHERE `ip` = '$ip'";
                Query($sql, $where);
                $isAllowed = true;
            }
        	
            if($current_time >= $reset_event){
                $date_time = (string)time();
                $sql = "UPDATE ip_table SET `count`='0', `date_time` = '$date_time' WHERE `ip` = '$ip'";
                Query($sql, $where);
                $isAllowed = true;
            }
        }

        // If there were no recored found, add one
        if($result->count == 0){
            $where = array();
            $where['count'] = 0;
            $where['ip'] = $ip;
            $where['date_time'] = (string)time();
            $sql = "INSERT INTO ip_table (ip, count, date_time) VALUES (:ip, :count, :date_time)";
            Query($sql, $where);
            $isAllowed = true;
        }

    return $isAllowed; 

    }
}