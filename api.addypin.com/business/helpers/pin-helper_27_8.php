<?php
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
class PinHelper{

    public static function CreatePin($email,$type,$uid,$location_name,$name,$code,$lat,$long,$address,$postal_code,$landmark,$guid,$is_gps, $mode = 'ip'){
    	$where['user_id'] = $uid;

    	// $where['is_active'] = 0;
    	$where['is_deleted'] = 0;

        $ip_allowed = UserHelper::IP_Sapaming();
        $isAllowed = UserHelper::PinLimit($uid,$type);
    	// $sql = "SELECT COUNT(1) As Total FROM pins WHERE user_id = :user_id AND is_active = :is_active AND is_deleted = :is_deleted";
        $sql = "SELECT COUNT(1) As Total FROM pins WHERE user_id = :user_id AND is_deleted = :is_deleted";
        $result = Query($sql, $where);
        $pinCount = $result->data[0]->Total;
        $result = "";
        // echo "-->".(int)(bool)$isAllowed. "-->>". $pinCount. "~~~~~".$ip_allowed;
        // echo "---";
        if ($ip_allowed == true) {
            
            // if ($pinCount < 5 && $isAllowed == true) {
            if ($pinCount < 5 && $isAllowed == true) {

            $where = array();
			$where['pin_type'] = $type;
            $where['uid'] = $uid;
            //$where['email'] = $email;
            $where['location'] = $location_name;
            $where['name'] = $name;
            $where['code'] = $code;
            $where['lat'] = $lat;
            $where['lng'] = $long;
            $where['address'] = $address;
            $where['postal_code'] = $postal_code;
            $where['is_gps'] = $is_gps;
            $where['landmark'] = $landmark;
            $where['activation_code'] = $guid;
            $where['mode'] = $mode;

            $country = '';
            if(!empty($lat) && !empty($long)){
                 
            $latlng = $lat.','.$long;
            $request_url = "http://maps.googleapis.com/maps/api/geocode/xml?latlng=".$latlng."&sensor=true";
            $xml = simplexml_load_file($request_url);

                if($xml->status == "OK") {
                    $address = $xml->result->formatted_address;
                    foreach ($xml->result->address_component as $address) {
                           if("country" ==  trim($address->type)) {
                                $country = $address->short_name;
                            }
                        
                    }
                      
                }
            }

               if(!empty($lat) && !empty($long))
            {
                $src = "https://maps.googleapis.com/maps/api/staticmap?center=$lat,$long&markers=color:red%7Clabel:C%7C$lat,$long&zoom=12&size=300x450";
                $time = time();
                $desFolder = '/var/www/html/api.addypin.com/img/';
                $imageName = $uid.'.png';
                $imagePath = $desFolder.$imageName;
                @file_put_contents($imagePath,file_get_contents($src));
            }else  if(!empty($country))
            {
                $src = "https://maps.googleapis.com/maps/api/staticmap?center=$country&markers=color:red%7Clabel:C%7C$country&zoom=12&size=300x450";
                $time = time();
                $desFolder = '/var/www/html/api.addypin.com/img/';
                $imageName = $uid.'.png';
                $imagePath = $desFolder.$imageName;
                @file_put_contents($imagePath,file_get_contents($src));
            }
            
             $where['country'] = $country;

            //return $where;
            $sql = "INSERT INTO pins (pin_type, user_id, name, location_name, code, lat, `long`, address,country, postal_code, landmark, date_created, activation_code, is_gps, mode) 
            VALUES (:pin_type, :uid, :name, :location, :code, :lat, :lng, :address, :country, :postal_code, :landmark, NOW(), :activation_code, :is_gps, :mode)";
            $result = Query($sql, $where);
            $pid = $result->db->lastInsertId();
            
            $emailResult = EmailHelper::PinActivationEmail($uid, $pid, $guid, $email,$code);
            $result = $emailResult;
            }
            else {
                $result = 'NULL';
            }
        }
        
        return $result;

    }
}