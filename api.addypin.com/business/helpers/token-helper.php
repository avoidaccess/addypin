<?php
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
class TokenHelper{
    public static function GenerateToken(){
        $signer = new Sha256();
        $token = (new Builder())->setIssuer('http://addypin.com') // Configures the issuer (iss claim)
        ->setAudience('http://addypin.com') // Configures the audience (aud claim)
        ->setId(Auth::KEY, true) // Configures the id (jti claim), replicating as a header item
        ->setIssuedAt(time()) // Configures the time that the token was issue (iat claim)
        ->setNotBefore(time()) // Configures the time that the token can be used (nbf claim)
        ->setExpiration(time() + 3600) // Configures the expiration time of the token (exp claim)
        ->set('uid', 1) // Configures a new claim, called "uid"
        ->sign($signer, 'ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQBUcGQSArg9PVmfGDSLNjeqJola8y4rR9X+ukp/hFe52KvG7MEb5fUZAN/iMbyouFYn5PS2ARldIBx2tg++h/J94uvkQgHI9f5sjlwEkRS26PMoyi+Mb1AwIc4aHc/Yoe0qCxFTPyGT4qXva3VCGyfK20JOE9DkHynhItrCQmhSqVrNFQJK67B01vwukJYZs/FeK+OkO4NrF17Q2T3jmZzq8yGH4svBTpOXKEK6eXqXuiRbTYbn8uNOQZMH8Iuq+8MnrjfIq5Ln/2DlpXS+gWPWp5b8NMiaoEeyDW4TKoEw1FCjPS2CVc6VG0AYu3+VVfyypwisfdzRhUVVCIkezMb7 rsa-key-20160429') // creates a signature using 2048 bit SS2-RSA key
        ->getToken(); // Retrieves the generated token
        return $token;
    }
}