<?php
class LookupHelper{
    public static function GetTodaysLookupLog($ip){
        $where = array();
        $where['ip'] = $ip;
        $sql = "SELECT * FROM lookup_limits WHERE ip = :ip AND lookup_date = DATE(NOW())";
        $result = Query($sql, $where);
        return $result;
    }
}