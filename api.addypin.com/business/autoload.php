<?php
function __autoload($class){
    $filename = strtolower($class) . '.php';
    $file = './services/' . $filename;

    if (file_exists($file) == true){
        include ($file);
        return;
    }
}