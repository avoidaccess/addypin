<?php
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
class Login
{
    
    /**
     * @url POST /login
     * @description Backend Login
     */
    public function doLogin($request_data = NULL){
        $user = $request_data['username'];
        $pswd = $request_data['password'];
        
        //print_R($request_data);
        //die(' hi ');

        $r = new stdClass();
        $r->valid = false;

        $password = UserHelper::AdminLogin($user);
        //echo $password;
        // addypin = 7c06af610963c0453d0f357a8bd85a01db2c5501
        if($password == sha1($pswd) ){
            // Generate a new token for the user
            $token = TokenHelper::GenerateToken();

            
            $r->valid = true;
            $r->token = (string)$token;
        }
        /*
        if($user == Auth::USER && $pswd == Auth::PSWD){
            // Generate a new token for the user
            $token = TokenHelper::GenerateToken();

            $r->valid = true;
            $r->token = (string)$token;
        }
        */
        return $r;
    }

    /**
     * @url POST /authorize
     * @description Frontend Login
     */
    public function authorize($request_data = NULL){
        // Check if the email address is valid then send an email to the user with a verification link. That link will contain the token.
        // Once they click it, the token will be saved in their local storage and they will be authorized to make the API calls.
        $email = $request_data['email'];
        $c = new stdClass();

        $token = TokenHelper::GenerateToken();

        $link = "https://" . SITE . "/#/verify/".(string)$token;

        // See if email is valid
        $where['email'] = $email;
        
		$sql = "SELECT * FROM users WHERE email = :email AND is_active = 1 AND is_deleted = 0 AND is_banned = 0";
		// New SQL by dilip to by-pass active tag
		$sql = "SELECT * FROM users WHERE email = :email AND is_deleted = 0 AND is_banned = 0";
		
        $result = Query($sql, $where);
        $response = array();
        $response[0] = new stdClass();
        $response[0]->status = "failed";

        if($result->count > 0){
            $dbEmail = $result->data[0]->email;
            $dbUid = $result->data[0]->user_id;
            $link .= "/" . $dbUid;

            $where1 = array();
            $where1['user_id'] = $dbUid;
            $time = time();
			
			// Commented by dilip
			$link_click = 0;
            $sql2 = "UPDATE users SET authorize_time = $time,link_click = $link_click WHERE user_id = :user_id";
            Query($sql2, $where1);
			$r = EmailHelper::SendManagePinsVerificationEmail($link, $email);
			
			if($result->data[0]->is_active == 0){
				$sql2 = "UPDATE users SET is_active = 1 WHERE user_id = :user_id";
				Query($sql2, $where1);
			}
			$response[0]->status = "sent";
        }

        //$c->token = (string)$token;

        //$c->success = true;
        //debug($request_data);
        return $response;
    }
}