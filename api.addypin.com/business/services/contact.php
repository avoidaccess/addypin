<?php
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

/**
 * All methods in this class are protected
 * @access protected
 */
class Contact{
    /**
    * @url POST /contact_us
    * @access public
    */
    // public function ContactUs($email, $text){
    //     return EmailHelper::SendContactEmail($email, $text);
    // }

    /**
     * @url GET /ip
     * @access public
     */
    public function IP(){
        $r = UserHelper::IP_Sapaming();

        return $r;
    }

}