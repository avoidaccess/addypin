<?php
use Luracast\Restler\iAuthenticate;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Parser;

class Auth implements iAuthenticate{
    const KEY = 'SuperSecretKey';
    const USER = 'admin';
    const PSWD = 'addypin';
    function __isAllowed()
    {
        $isAuth = false;
        $authToken = "";
        $headers = apache_request_headers();/*
        $auth = str_replace("Bearer", "", $headers['Authorization']);*/
        //debug($headers);

        if(isset($headers['AUTHORIZATION']) && !empty($headers['AUTHORIZATION'])){
            $authToken = $headers['AUTHORIZATION'];
        }
        // If Auth Token is empty, check if it's appended as a query string
        else if(!empty($_GET['api_key'])){
            $authToken = $_GET['api_key'];
        }
        
        // Check to see if we have an Auth header if not check for an api key
        if (!empty($authToken))
        {
            $origin = $headers['ORIGIN'];
            $cleanAuthToken = str_replace('Bearer ', '', $authToken);
            $token = (new Parser())->parse((string) $cleanAuthToken); // Parses from a string
            $token->getHeaders(); // Retrieves the token header
            $token->getClaims(); // Retrieves the token claims

            $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
            $data->setIssuer('http://addypin.com');
            $data->setAudience('http://addypin.com');
            $data->setId(Auth::KEY);

            $isAuth = $token->validate($data);
            
            return $isAuth;
        }
        
        //return true;
        return isset($_GET['key']) && $_GET['key'] == SimpleAuth::KEY ? TRUE : FALSE;
    }
    public function __getWWWAuthenticateString()
    {
        return 'Query name="key"';
    }
}