<?php
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

/**
 * All methods in this class are protected
 * @access protected
 */
class User{
	
	
    /**
    * @url GET /users/get/all
    * @access public
    */
    public function GetAllUsers($is_active = -1, $is_deleted = -1, $is_banned = -1){
        $r = new stdClass();
        $where = array();
        $whereStr = "";

        if($is_active >= 0){
            $where['is_active'] = $is_active;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_active = :is_active";
        }
        if($is_deleted >= 0){
            $where['is_deleted'] = $is_deleted;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_deleted = :is_deleted";
        }
        if($is_banned >= 0){
            $where['is_banned'] = $is_banned;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_banned = :is_banned";
        }
        $whereStr = empty($whereStr) ? $whereStr : "WHERE " . $whereStr;
        $sql = "SELECT * FROM users " . $whereStr;
      
        $result = Query($sql, $where);
        $r->users = $result->data;
        $r->rowsCount = $result->count;
        return $r;
    }
    
    
    /**
    * @url GET /users/get/corporate
    * @access public
    */
    public function GetUsersCorporate($is_active = -1, $is_deleted = -1, $is_banned = -1){
        $r = new stdClass();
        $where = array();
        $whereStr = "";

        if($is_active >= 0){
            $where['is_active'] = $is_active;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_active = :is_active";
        }
        if($is_deleted >= 0){
            $where['is_deleted'] = $is_deleted;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_deleted = :is_deleted";
        }
        if($is_banned >= 0){
            $where['is_banned'] = $is_banned;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_banned = :is_banned";
        }

        $where['user_type_id'] = 2;
        $whereStr .= (empty($whereStr) ? "" : " AND ") . " user_type_id = :user_type_id";

        $whereStr = empty($whereStr) ? $whereStr : "WHERE " . $whereStr;
        $sql = "SELECT * FROM users " . $whereStr;
      
        $result = Query($sql, $where);
        $r->users = $result->data;
        $r->rowsCount = $result->count;
        return $r;
    }

    /**
    * @url GET /users/get/individual
    * @access public
    */
    public function GetUsersIndividual($is_active = -1, $is_deleted = -1, $is_banned = -1){
        $r = new stdClass();
        $where = array();
        $whereStr = "";

        if($is_active >= 0){
            $where['is_active'] = $is_active;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_active = :is_active";
        }
        if($is_deleted >= 0){
            $where['is_deleted'] = $is_deleted;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_deleted = :is_deleted";
        }
        if($is_banned >= 0){
            $where['is_banned'] = $is_banned;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_banned = :is_banned";
        }

        $where['user_type_id'] = 1;
        $whereStr .= (empty($whereStr) ? "" : " AND ") . " user_type_id = :user_type_id";

        $whereStr = empty($whereStr) ? $whereStr : "WHERE " . $whereStr;
        $sql = "SELECT * FROM users " . $whereStr;
      
        $result = Query($sql, $where);
        $r->users = $result->data;
        $r->rowsCount = $result->count;
        return $r;
    }



    /**
     * @url GET /users/by/id/{user_id}
     */
    public function GetUser($user_id){
        $r = new stdClass();
        $whereStr = "";
        $where = array();
        $where['user_id'] = $user_id;
        $sql = "SELECT * FROM users WHERE user_id = :user_id";
        $result = Query($sql, $where);
        $r->user = $result->data;

        date_default_timezone_set('Asia/Karachi');

        $r->timestamp = date("h:i:sa");
        return $r;
    }

    /**
     * @url GET /users/by/type/{type}
     */
    public function GetUsersByType($type){
        $type = strtolower($type);
        $r = new stdClass();
        $whereStr = "";
        $where = array();
        $where['user_type'] = $type;
        $sql = "SELECT u.* FROM users u JOIN user_types ut ON u.user_type_id = ut.user_type_id WHERE ut.user_type = :user_type";
        $result = Query($sql, $where);
        $r->user = $result->data;
        return $r;
    }

    /**
     * @url GET /users/get/all/count
     *@access public
     */
    public function GetAllUsersCount($is_active = 1, $is_deleted = 0, $is_banned = 0){
        /*$db = new DB();*/
        $r = new stdClass();
        $where = array();
        $whereStr = "";

        if($is_active >= 0){
            $where['is_active'] = $is_active;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_active = :is_active";
        }
        if($is_deleted >= 0){
            $where['is_deleted'] = $is_deleted;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_deleted = :is_deleted";
        }
        if($is_banned >= 0){
            $where['is_banned'] = $is_banned;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_banned = :is_banned";
        }
        $whereStr = empty($whereStr) ? $whereStr : "WHERE " . $whereStr;
        //debug($whereStr);
        $sql = "SELECT count(1) As TotalUsers FROM users " . $whereStr;

        $result = Query($sql, $where);
        $r->usersCount = $result->data;
        //$r->usersCount[0]->TotalUsers = 10;   // For Testing Purpose
        $r->rowsCount = $result->count;
        return $r;
    }

    /**
     * @url GET /users/get/all/individual/count
     */
    public function GetAllIndividualUsersCount($is_active = 1, $is_deleted = 0, $is_banned = 0){
        /*$db = new DB();*/
        $r = new stdClass();
        $where = array();
        $whereStr = "";

        if($is_active >= 0){
            $where['is_active'] = $is_active;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_active = :is_active";
        }
        if($is_deleted >= 0){
            $where['is_deleted'] = $is_deleted;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_deleted = :is_deleted";
        }
        if($is_banned >= 0){
            $where['is_banned'] = $is_banned;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_banned = :is_banned";
        }

        $where['user_type_id'] = 1;
        $whereStr .= (empty($whereStr) ? "" : " AND ") . " user_type_id = :user_type_id";

        $whereStr = empty($whereStr) ? $whereStr : "WHERE " . $whereStr;
        //debug($whereStr);
        $sql = "SELECT count(1) As TotalUsers FROM users " . $whereStr;

        $result = Query($sql, $where);
        $r->usersCount = $result->data;
        //$r->usersCount[0]->TotalUsers = 10;   // For Testing Purpose
        $r->rowsCount = $result->count;
        return $r;
    }

    /**
     * @url GET /users/get/all/corporate/count
     */
    public function GetAllCorporateUsersCount($is_active = 1, $is_deleted = 0, $is_banned = 0){
        /*$db = new DB();*/
        $r = new stdClass();
        $where = array();
        $whereStr = "";

        if($is_active >= 0){
            $where['is_active'] = $is_active;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_active = :is_active";
        }
        if($is_deleted >= 0){
            $where['is_deleted'] = $is_deleted;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_deleted = :is_deleted";
        }
        if($is_banned >= 0){
            $where['is_banned'] = $is_banned;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_banned = :is_banned";
        }

        $where['user_type_id'] = 2;
        $whereStr .= (empty($whereStr) ? "" : " AND ") . " user_type_id = :user_type_id";

        $whereStr = empty($whereStr) ? $whereStr : "WHERE " . $whereStr;
        //debug($whereStr);
        $sql = "SELECT count(1) As TotalUsers FROM users " . $whereStr;

        $result = Query($sql, $where);
        $r->usersCount = $result->data;
        //$r->usersCount[0]->TotalUsers = 10;   // For Testing Purpose
        $r->rowsCount = $result->count;
        //sleep(30);
        return $r;
    }

    /**
     * @url POST /users/save
     */
    public function SaveUser($request_data = NULL){
        $r = new stdClass();
        //debug($request_data);
        $where = array();
       

        $where['user_id'] = $request_data['user_id'];

        $first_name = $request_data['first_name'];
        $last_name = $request_data['last_name'];
        $email = $request_data['email'];
        $is_active = $request_data['is_active'];
        $is_deleted = $request_data['is_deleted'];
        $is_banned = $request_data['is_banned'];

        $userStatus = $request_data['Status'];

         $is_deleted ="0";
        $is_active ="0";
        $is_banned ="0";
         if($userStatus == "Active" ){
            $is_active="1";
         }else if($userStatus == "Banned"){
            $is_banned ="1";
         }else if($userStatus == "Inactive"){
            $is_deleted="1";
         }

         
        
       $sql = "UPDATE users SET `first_name` = '$first_name', `last_name` = '$last_name', `email` = '$email', `is_active` = '$is_active', `is_deleted` = '$is_deleted', `is_banned` = '$is_banned' WHERE `user_id` = :user_id";
        $result = Query($sql, $where);
        $r->rowsAffected = $result->data;
        return $r;
    }

    /**
     * @url POST /users/delete/{user_id}
     */
    public function DeleteUser($user_id){
        $r = new stdClass();
        $where = array();
        $where['user_id'] = $user_id;
        $sql = "UPDATE users SET is_deleted = 1 WHERE user_id = :user_id";
        $result = Query($sql, $where);
        $r->rowsAffected = $result->data;
        //return $r;
        return $user_id;
    }

    /**
     * @url POST /register
     * @access public
     */
    public function Register($request_data = NULL){
        $r = new stdClass();
        $email = $request_data['email'];
        $type = $request_data['type'];
        $user_agent = "";

        // Check in database if a user exists with this email address and is active.

        $where = array();
        $where['email'] = $email;
        $sql = "SELECT * FROM users WHERE email = :email AND is_active = 1 AND is_deleted = 0 AND is_banned = 0";
        $result = Query($sql, $where);
        $r->user = $result->data;
        $r->rowsCount = $result->count;

        $guid = Functions::generateRandomString();
        $ip = $_SERVER['REMOTE_ADDR'];

        // If there were no users found, add one
        if($r->rowsCount == 0){
            $where = array();
            $where['email'] = $email;
            $where['user_type_id'] = $type == "personal" ? 1 : 2;
            $where['user_agent'] = $user_agent;
            $where['activation_code'] = $guid;
            $where['ip'] = $ip;

            $sql = "INSERT INTO users (email, user_type_id, date_registered, user_agent, activation_code, ip) VALUES (:email, :user_type_id, NOW(), :user_agent, :activation_code, :ip)";
            $result = Query($sql, $where);
        }
        else{
            $uid = $result->data[0]->user_id;
            $where1 = array();
            $where1['guid'] = $guid;
            $where1['uid'] = $uid;

            $sql = "UPDATE users SET activation_code = :guid WHERE user_id = :uid";
            $result1 = Query($sql, $where1);
        }
        return EmailHelper::SendRegistrationEmail($email, $type, $guid, $r->rowsCount > 0);
    }

    /**
     * @url POST /registration-activation
     * @access public
     */
    public function RegistrationActivation($request_data = NULL){
        $r = new stdClass();
        $email = $request_data['email'];
        $type = $request_data['type'];
        $code = $request_data['code'];
        //sleep(5000);
        //return $request_data;
        $where = array();
        $where['email'] = $email;
        $where['user_type_id'] = $type == "personal" ? 1 : 2;
        $where['activation_code'] = $code;

        $sql = "SELECT * FROM users WHERE email = :email AND user_type_id = :user_type_id AND activation_code = :activation_code AND activation_code IS NOT NULL AND activation_code != '' AND is_deleted = 0 AND is_banned = 0";
        $result = Query($sql, $where);
        //$r->user = $result->data;
        $r->rowsCount = $result->count;
        $r->uid = 0;
        $r->token = '';
        // This means inactive user account was found that needs to be activated
        if($r->rowsCount > 0){
            $user = $result->data[0];
            if($user->is_active == 0) {
                $sql = "UPDATE users SET is_active = 1 WHERE email = :email AND user_type_id = :user_type_id AND activation_code = :activation_code AND is_active = 0 AND is_deleted = 0 AND is_banned = 0";
                $result = Query($sql, $where);
            }
            // Should return a fresh token to the client from here.
            $token = TokenHelper::GenerateToken();

            $r->uid = $user->user_id;
            $r->token = (string)$token;
        }
        return $r;
    }

    /**
     * @url POST /contact_us
     * @access public
     */
    public function ContactUs(){
        // return json_encode($_POST); die;
        return EmailHelper::SendContactEmail($_POST['email'], $_POST['text']);
    }

    /**
     * @url GET /code-suggestions/{email}
     * @access public
     */
    public function GetCodeSuggestions($email){
        $str = substr($email, 0, 3);
        $strings = array();
        for($i = 1; $i <= 3; $i++){
            $random = Functions::generateRandomString(2);
            $randomNumber = Functions::generateRandomNumber();
            $str1 = $random . $randomNumber;
            $shuffled = str_shuffle($str1);
            $str2 = $str . $shuffled;
            if(!in_array($str2, $strings) && !Functions::pinExistsInDatabase($str2))
                $strings[] = $str2;
            else
                $i--;
        }

        return $strings;
    }

    /**
     * @url GET /username
     * @access public
     */
    public function GetUserName(){
        $r = new stdClass();
        $where = array();
        $where['id'] = 1;
        $sql = "SELECT au_id, username FROM admin_users  WHERE au_id = :id";
        $result = Query($sql, $where);
        $r->admin_user = $result->data;
        return $r;
    }

    /**
     * @url POST /password/save
     * @access public
     */
    public function SavePassword($request_data = NULL){
        $r = new stdClass();
        //debug($request_data);
        $r->rowsAffected = 'null';
        $where = array();

        $user_id = $request_data['au_id'];

        $user_name = $request_data['username'];
        $old = $request_data['old_pswd'];
        $new = $request_data['new_pswd'];
        $old_password = sha1($old);
        $new_password = sha1($new);

        $where['username'] = $request_data['username'];
        $where['password'] = $new_password;

        $get_old_pswd = UserHelper::AdminLogin($user_name);

         if ($old_password == $get_old_pswd) {
            $sql = "UPDATE admin_users SET `username` = '$user_name' , `password` = '$new_password' WHERE au_id = '$user_id' ";
            $result = Query($sql, $where);
            $r->rowsAffected = $result->data;
            //$r->rowsAffected = 1;
         }
         return $r;
         
        
    }

    /**
     * @url GET /ip
     * @access public
     */
    public function IP(){
        $r = UserHelper::IP_Sapaming();

        return $r;
    }

}