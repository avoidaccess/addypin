<?php
/**
 * All methods in this class are protected
 * @access protected
 */
class Pin{
    /**
     * @url GET /pins/get/all
     */
    public function GetAllPins($is_deleted = -1,$user_id = NULL){
		 $r = new stdClass();
        $where = array();
        $whereStr = "";

        if($is_deleted >= 0){
            $where['is_deleted'] = $is_deleted;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_deleted = :is_deleted";
        }

        if(!empty($user_id) && $user_id != 'all' ){
            $where['user_id'] = $user_id;
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " user_id = :user_id";
        }
        $whereStr = empty($whereStr) ? $whereStr : "WHERE " . $whereStr;
        $sql = "SELECT * FROM pins " . $whereStr;


        /*$sql = "select pin_id,user_id,name,location_name,code,lat,long,address,postal_code,landmark,  date_created,activation_code,mode,is_gps,is_active,is_deleted,
    case pins.pin_type
        when '1' then 'Individual'
        when '2' then 'Corporate'
    end as pin_type
from pins". $whereStr;*/

        $result = Query($sql, $where);

        foreach($result->data as $key => $val){
           
           $pin_type = 'Corporate';
            if($val->pin_type == '1'){
                    $pin_type = 'Individual';
            }

             $st[] = array('pin_id' => $val->pin_id,
                           'user_id' => $val->user_id,
                           'name' => $val->name,
                           'pin_type' => $pin_type,
                           'location_name' => $val->location_name,
                           'code' => $val->code,
                           'lat' => $val->lat,
                           'long' => $val->long,
                           'address' => $val->address,
                           'postal_code' => $val->postal_code,
                           'landmark' => $val->landmark,
                           'date_created' => $val->date_created,
                           'activation_code' => $val->activation_code,
                           'mode' => $val->mode,
                           'is_gps' => $val->is_gps,
                           'is_active' => $val->is_active,
                           'is_deleted' => $val->is_deleted,
                           ); 
        }
        $r->pins = $st;
        //$r->pins = $result->data;
        $r->rowsCount = $result->count;
        return $r;
    }

    
     /**
     * @url POST image_check/{pid}
     * @access public
     */
     public function imageCheck($pin_id,$lat,$long){

        if($pin_id !=''){
        
            $imageName = $pin_id.'.png';
            $imagePath =  '/var/www/html/addypin.com/assets/map/';
            $filePath = trim($imagePath).trim($imageName);
          
           if(@getimagesize($filePath)){
                return true;
            }else{
                
                $src = "https://maps.googleapis.com/maps/api/staticmap?center=$lat,$long&markers=color:red%7C$lat,$long&zoom=15&size=300x350";
                $time = time();
                $desFolder = $imagePath;
                $imageName = $pin_id.'.png';
                $imagePath = $desFolder.$imageName;
                @file_put_contents($imagePath,file_get_contents($src));
           
               
            }
        }
        
     }



    /**
     * @url GET /pins/by/uid/{uid}
     */
    public function GetPinsByUserId($uid){
        $r = new stdClass();
        $where = array();
        $whereStr = "";

        //if($is_deleted >= 0){
            $where['is_deleted'] = 0;
            // $where['is_active'] = 1;
            // $whereStr .= (empty($whereStr) ? "" : " AND ") . " is_deleted = :is_deleted AND is_active = :is_active";
            $whereStr .= (empty($whereStr) ? "" : " AND ") . " pins.is_deleted = :is_deleted";
        //}

        $where['user_id'] = $uid;
        $whereStr .= (empty($whereStr) ? "" : " AND ") . " pins.user_id = :user_id";

        $whereStr = empty($whereStr) ? $whereStr : "WHERE " . $whereStr;
		/* cons Unverified and verified */
        $sql = "SELECT pins.*, users.first_name, users.last_name, users.email, users.user_type_id, users.date_registered, users.is_banned, users.user_agent, users.activation_code, users.ip  FROM pins INNER JOIN users ON users.user_id = pins.user_id  " . $whereStr;
      //   echo $sql; die;
        $result = Query($sql, $where);

$st = array();
        foreach($result->data as $key => $val){
           // $this->image
             $this->imageCheck($val->pin_id, $val->lat, $val->long);

             $st[] = array('pin_id' => $val->pin_id,
                           'pin_map_img' => $val->pin_id,
                           'user_id' => $val->user_id,
                           'name' => $val->name,
                           'pin_type' => $val->pin_type,
                           'location_name' => $val->location_name,
                           'code' => $val->code,
                           'lat' => $val->lat,
                           'long' => $val->long,
                           'address' => $val->address,
                           'country' => $val->country,
                           'postal_code' => $val->postal_code,
                           'landmark' => $val->landmark,
                           'date_created' => $val->date_created,
                           'activation_code' => $val->activation_code,
                           'mode' => $val->mode,
                           'is_gps' => $val->is_gps,
                           'is_active' => $val->is_active,
                           'is_deleted' => $val->is_deleted,
                           'authorize_time' => $val->authorize_time,
                           'link_click' => $val->link_click,
                           'first_name' => $val->first_name,
                           'last_name' => $val->last_name,
                           'email' => $val->email,
                           'user_type_id' => $val->user_type_id,
                           'date_registered' => $val->date_registered,
                           'is_banned' => $val->is_banned,
                           'user_agent' => $val->user_agent,
                           'activation_code' => $val->activation_code,
                           'ip' => $val->ip
                           ); 
        }
        $r->pins = $st;
        //$r->pins = $result->data;
        $r->rowsCount = $result->count;

        // Get User Data
        $where1 = array();
        $where1['user_id'] = $uid;
        $sql1 = "SELECT * FROM users WHERE user_id = :user_id";
        $result1 = Query($sql1, $where1);
        $r->user = $result1->data[0];
        return $r;
    }

    /**
     * @url GET /pin/by/id/{pin_id}
     * 
     */
    public function GetPin($pin_id){
        $r = new stdClass();
        $whereStr = "";
        $where = array();
        $where['pin_id'] = $pin_id;
        $sql = "SELECT * FROM pins WHERE pin_id = :pin_id";
        $result = Query($sql, $where);
        $r->pin = $result->data;
        return $r;
    }

    /**
     * @url GET /pin/by/code/{code}
     * @access public
     */
    public function GetPinByCode($code){
        $code = strtolower($code);
        $r = new stdClass();
		
        $ip = $_SERVER['REMOTE_ADDR'];
        $date_time = time(); 


        $st = $this->LookupExpiredTimeCheck();
        if(!empty($st)){
        $r->sucess = false;
        $r->limit_reached = true;
        $r->message = "Your ip address has been blacklisted".$st;
        return $r;
        }

        $whereStr = "";
        $where = array();
        $where['code'] = $code;
        $sql = "SELECT * FROM pins INNER JOIN users ON pins.user_id = users.user_id WHERE pins.code = :code AND  pins.is_active = 1 AND pins.is_deleted = 0";
        $result = Query($sql, $where);
        if($result->count > 0){


		
		$sql1 = "SELECT * FROM view_limit WHERE ip = :ip AND view_date = DATE(NOW())";
        $where1 = array();
        $where1['ip'] = $ip;
        $result1 = Query($sql1, $where1);

        // IF this IP was used the very first time. Just log the entry
        if($result1->count == 0){
            $sql2 = "INSERT INTO view_limit (ip, view_date, view_count) VALUES (:ip, DATE(NOW()), 1)";
            $result2 = Query($sql2, $where1);
        } else if($result1->count > 0){
            $lookup_data = $result1->data;

            // User can still do more lookups
            if($lookup_data[0]->view_count < 10){
                $sql2 = "UPDATE view_limit SET view_count = view_count + 1 WHERE ip = :ip AND view_date = DATE(NOW())";
                $result2 = Query($sql2, $where1);
            }

            // The lookups have finished. It cannot be greater than 10. But greater and equal to operator is only added just in case for preventing of unseen bugs
            else if($lookup_data[0]->view_date >= 10){
                $r->sucess = false;
                $r->limit_reached = true;
                $r->message = "You've reached your maximum 10 addypin queries a day";
                return $r;
            }
        }
		
		
			$r->pin = $result->data;
			return $r;
		}else{

            // get invalid addypin
            $sql1 = "SELECT * FROM lookup_limit_invalid WHERE ip = :ip AND lookup_date = DATE(NOW())";
            $where1 = array();
            $where1['ip'] = $ip;
            $result1 = Query($sql1, $where1);
            
           
             // IF this IP was used the very first time. Just log the entry
            if($result1->count == 0){
                
                $sql2 = "INSERT INTO lookup_limit_invalid (ip, lookup_date,lookup_date_time,lookup_count) VALUES (:ip, DATE(NOW()),
                :date_time,1)";
                $where1['date_time'] = $date_time;
                $result2 = Query($sql2, $where1);
            }
            else if($result1->count > 0){
                $lookup_data = $result1->data;

                // User can still do more lookups
                if($lookup_data[0]->lookup_count < 6){
                    $where2 = array();
                    $where2['ip'] = $ip;
                    $sql2 = "UPDATE lookup_limit_invalid SET lookup_count = lookup_count + 1, lookup_date_time = $date_time WHERE ip = :ip AND lookup_date = DATE(NOW())";
                    $result2 = Query($sql2, $where2);
                }else if($lookup_data[0]->lookup_count >= 6){
                    $r->sucess = false;
                    $r->limit_reached = true;
                    $r->message = "You've reached your maximum 6 addypin queries a day";
                    return $r;
                }
            }

        }
		 $r->sucess = false;
         $r->notfound = true;
		 return $r;
    }

    /**
     * @url GET /pin/latest
     * 
     */
    public function GetLatestPin(){
        $r = new stdClass();
        $whereStr = "";
        $where = array();
        $sql = "SELECT * FROM pins ORDER BY pin_id DESC LIMIT 1";
        $result = Query($sql, $where);
        $r->pin = $result->data;
        return $r;
    }
    

     /**
     * @url POST /lookup-expired-time-check
     * @access public
     */
    public function LookupExpiredTimeCheck(){

    $ip = $_SERVER['REMOTE_ADDR'];
    $date_time = time(); 

    $sql1 = "SELECT * FROM lookup_limit_invalid WHERE ip = :ip AND lookup_date = DATE(NOW())";
    $where1 = array();
    $where1['ip'] = $ip;
    $result1 = Query($sql1, $where1);
        if($result1->count > 0){
              
            if($result1->data[0]->lookup_count > 5){
                $authorize_time = $result1->data[0]->lookup_date_time;
                $st = $this->get_time_ago($authorize_time,6);
                if($st > 0){
                // ip block
                    return true;
                }else{
                // not ip block 
                    $where2 = array();
                    $where2['ip'] = $ip;
                    $sql2 = "DELETE FROM lookup_limit_invalid WHERE ip = :ip";
                    $result2 = Query($sql2, $where2);
                    return false;
                }
            }
            
        }
       return false;   
    }
    

    /**
     * @url GET /lookup/{keyword}
     * @access public
     */
    public function Lookup($keyword){
        // First Identify what kind of keyword it is.
        // 1. Code // If it's length is 6
        // 2. Corporate Name etc if it's less/greater than 6 characters
        // 3. Addypin URL, if it contains http, https, dots etc
        // Apply a UNION in all possible results and return

        $sql = array();
        $r = new stdClass();
        $keyword = trim($keyword);
        $where = array();
        $ip = $_SERVER['REMOTE_ADDR'];
        // Check for being a code
        if(strlen($keyword) == 6){
            $sql[] = "SELECT p.*, ut.user_type FROM pins p JOIN users u ON p.user_id = u.user_id JOIN user_types ut ON u.user_type_id = ut.user_type_id WHERE p.code = :keyword AND p.is_deleted = 0 AND u.is_active = 1 AND u.is_banned = 0 AND u.is_deleted = 0 AND p.pin_type = 2";
            //AND p.country = :country
            $where['keyword'] = $keyword;
        }

        // Check for being a corporate name
        if(strpos($keyword, " ") || strlen($keyword) >= 3){
            //$sql[] = "SELECT p.*, CONCAT(u.first_name, ' ', u.last_name) AS full_name, ut.user_type FROM pins p JOIN users u ON p.user_id = u.user_id JOIN user_types ut ON ut.user_type_id = u.user_type_id WHERE ut.user_type = 'corporate' AND (concat(u.first_name,u.last_name) like :corporate_name OR concat(u.first_name,' ',u.last_name) like :corporate_name1) AND p.is_deleted = 0 AND u.is_active = 1 AND u.is_banned = 0 AND u.is_deleted = 0";
            $sql[] = "SELECT p.*, ut.user_type FROM pins p JOIN users u ON p.user_id = u.user_id JOIN user_types ut ON ut.user_type_id = u.user_type_id WHERE ut.user_type = 'corporate' AND p.name LIKE :corporate_name AND p.is_deleted = 0 AND u.is_active = 1 AND u.is_banned = 0 AND u.is_deleted = 0 AND p.pin_type = 2";
            $where['corporate_name'] = "%" . $keyword . "%";
            //$where['corporate_name1'] = "%" . $keyword . "%";
        }

        // Check for being a Addypin URL
        if(strpos($keyword, "http") || strpos($keyword, "https") || strpos($keyword, ".addypin")){
            $keyword = str_replace("https://", "", $keyword);
            $keyword = str_replace("http://", "", $keyword);
            $keyword = str_replace(".addypin.com", "", $keyword);
            $keyword = trim($keyword);
            $sql[] = "SELECT p.*, ut.user_type FROM pins p JOIN users u ON p.user_id = u.user_id JOIN user_types ut ON u.user_type_id = ut.user_type_id WHERE p.code = :keyword1 AND p.is_deleted = 0 AND u.is_active = 1 AND u.is_banned = 0 AND u.is_deleted = 0 AND p.pin_type = 2";
            $where['keyword1'] = $keyword;
        }

       

        // Check to see if this IP has any entry in the database against current Date. If it has, check if it has crossed the daily limit which is 10.
        // If it has, do not run the query and return error.
        // If not, show the result but increase the counter

        $sql1 = "SELECT * FROM lookup_limit WHERE ip = :ip AND lookup_date = DATE(NOW())";
        $where1 = array();
        $where1['ip'] = $ip;
        $result1 = Query($sql1, $where1);

        // IF this IP was used the very first time. Just log the entry
        if($result1->count == 0){
            $sql2 = "INSERT INTO lookup_limit (ip, lookup_date, lookup_count) VALUES (:ip, DATE(NOW()), 1)";
            $result2 = Query($sql2, $where1);
        }
        else if($result1->count > 0){
            $lookup_data = $result1->data;

            // User can still do more lookups
            if($lookup_data[0]->lookup_count < 10){
                $sql2 = "UPDATE lookup_limit SET lookup_count = lookup_count + 1 WHERE ip = :ip AND lookup_date = DATE(NOW())";
                $result2 = Query($sql2, $where1);
            }

            // The lookups have finished. It cannot be greater than 10. But greater and equal to operator is only added just in case for preventing of unseen bugs
            else if($lookup_data[0]->lookup_count >= 10){
                $r->sucess = false;
                $r->limit_reached = true;
                $r->message = "You've reached your maximum 10 addypin queries a day";
                return $r;
            }
        }
        
        $pageContent = file_get_contents('http://freegeoip.net/json/'.$ip);
        $parsedJson  = json_decode($pageContent);
        $country_code = $parsedJson->country_code; 

        // $where['country'] = $country_code;
        $result = Query(implode(" UNION ", $sql), $where);
        $r->pins = $result->data;
        $r->rowsCount = $result->count;
        $r->ip = $ip;
        $r->sql = implode(" UNION ", $sql);
        return $r;
    }

    /**
     * @url GET /remaining-lookup-count
     * @access public
     */
    public function GetRemainingLookupCount(){
        $r = new stdClass();
        $ip = $_SERVER['REMOTE_ADDR'];
        $sql = "SELECT * FROM lookup_limit WHERE ip = :ip AND lookup_date = DATE(NOW())";
        $where = array();
        $where['ip'] = $ip;
        $result = Query($sql, $where);
        $r->remaining = 0;
        if($result->count > 0){
            $r->remaining = 10 - ($result->data[0]->lookup_count);
        }
        return $r;
    }

    /**
     * @url GET /pin/by/email
     * @access public
     */
    public function GetPinByEmail(){
        $result = EmailHelper::SendResponseEmailWithMap(31.473603, 74.377470, 'abc123');
        return $result;
    }


	
    /**
     * @url POST /pin/by/email
     * @access public
     */
    public function GetPinByEmailPost($request_data = NULL){
        $jd = json_decode($request_data['mandrill_events']);
        $from_email = $jd[0]->msg->from_email;
        $to = $jd[0]->msg->to[0][0];
		
		/* $where1['date_created'] = $request_data['mandrill_events'];
		 $sql2 = "INSERT INTO test (mesage, date_created) VALUES (:date_created, DATE(NOW()))";
         $result2 = Query($sql2, $where1);*/
			
		$code = @explode("@", $to)[0];
        $where['code'] = $code;
        $sql = "SELECT p.* FROM pins p JOIN users u ON p.user_id = u.user_id JOIN user_types ut ON u.user_type_id = ut.user_type_id WHERE p.code = :code AND p.is_deleted = 0 AND u.is_active = 1 AND u.is_banned = 0 AND u.is_deleted = 0";
        $result = Query($sql, $where);

        if($result->count > 0){
            $data = $result->data;
            //ob_start();
            //debug($data);
            //$contents = ob_get_contents();
            //ob_end_clean();

            $lat = $data[0]->lat;
            $long = $data[0]->long;
            $c = $data[0]->code;

            $result = EmailHelper::SendResponseEmailWithMap($lat, $long, $c, $from_email);

            //EmailHelper::SendTestEmail($contents);
        }else{
			
			EmailHelper::ErrorEmail($from_email, $to,$code);
		}
       return;
    }

  /**
     * @url GET /pin-test
     * @access public
     */
  function testMail(){
	   $from_email = 'pramod.jain@consagous.com';
        $to = 'UyDE9Q@addypin.com';
	 $st = EmailHelper::ErrorEmail($from_email, $to); 
	 print_r($st);
	 }
    /**
     * @url GET /pin/share/{code}/{email}/{uid}
     */
    public function SharePin($code, $email, $uid){
        $r = new stdClass();
        $r->shares = array();
        $r->status = "";
        // Get user email address
        $where = array();
        $shared = 0;
        $where['user_id'] = $uid;
        $sql = "SELECT * FROM users WHERE user_id = :user_id";
        $result = Query($sql, $where);

        if($result->count > 0){
            $senderEmail = $result->data[0]->email;
            $emails = explode(",", $email);
            foreach($emails AS $e){
                $e = trim($e);
                if(empty($e))
                    continue;

                $emailResult = EmailHelper::SendSharePinEmail($code, $e, $senderEmail);
                //debug($emailResult[0]['status']);
                $r->shares[] = $emailResult;
                if($emailResult[0]['status'] == "sent")
                    $shared++;
            }
        }
        if($shared > 0)
            $r->status = "shared";
        return $r;
    }
    /**
     * @url GET /pin/delete/{pin_id}
     */
    public function DeletePin($pin_id){
        $r = new stdClass();
        $where = array();
        $where['pin_id'] = $pin_id;
        $sql = "UPDATE pins SET is_deleted = 1 WHERE pin_id = :pin_id";
        $result = Query($sql, $where);
        $r->rowsAffected = $result->data;
        return $r;
    }
	
	 /**
     * @url GET /pin/verify/{pin_id}
     */
    public function VerifyPin($pin_id){
        $r = new stdClass();
        $where = array();
        $where['pin_id'] = $pin_id;
        $sql = "UPDATE pins SET is_active = 1 WHERE pin_id = :pin_id";
        $result = Query($sql, $where);
        $r->rowsAffected = $result->data;
        return $r;
    }

    /**
     * @url POST /register-pin
     * @access public
     */
    public function RegisterPin($request_data = NULL){
        $r = new stdClass();


        
        $location_name = $request_data['location'];
        $name = $request_data['name'];
        
        $email = $request_data['email'];
        $type= $request_data['type'] == "personal" ? 1 : 2;
        //$type = $request_data['type'];
        $code = $request_data['code'];
        $lat = $request_data['lat'];
        $is_gps = isset($request_data['is_gps'])?$request_data['is_gps']:0;
        $long = $request_data['lng'];
        $address = $request_data['address'];
        $postal_code = $request_data['postal_code'];
        $landmark = $request_data['landmark'];
        $mode = $request_data['mode'];
        $guid = Functions::generateRandomString();
        
        $uid = UserHelper::CreateUser($email, $type);

    	$result = PinHelper::CreatePin($email,$type,$uid,$location_name,$name,$code,$lat,$long,$address,$postal_code,$landmark,$guid,$is_gps,$mode);

        return $result;
       

    }
	
	 
    /**
     * @url GET /pin-register-test
     * @access public
     */
	 
	
    public function testPin (){
        $where = array();
        $where['uid'] = 1;
        //$where['email'] = "basitnizami@gmail.com";
        $where['location'] = "Model Town";
        $where['name'] = "Basit Nizami";
        $where['code'] = "mod123";
        $where['lat'] = "30.887499352579315";
        $where['lng'] = "75.84553534445797";
        $where['address'] = "model town lahore";
        $where['postal_code'] = "54000";
        $where['landmark'] = "Model Town Park";
        $where['activation_code'] = "lhyywhnrr9wczz9";
        $sql = "INSERT INTO pins (user_id, name, location_name, code, lat, `long`, address, postal_code, landmark, date_created, activation_code) VALUES (:uid, :name, :location, :code, :lat, :lng, :address, :postal_code, :landmark, NOW(), :activation_code)";
        $result = Query($sql, $where);
        return $result;
    }

    /**
     * @url POST /pin-code-availability/{code}/{pin_id}
     * @access public
     */
    public function PinCodeAvailability($code,$pin_id=0){

        $r = new stdClass();
        $where = array();
        $where['code'] = $code;
        $sql = "SELECT COUNT(1) AS Total FROM pins WHERE code = :code AND is_deleted = 0";
        if($pin_id!=0){
            $where['pin_id'] = $pin_id;
            $sql .= " AND pin_id != :pin_id";
        }
        $result = Query($sql, $where);
        $is_valid = preg_match('/^(?=.*[a-zA-Z])(?=.*\d).+$/', $code);
        $r->query = $sql;
        $r->pin_id = $pin_id;
        $r->code = $code;
        $r->available = false;
        $r->valid = false;
        // Means the code is available to be registered
        if($result->data[0]->Total == 0){
            $r->available = true;
        }
        if($is_valid){
            $r->valid = true;
        }
        return $r;
    }

     /**
     * @url POST /expired-time-check/{uid}
     * @access public
     */
    public function ExpiredTimeCheck($uid){
        $r = new stdClass();
        $r->valid = false;
        $where = array();
        
        $where['user_id'] = $uid;
        $sql = "SELECT * FROM users WHERE user_id = :user_id";
       
        
        /*$where['pin_id'] = $pin_id;
        $sql = "SELECT * FROM pins WHERE pin_id = :pin_id";*/
      
        $result = Query($sql, $where);
        if($result->count > 0){

        $authorize_time = $result->data[0]->authorize_time;

       /* $authorize_time = $result->data[0]->date_created;
        $authorize_time = strtotime($date);*/
        $st = $this->get_time_ago($authorize_time);

        if($st > 0){

            if($result->data[0]->link_click == 1){
                 $r->valid = 'expire';

            }else{
                $where1 = array();
                $link_click = 1;
                
                $where1['user_id'] = $uid;
                $sql2 = "UPDATE users SET link_click = $link_click WHERE user_id = :user_id";

               /* $where1['pin_id'] = $pin_id;
                $sql2 = "UPDATE pins SET link_click = $link_click WHERE pin_id = :pin_id";*/
                Query($sql2, $where1);
                $r->valid = true;
            }

        }else{
             $r->valid = 'expire';

        }

        
           
        }
        return $r;
    }
    

     /**
     * @url POST get_time_ago/{pid}
     * @access public
     */
    public function get_time_ago($datetime, $hours = 1){
    
    $time = time() - $datetime; // to get the time since that moment    
    $tokens = array (
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    );  
    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        if(($text == hour) && ($numberOfUnits <= $hours) ){
              return '1';
            }else if($text == minute){
              return '1';
            }else if($text == second){
              return '1';
            }
        return '0';
        return $numberOfUnits.'-'.$text.(($numberOfUnits>1)?'s':'');
    }
}

    /**
     * @url POST /pin-activation/{uid}/{guid}/{pid}
     * @access public
     */
    public function PinActivation($uid, $guid, $pid){
        $r = new stdClass();
        $r->activated = false;
        $r->code = '';
        $r->valid = '';

        $where3 = array();
        $where3['pin_id'] = $pid;
        $sql = "SELECT * FROM pins WHERE pin_id = :pin_id";
        $result3 = Query($sql, $where3);
        if($result3->count > 0){
            $authorize_time = $result3->data[0]->authorize_time;
            $st = $this->get_time_ago($authorize_time);

            if($st > 0){
                if($result3->data[0]->is_active == 1){
                 $r->valid = 'expire-link';
                  return $r; 

                 }

            }else{
             $r->valid = 'expire';  
             return $r; 
            }
        
        }

        $where = array();
        $where['user_id'] = $uid;
        $where['activation_code'] = $guid;
        $where['pin_id'] = $pid;

        
        $sql = "SELECT COUNT(1) AS Total,code FROM pins WHERE user_id = :user_id AND activation_code = :activation_code AND pin_id = :pin_id AND is_active = 0 AND is_deleted = 0";
        $result = Query($sql, $where);

        if($result->data[0]->Total > 0){
            $sql1 = "UPDATE pins SET is_active = 1 WHERE user_id = :user_id AND activation_code = :activation_code AND pin_id = :pin_id AND is_active = 0 AND is_deleted = 0";
            Query($sql1, $where);
            $where2 = array();
            $where2['user_id'] = $uid;
            $sql2 = "UPDATE users SET is_active = 1 WHERE user_id = :user_id";
            Query($sql2, $where2);
            $r->activated = true;
			$r->code = $result->data[0]->code;
        }
        return $r;
    }

    /**
     * @url POST /pin/save-pin
     */
    public function UpdatePin($request_data = NULL){
        $r = new stdClass();
		
		$where = array();
        //debug($request_data);
        $where['pin_id'] = $request_data['pin_id'];
        $where['name'] = $request_data['name'];
        $where['location_name'] = $request_data['location_name'];
        $where['code'] = $request_data['code'];
        $where['lat'] = $request_data['lat'];
        $where['long'] = $request_data['long'];
        $where['address'] = $request_data['address'];
        $where['postal_code'] = $request_data['postal_code'];
        $where['landmark'] = $request_data['landmark'];
        $where['is_active'] = $request_data['is_active'];
        $where['is_deleted'] = 0;
         
		
	$where2 = array();
	$where2['pin_id'] = $request_data['pin_id']; 
	$sql = "SELECT u.email, p.* FROM pins as p JOIN users as u on u.user_id = p.user_id where pin_id = :pin_id"; 
	
    $result = Query($sql,$where2);
   	  if($result->count > 0){
		    $code = trim(strtolower($result->data[0]->code)); 
            $request_code = trim(strtolower($request_data['code'])); 

		   if($code == $request_code){
            //echo 'not same'; die;
            /* addypin changes coordinate */
		    EmailHelper::PinUpdatedEmail($result->data[0]->user_id, $result->data[0]->pin_id, $result->data[0]->activation_code, $result->data[0]->email, $result->data[0]->code);
		   }else if($code != $request_code){
             //echo 'same'; die;
            /* addypin changes addypin */
              EmailHelper::PinUpdatedAddypin($result->data[0]->user_id, $result->data[0]->pin_id, $result->data[0]->activation_code, $result->data[0]->email, $request_data['code'], $result->data[0]->code);
           }
	  }
	  
        $sql = "UPDATE pins SET name = :name, location_name = :location_name, code = :code, lat = :lat, `long` = :long, address = :address, postal_code = :postal_code, landmark = :landmark WHERE pin_id = :pin_id AND is_active = :is_active AND is_deleted = :is_deleted";
        //$sql = "UPDATE pins SET name = :name, location_name = :location_name, code = :code, lat = :lat, `long` = :long WHERE pin_id = :pin_id AND is_active = :is_active AND is_deleted = :is_deleted";
        //$sql = "UPDATE pins SET is_active = 1 WHERE pin_id = :pin_id AND is_active = :is_active AND is_deleted = :is_deleted";
        
        Query($sql, $where);

         $lat = $request_data['lat'];
         $long = $request_data['long'];
         $pid = $request_data['pin_id'];
         $country = $request_data['address'].','.$request_data['postal_code'].','.$request_data['landmark'];

         if(!empty($lat) && !empty($long))
            {
                $src = "https://maps.googleapis.com/maps/api/staticmap?center=$lat,$long&markers=color:red%7C$lat,$long&zoom=15&size=300x350";
                $time = time();
                $desFolder = '/var/www/html/addypin.com/assets/map/';
                $imageName = $pid.'.png';
                $imagePath = $desFolder.$imageName;
                @file_put_contents($imagePath,file_get_contents($src));
            }else  if(!empty($country))
            {
                $src = "https://maps.googleapis.com/maps/api/staticmap?center=$country&markers=color:red%7C$country&zoom=15&size=300x350";
                $time = time();
                $desFolder = '/var/www/html/addypin.com/assets/map/';
                $imageName = $pid.'.png';
                $imagePath = $desFolder.$imageName;
                @file_put_contents($imagePath,file_get_contents($src));
            }

        $r->updated = true;
        return $r;
    }
   
   
   
    /**
     * @url POST /pin/address/save-pin
     */
    public function UpdatePinAddress($request_data = NULL){
        $r = new stdClass();
		
		$where = array();
        $where['pin_id'] = $request_data['pin_id'];
        $where['lat'] = $request_data['lat'];
        $where['long'] = $request_data['long'];
        $where['address'] = $request_data['address'];
         
		
	$where2 = array();
	$where2['pin_id'] = $request_data['pin_id']; 
	$sql = "SELECT u.email, p.* FROM pins as p JOIN users as u on u.user_id = p.user_id where pin_id = :pin_id"; 
	
    $result = Query($sql,$where2);
   	  if($result->count > 0){
		    $code = $result->data[0]->code; 
		    //EmailHelper::PinUpdatedEmail($result->data[0]->user_id, $result->data[0]->pin_id, $result->data[0]->activation_code, $result->data[0]->email, $result->data[0]->code);
	  }
	  
        $sql = "UPDATE pins SET lat = :lat, `long` = :long, address = :address WHERE pin_id = :pin_id";
        Query($sql, $where);
        $r->updated = true;
        return $r;
    }



    /**
     * @url POST /pin/update-pin
     */
    public function UpdatePinByAdmin($request_data = NULL){
        $r = new stdClass();
        $where = array();
        //debug($request_data);
        $where['pin_id'] = $request_data['pin_id'];
        $where['name'] = $request_data['name'];
        $where['location_name'] = $request_data['location_name'];
        $where['code'] = $request_data['code'];
        $where['lat'] = $request_data['lat'];
        $where['long'] = $request_data['long'];
        $where['address'] = $request_data['address'];
        $where['postal_code'] = $request_data['postal_code'];
        $where['landmark'] = $request_data['landmark'];
        $where['is_active'] = $request_data['is_active'];
        $where['is_deleted'] = $request_data['is_deleted'];

        $sql = "UPDATE pins SET name = :name, location_name = :location_name, code = :code, lat = :lat, `long` = :long, address = :address, postal_code = :postal_code, landmark = :landmark, is_active = :is_active, is_deleted = :is_deleted WHERE pin_id = :pin_id";

        Query($sql, $where);
        $r->updated = true;
        return $r;
    }
	
	
	   /**
    * @url GET /pin/activatePin/{pin_id}
    */
   public function activatePin($pin_id){
       $r = new stdClass();
       $where = array();
       $where['pin_id'] = $pin_id;
       $sql = "UPDATE pins SET is_active = 1 WHERE pin_id = :pin_id";
       $result = Query($sql, $where);
       $r->rowsAffected = $result->data;
       return $r;
   }
   
    /**
     * @url GET /pin-cron
     * @access public
     */
	 public function CronPin(){
		$r = new stdClass();
        $where = array();
        $date = date("Y-m-d H:i:s"); 
	   
		   
$sql = "SELECT u.email, p.pin_id, p.pin_type, p.user_id, p.name, p.location_name, p.code, p.lat, p.long, p.address, p.postal_code, p.landmark, p.mode, p.is_gps, p.date_created, DATEDIFF('$date',p.date_created) as diffDate FROM pins as p JOIN users as u on u.user_id = p.user_id where p.is_active = 0 && p.is_deleted = 0 && u.is_active = 1 && u.is_deleted = 0 && DATEDIFF('$date',p.date_created) > 6 GROUP BY p.user_id";

     $result = Query($sql,$where);
   	//echo '<pre/>'; print_r($result->data); die;
	  if($result->count > 0){
		 foreach($result->data as $val){
		   $emailResult = EmailHelper::PinActivationReminderEmail($val->user_id, $val->pin_id, $val->activation_code, $val->email, $val->code);
		 }
	  }
   
   /* delete pin after 6 days */
   $sql = "SELECT u.email, p.pin_id, p.pin_type, p.user_id, p.name, p.location_name, p.code, p.lat, p.long, p.address, p.postal_code, p.landmark, p.mode, p.is_gps, p.date_created, DATEDIFF('$date',p.date_created) as diffDate FROM pins as p JOIN users as u on u.user_id = p.user_id where p.is_active = 0 && p.is_deleted = 0 && u.is_active = 1 && u.is_deleted = 0 && DATEDIFF('$date',p.date_created) > 7 GROUP BY p.user_id";

     $result = Query($sql,$where);
    //echo '<pre/>'; print_r($result->data); die;
      if($result->count > 0){
         foreach($result->data as $val){
            
            $where1 = array();
            $where1['pin_id'] = $val->pin_id;
            $sql = "UPDATE pins SET is_deleted = 1 WHERE pin_id = :pin_id";
            Query($sql, $where1);
           
         }
      }


  }
	 
   
   
}