<?php

class Functions{
    public static function generateRandomString($length = 15){
        $characters = "123456789abcdefghijklmnpqrstuvwxyz";
        $string = "";
        for ($p = 0; $p < $length; $p++)
            $string .= $characters[mt_rand(0, strlen($characters)-1)];
        return $string;
    }

    public static function generateRandomNumber($length = 1){
        $characters = "123456789";
        $string = "";
        for ($p = 0; $p < $length; $p++)
            $string .= $characters[mt_rand(0, strlen($characters)-1)];
        return $string;
    }

    public static function pinExistsInDatabase($pin){
        $where = array();
        $where['code'] = $pin;
        $sql = "SELECT code from pins WHERE code = :code";
        $result = Query($sql, $where);
        return $result->count > 0;
    }
}