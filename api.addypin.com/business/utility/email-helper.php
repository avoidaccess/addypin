<?php
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use \Mailjet\Resources;
class EmailHelper
{
    public static function SendResponseEmailWithMap($lat, $lng, $code, $to=null)
    {
        $gogole_api_key = GOOGLE_API_KEY;
        
        if($to != null && filter_var($to, FILTER_VALIDATE_EMAIL)){   
            
            $url = "http://maps.google.com/maps/api/staticmap?key=".$gogole_api_key."&center=" . $lat . "," . $lng . "&markers=color:red|" . $lat . "," . $lng . "&zoom=15&size=640x640&sensor=false";
            $attachment = file_get_contents($url);
            $attachment_encoded = base64_encode($attachment);
            
            $image_src = '';
            $pin_id = null;
            $sql = "SELECT p.pin_id FROM pins as p JOIN users as u on u.user_id = p.user_id where code = :code"; 
	        $result = Query($sql,['code' => $code]);
   	        if($result->count > 0){
                $pin_id = trim(strtolower($result->data[0]->pin_id)); 
            }
            $imagePath = '/var/www/html/addypin.com/assets/map/'.$pin_id.'.png';
            if(!file_exists($imagePath)) {
                @file_put_contents($imagePath, $attachment);
                $image_src = 'https://www.addypin.com/assets/map/'.$pin_id.'.png';
            }
            //$subject = "Download Map for Reference #: " . $code;
            $subject = "Map for addypin " . strtoupper($code);
            
            $body = file_get_contents(dirname ( __FILE__ )."/templates/map_addypin.php");
            $body = str_replace("#CODE#",$code, $body );
            $body = str_replace("#LAT#",$lat, $body );
            $body = str_replace("#LNG#",$lng, $body );
            $body = str_replace('#API_KEY#',$gogole_api_key, $body );
            //$body = str_replace('#MAP_IMG',$image_src, $body );
            

            $r = EmailHelper::SendEmail($to,$subject,$body,'noreply@addypin.com','Addypin',null);
            return $r;
        } else {
            return false;
        }
    }

        public static function SendSharePinEmail($code, $email, $senderEmail){
        //$mandrill = new Mandrill('jtOedMdGZKj5haYzki6FHg');
        /*$body = "Dear User,<br/><br/>
                Your friend $senderEmail has just shared a pin with you via www.addypin.com. You can view the pin by visiting http://$code.addypin.com or you can send an email to $code@addypin.com and we will reply you back with a map attached.<br/><br/>
                Cheers!<br/>
                The Addypin Team
                ";*/
         /*       $body = "<p style='font-family:calibri;'>Hello Nomad!<br/><br/>
                
                Your friend <a href='mailto:$senderEmail'>$senderEmail</a> has just shared their e-address with you via <a href='https://www.addypin.com'>www.addypin.com</a>.
                You can view the pin by visiting from your mobile <a href='http://$code.addypin.com'>http://$code.addypin.com</a> or you can send an email to <a href='mailto:$code@addypin.com'>$code@F.com</a> and we will reply with a map attached.<br><br>
                
                Don’t facebook and drive!<br/><br/>
                The addypin team<br/><br/></p>
                <hr>
                <div style='text-align:center;color:#ccc;font-size:12px'>addypin is a product of Cairenes.co, “Cairenes Solutions LLC” is registered in Egypt No. 83927</div>
                ";*/

               /* $body ='<head>
                <title>Test</title>
                <style type="text/css">
                    p{
                            
                            -webkit-margin-before: 0;
                            -webkit-margin-after: 0;
                            margin: 10px 0;
                    }
                    .main-div{
                        width:100%;background-color:#F2F2F4; padding-bottom: 15px;
                    }

                    .main-div h4{
                        text-align:center; color:#000; padding-top: 30px; font-size: 24px; margin-top: 0px;
                    }
                    .email-content{
                            width: 77%;
                            background-color: #fff;
                            margin: 0 auto;
                            align-content: center;
                            padding-bottom: 15px;
                            margin-bottom: 78px;
                            padding: 14px;
                    }
                    .email-para{
                        line-height: 22px;font-size: 16px; font-family:"HelveticaNeue-Light","Helvetica Neue Light","Helvetica Regular",Arial,sans-serif;
                    }
                    .user{
                        padding-top: 60px; font-size: 34px; color: #000; text-align:center;
                    }
                    .pin-btn
                    {
                        font-size: 16px;background-color: #1fbad6;padding: 15px;color: #fff;position: relative;text-decoration:none;
                        display: block;
                        max-width: 200px;
                    }
                    .end-para{
                        font-size: 16px; color: #000; text-align:center; padding-bottom:35px;   
                    }

                    .main-div img{
                        padding-top: 33px;
                    }
                    .images{
                        margin-top: 26px;
                    }
                    

                </style>
            </head>
            <body style="background-size:100%;" >

                <div class="main-div" align="center" style="">
                    <img src="https://www.addypin.com/assets/img/logo.png">

                    <div class="email-content" align="center" style=" ">
                        <p class="user" style="">Welcome Nomad!<p>
                            <p class="email-para" style="">Your friend <a href="mailto:'.$senderEmail.'">'.$senderEmail.'</a> has just shared their e-address.

                                <p>You can view the pin by visiting from your mobile</p> <a class="pin-btn pin-new" style="display:block; max-width: 200px" href="http://'.$code.'.addypin.com">http://'.$code.'.addypin.com</a>
                                <br><p> Or you can send an email to </p> <br> <a class="pin-btn pin-new" href="mailto:'.$code.'@addypin.com">'.$code.'@addypin.com</a> <br> <p> and we will reply with a map attached. </p> <br><br>

                                <p class="end-para"  style=""><i> Don’t facebook and drive!<br/><br/>
                                    The addypin team</i></p></p>
                                </div>
                            </div>  
                        </body>';*/




        /*
        $message = array(
            'subject' => "A Pin was shared with you",
            'html' => $body, // or just use 'html' to support HTMl markup
            'from_email' => 'noreply@addypin.com',
            'from_name' => 'Addypin', //optional
            'to' => array(
                //array( 'email' => 'autumnremain@gmail.com' ),
                array( 'email' => $email)
            ),
            'track_opens' => TRUE,
            'track_clicks' => TRUE
        );
       
        $result = $mandrill->messages->send($message);
        return $result;
        */
        //$r = EmailHelper::SendEmail($email,'A Pin was shared with you',$body,'noreply@addypin.com','Addypin');

        $body = file_get_contents(dirname ( __FILE__ )."/templates/share_addypin.php");
        $body = str_replace("#PIN_NAME#",$code, $body );
        $body = str_replace("#SENDER_EMAIL#",$senderEmail, $body );


        $r = EmailHelper::SendEmail($email,'Your friend has shared their addypin',$body,'noreply@addypin.com','Addypin');

        return $r;



    }

    public static function SendManagePinsVerificationEmail($link, $email){
        //$mandrill = new Mandrill('jtOedMdGZKj5haYzki6FHg');

        /*$body = "Dear User,<br/><br/>
                Please click on the following verification link to manage your pins or copy and paste it in your browser address bar.<br/><br/>
                $link <br/><br/>
                Cheers!<br/>
                The Addypin Team
                ";*/
        /*$body = "Dear User,<br/><br/>
                Please click <a href='$link' target='_blank'>here</a> to manage your pins.<br/><br/>
                Cheers!<br/>
                The Addypin Team
                ";*/
       /* $body = "<p style='font-family:calibri;'>Hello Nomad!<br/><br/>
                Please click on the following link to manage your addypins.<br/><br/>
                <a href='$link' target='_blank'>Click me</a><br/><br/>
                Don’t PokemonGo and drive!<br/><br/>
                The addypin team<br><br></p>
                <hr>
                <div style='text-align:center;color:#ccc;font-size:12px'>addypin is a product of Cairenes.co, “Cairenes Solutions LLC” is registered in Egypt No. 83927</div>
        ";
*/
        /*$body = '<head>
        <title>Test</title>
        <style type="text/css">

            .main-div{
                width:100%;background-color:#F2F2F4; padding-bottom: 15px;
            }

            .main-div h4{
                text-align:center; color:#000; padding-top: 30px; font-size: 24px; margin-top: 0px;
            }
            .email-content{
                width:80%;  background-color:#fff; margin: 0 auto; align-content: center; padding-bottom: 15px; margin-bottom:78px;
            }
            .email-para{
                line-height: 22px;font-size: 16px; font-family:"HelveticaNeue-Light","Helvetica Neue Light","Helvetica Regular",Arial,sans-serif;
            }
            .user{
                padding-top: 60px; font-size: 34px; color: #000; text-align:center;
            }
            .pin-btn
            {
                font-size: 16px;background-color: #1fbad6;display: block;padding: 15px;color: #fff;position: relative;width: 112px;text-decoration:none;
            }
            .end-para{
                font-size: 16px; color: #000; text-align:center; padding-bottom:35px;   
            }

            .main-div img{
                padding-top: 33px;
            }
            .images{
                margin-top: 26px;
            }

        </style>
    </head>
    <body style="background-size:100%;" >

        <div class="main-div" align="center" style="">
            <img src="https://www.addypin.com/assets/img/logo.png">

            <div class="email-content" align="center" style=" ">
                <p class="user" style="">Welcome Nomad!<p>
                    <p class="email-para" style="">Please click on the following link to manage your addypins. </p>
                    <p class="email-para"><a class="pin-btn" style="" href="'.$link.'" target="_blank">Click me</a></p>

                    <p class="end-para"  style=""><i> Don’t PokemonGo and drive!<br/><br/>
                        The addypin team</i></p></p>
                    </div>
                </div>  
            </body>';*/







        /*
        $message = array(
            'subject' => "Manage Pins Verification",
            'html' => $body, // or just use 'html' to support HTMl markup
            'from_email' => 'noreply@addypin.com',
            'from_name' => 'Addypin', //optional
            'to' => array(
                //array( 'email' => 'autumnremain@gmail.com' ),
                array( 'email' => $email)
            ),
            'track_opens' => TRUE,
            'track_clicks' => TRUE
        );

        $result = $mandrill->messages->send($message);
        return $result;
        */
        //$r = EmailHelper::SendEmail($email,'Manage Pins Verification',$body,'noreply@addypin.com','Addypin');

        $body = file_get_contents(dirname ( __FILE__ )."/templates/manage_addypin.php");
        $body = str_replace("#LINK#",$link, $body );

        $r = EmailHelper::SendEmail($email,'Manage your addypins',$body,'noreply@addypin.com','Addypin');
        return $r;
    }

    public static function SendTestEmail(){
        //$mandrill = new Mandrill('jtOedMdGZKj5haYzki6FHg');
        $body = "Hello";
       /*
        $message = array(
            'subject' => "Test Data",
            'html' => $body, // or just use 'html' to support HTMl markup
            'from_email' => 'noreply@addypin.com',
            'from_name' => 'Addypin', //optional
            'to' => array(
                array( 'email' => 'basitnizami@gmail.com' )
            ),
            'track_opens' => TRUE,
            'track_clicks' => TRUE
        );

        $result = $mandrill->messages->send($message);
        return $result;
        */
        $r = EmailHelper::SendEmail('rajaishtiaq6@gmail.com','Test Data',$body,'noreply@addypin.com','Addypin');
        return $r;
    }



    public static function SendContactEmail($email, $text){

        $signer = new Sha256();

            $body = file_get_contents(dirname ( __FILE__ )."/templates/feedback.php");
            $body = str_replace("#EMAIL#",$email, $body );
            $body = str_replace("#TEXT#",$text, $body );
            
            $r = EmailHelper::SendEmail("addypin@cairenes.co",'Customer’s feedback from addypin',$body,'noreply@addypin.com','Addypin');
			//$r = EmailHelper::SendEmail("pramod.jain@consagous.com",'Customer’s feedback from addypin',$body,'noreply@addypin.com','Addypin');
            return $r;
        }



        public static function SendRegistrationEmail($email, $type, $guid, $isExistingUser = false){
       // $mandrill = new Mandrill('jtOedMdGZKj5haYzki6FHg');
            $body = "";
            $subject = "";
            $link = "https://" . SITE . ".com/#/register-continue/" . $guid . "/" . $email . "/" . $type;
            if($isExistingUser){
                $subject = "Addypin Registration";
            /*$body = "Dear User,<br/><br/>
                Please click on the following verification link to continue creating your Addypin.<br/><br/>
                $link <br/><br/>
                Cheers!<br/>
                The Addypin Team
                ";*/
                $body = "Dear User,<br/><br/>
                Please click <a href='$link' target='_blank'>here</a> to continue creating your Addypin.<br/><br/>
                Cheers!<br/>
                The Addypin Team
                ";
            }
            else{
                $subject = "Addypin Registration Confirmation";
            /*$body = "Dear User,<br/><br/>
                Thank you very much for choosing Addypin. Please click on the following verification link to continue creating your Addypin.<br/><br/>
                $link <br/><br/>
                Cheers!<br/>
                The Addypin Team
                ";*/
                $body = "Dear User,<br/><br/>
                Thank you very much for choosing Addypin. Please click <a href='$link' target='_blank'>here</a> to continue creating your Addypin.<br/><br/>
                Cheers!<br/>
                The Addypin Team
                ";
            }
        
        $r = EmailHelper::SendEmail($email,$subject,$body,'noreply@addypin.com','Addypin');
        return $r;
    }

    public static function PinActivationEmail($uid, $pid, $guid, $email,$code=""){
        //$mandrill = new Mandrill('jtOedMdGZKj5haYzki6FHg');

        $signer = new Sha256();
        $token = TokenHelper::GenerateToken();

        $link = "http://" . SITE . "/#/verify-pin/".(string)$token."/".$uid."/".$guid."/".$pid;
        $delete_link = "http://" . SITE . "/#/delete-pin/".(string)$token."/".$uid."/".$guid."/".$pid."/".$code;

        /*$body = "Dear User,<br/><br/>
                Please click on the following verification link to activate your pin or copy and paste it in your browser address bar.<br/><br/>
                $link <br/><br/>
                Cheers!<br/>
                The Addypin Team
                ";*/
        /*$body = "Dear User,<br/><br/>
                Please click <a href='$link' target='_blank'>here</a> to activate your pin.<br/><br/>
                Cheers!<br/>
                The Addypin Team
                ";*/
        /*$body = "<p style='font-family:calibri;'>Welcome Nomad!<br/><br/>
                You have just created a new addypin <b>".$code."</b><br><br>
                You can open it from your mobile maps ".$code.".addypin.com or retrieve the map through ".$code."@addypin.com<br><br>
                <i>Please click on the following verification link to activate your addypin. &quot;addypin will be automatically deleted within 7 days if no confirmation received.&quot;<br/><br/>
                <a href='$link' target='_blank'>Click me</a><br/><br/>
                In case you didn't notate this addypin please delete it <a href='$link' target='_blank'>here</a>.<br/><br/>
                <i>Don't drink and drive!<br/><br/>
                The addypin team</i></p>
                <hr>
                <div style='text-align:center;color:#ccc;font-family:calibri;font-size:11px'>addypin is a product of Cairenes.co, “Cairenes Solutions LLC” is registered in Egypt No. 83927</div>
                ";*/
                /*$body = '<head>
                <title>Test</title>
                <style type="text/css">

                    .main-div{
                        width:100%;background-color:#F2F2F4; padding-bottom: 15px;
                    }

                    .main-div h4{
                        text-align:center; color:#000; padding-top: 30px; font-size: 24px; margin-top: 0px;
                    }
                    .email-content{
                        width:80%;  background-color:#fff; margin: 0 auto; align-content: center; padding-bottom: 15px; margin-bottom:78px;
                    }
                    .email-para{
                        line-height: 22px;font-size: 16px; font-family:"HelveticaNeue-Light","Helvetica Neue Light","Helvetica Regular",Arial,sans-serif;
                    }
                    .user{
                        padding-top: 60px; font-size: 34px; color: #000; text-align:center;
                    }
                    .pin-btn
                    {
                        font-size: 16px;background-color: #1fbad6;display: block;padding: 15px;color: #fff;position: relative;width: 112px;text-decoration:none;
                    }
                    .end-para{
                        font-size: 16px; color: #000; text-align:center; padding-bottom:35px;   
                    }

                    .main-div img{
                        padding-top: 33px;
                    }
                    .images{
                        margin-top: 26px;
                    }

                </style>
            </head>
            <body style="background-size:100%;" >

                <div class="main-div" align="center" style="">
                    <img src="https://www.addypin.com/assets/img/logo.png">

                    <div class="email-content" align="center" style="padding:30px">
                        <p class="user" style="">Welcome Nomad!<p>
                            <p class="email-para">You have just created a new addypin <b><a href="https://'.$code.'.addypin.com" target="_blank">'.$code.'</a></b></p>
                            <p class="email-para">You can open it from your mobile maps <a href="https://'.$code.'.addypin.com" target="_blank">'.$code.'.addypin.com</a> or retrieve the map through <a href="mailto:'.$code.'@addypin.com">'.$code.'@addypin.com</a></p>
                            <p class="email-para"><i>Please click on the following link to activate your addypin.<br>&quot;addypin will be automatically deleted within 7 days if no confirmation received.&quot;<br/><br/></p>
                            <p class="email-para" style="">Please click on the following verification button to activate your pin </p>
                            <p class="email-para"><a class="pin-btn" style="" href="'.$link.'" target="_blank">Verfiy Pins</a></p>

                            <p class="email-para">In case you didn\'t notate this addypin please delete it <a href="'.$link.'" target="_blank">here</a>.<br/><br/></p>
                            <p class="end-para"  style=""><i>Don\'t drink and drive!<br/><br/>
                                The addypin team</i></p></p>
                            </div>
                        </div>  
                    </body>';/*
        /*
        $message = array(
            'subject' => "Pin Activation",
            'html' => $body, // or just use 'html' to support HTMl markup
            'from_email' => 'noreply@addypin.com',
            'from_name' => 'Addypin', //optional
            'to' => array(
                //array( 'email' => 'autumnremain@gmail.com' ),
                array( 'email' => $email)
            ),
            'track_opens' => TRUE,
            'track_clicks' => TRUE
        );

        $result = $mandrill->messages->send($message);
        return $result;
        */
        //$r = EmailHelper::SendEmail($email,'Pin Activation',$body,'noreply@addypin.com','Addypin');
        // echo dirname ( __FILE__ );
        // echo "<br/>--------\n";
        // echo "file".__FILE__;
        // die("sending email");
        $sql = "SELECT * from users where user_id = :user_id";
        $where['user_id'] = $uid;
        $result = Query($sql, $where);
        $code_type = $result->data[0]->user_type_id==2?"Business":"Personal";
        $body = file_get_contents(dirname ( __FILE__ )."/templates/new_addypin.php");
        $body = str_replace("#PIN_TYPE#",$code_type, $body );
        $body = str_replace("#PIN_NAME#",$code, $body );
        $body = str_replace("#VERIFY_LINK#",$link, $body );
        $body = str_replace("#DELETE_LINK#",$delete_link, $body );
        

        
        
        

        $r = EmailHelper::SendEmail($email,'Activate your '.$code_type.' addypin – '.$code,$body,'noreply@addypin.com','Addypin');
        return $r;
    }


       public static function PinUpdatedEmail($uid, $pid, $guid, $email,$code=""){
         $signer = new Sha256();
        $token = TokenHelper::GenerateToken();

        $link = "http://" . SITE . "/#/verify-pin/".(string)$token."/".$uid."/".$guid."/".$pid;
        $delete_link = "http://" . SITE . "/#/delete-pin/".(string)$token."/".$uid."/".$guid."/".$pid."/".$code;

       
        $sql = "SELECT * from users where user_id = :user_id";
        $where['user_id'] = $uid;
        $result = Query($sql, $where);
        $code_type = $result->data[0]->user_type_id==2?"Business":"Personal";
        $body = file_get_contents(dirname ( __FILE__ )."/templates/addypin.change.coord.php");   /*update_addypin.php*/
        $body = str_replace("#PIN_TYPE#",$code_type, $body );
        $body = str_replace("#PIN_NAME#",$code, $body );
        $body = str_replace("#VERIFY_LINK#",$link, $body );
        $body = str_replace("#DELETE_LINK#",$delete_link, $body );
        
        $r = EmailHelper::SendEmail($email,"You just updated ".$code_type." addypin – ".$code,$body,'noreply@addypin.com','Addypin');
        return $r;
    }
	

       public static function PinUpdatedAddypin($uid, $pid, $guid, $email,$code="",$old_code=""){
         $signer = new Sha256();
        $token = TokenHelper::GenerateToken();

        $link = "http://" . SITE . "/#/verify-pin/".(string)$token."/".$uid."/".$guid."/".$pid;
        $delete_link = "http://" . SITE . "/#/delete-pin/".(string)$token."/".$uid."/".$guid."/".$pid."/".$code;

       
        $sql = "SELECT * from users where user_id = :user_id";
        $where['user_id'] = $uid;
        $result = Query($sql, $where);
        $code_type = $result->data[0]->user_type_id==2?"Business":"Personal";
        $body = file_get_contents(dirname ( __FILE__ )."/templates/addypin.change.addypin.php");   /*update_addypin.php*/
        $body = str_replace("#PIN_TYPE#",$code_type, $body );
        $body = str_replace("#PIN_NAME#",$code, $body );

        $body = str_replace("#PIN_NAME_OLD#",$old_code, $body );

        $body = str_replace("#VERIFY_LINK#",$link, $body );
        $body = str_replace("#DELETE_LINK#",$delete_link, $body );
        
        $r = EmailHelper::SendEmail($email,"You just updated addypin – ".$code,$body,'noreply@addypin.com','Addypin');
        return $r;
    }

	
       public static function PinActivationReminderEmail($uid, $pid, $guid, $email,$code=""){
        $signer = new Sha256();
        $token = TokenHelper::GenerateToken();

        $link = "http://" . SITE . "/#/verify-pin/".(string)$token."/".$uid."/".$guid."/".$pid;
        $delete_link = "http://" . SITE . "/#/delete-pin/".(string)$token."/".$uid."/".$guid."/".$pid."/".$code;

     
        $sql = "SELECT * from users where user_id = :user_id";
        $where['user_id'] = $uid;
        $result = Query($sql, $where);
        $code_type = $result->data[0]->user_type_id==2?"Business":"Personal";
        $body = file_get_contents(dirname ( __FILE__ )."/templates/reminder_addypin.php");
        $body = str_replace("#PIN_TYPE#",$code_type, $body );
        $body = str_replace("#PIN_NAME#",$code, $body );
        $body = str_replace("#VERIFY_LINK#",$link, $body );
        $body = str_replace("#DELETE_LINK#",$delete_link, $body );
        
        $r = EmailHelper::SendEmail($email,'Reminder Activate your '.$code_type.' addypin – '.$code,$body,'noreply@addypin.com','Addypin');
        return $r;
    }
	


     public static function PinGoogleAnalyticsEmail($addypin_create, $addypin_view, $device_type_desktop, $device_type_iPad, $device_type_windows, $device_type_android, $device_type_iOS, $device_type_unknown){
        $signer = new Sha256();
        $token = TokenHelper::GenerateToken();
        $date = date("Y-m-d"); 
        $body = file_get_contents(dirname ( __FILE__ )."/templates/google_analytics_reports.php");
       
        $body = str_replace("#addypin_create#",$addypin_create, $body );
        $body = str_replace("#addypin_view#",$addypin_view, $body );
        $body = str_replace("#device_type_desktop#",$device_type_desktop, $body );
        $body = str_replace("#device_type_iPad#",$device_type_iPad, $body );

        $body = str_replace("#device_type_windows#",$device_type_windows, $body );
        $body = str_replace("#device_type_android#",$device_type_android, $body );
        $body = str_replace("#device_type_iOS#",$device_type_iOS, $body );
        $body = str_replace("#device_type_unknown#",$device_type_unknown, $body );
        $body = str_replace("#date#",$date, $body );
        
       
        $email = 'addypin.com@gmail.com';
        $r = EmailHelper::SendEmail($email,'Addypin reports :: '.$date, $body, 'noreply@addypin.com','Addypin');
        return $r;
    }


	
	 public static function ErrorEmail($from_email, $to, $code){
        $signer = new Sha256();
        $token = TokenHelper::GenerateToken();

        $body = file_get_contents(dirname ( __FILE__ )."/templates/error404_addypin.php");
    
	    $body = str_replace("#EMAIL#",$to, $body );
        $body = str_replace("#CODE#", strtoupper($code), $body );
        
        $r = EmailHelper::SendEmail($from_email, 'addypin not found!',  $body, 'noreply@addypin.com','Addypin');
        return $r;
    }

    public static function SendEmail($to,$subject,$body,$from_email,$from_name, $attachment_encoded=null){

        $mandrill = new Mandrill('jtOedMdGZKj5haYzki6FHg');

        $message  = array(
            'subject' => $subject ,
            'html' => $body,
            'from_email' => $from_email ,
            'from_name' => $from_name,
            'to' => array(
                array( 'email' => $to)
                ),
            'track_opens' => TRUE,
            'track_clicks' => TRUE
            );

        if($attachment_encoded != null){
            $message['images'] = array(
                array(
                    "type"=> "image/png",
                    "name"=> "mapimg",
                    "content"=> $attachment_encoded
                    )
                );
        }

        // Mail Jet
        $r = self :: sendViaMailjet($to,$subject,$body,$from_email,$from_name, $attachment_encoded);
        if($r['status'] == 1) {
            $result[] = ['status' =>'sent', 'email' => $to];
        } else {
            $result[] = ['status' =>'failed'];
        }
        return $result;
        
        
        // Send Grid
        /*
        $r = self :: sendGrid($to,$subject,$body,$from_email,$from_name, $attachment_encoded);
        if($r >= 200 && $r < 300) {
            $result[] = ['status' =>'sent', 'email' => $to];
        } else {
            $result[] = ['status' =>'failed'];
        }
        return $result;
        
        $result = $mandrill->messages->send($message);
        
        // If email wasn't sent by Mandrell
        if($result[0]['status'] == 'queued') {
            $r = self :: callSimpleMail($to,$subject,$body,$from_email,$from_name, $attachment_encoded);
            if($r) {
                $result[0]['status'] = 'sent';
            }
        }
        */
        return $result;

    }
    
    public static function callSimpleMail($to,$subject,$body,$from_email,$from_name, $attachment_encoded=null){
        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
 
        // Create email headers
        $headers .= 'From: '.$from_email."\r\n".
            'Reply-To: '.$from_email."\r\n" .
            'X-Mailer: PHP/' . phpversion();
        return mail($to,$subject,$body,$headers);
    }

    function sendViaMailjet($to,$subject,$body,$from_email,$from_name, $attachment_encoded=null){
        
        $api_key= 'bcf488b1b1bcf3b4bc3d47221e661a83';
        $secret_key = '952ed7315f71698a9223a5958ac55b2d';
    
        $mj = new \Mailjet\Client($api_key,$secret_key,true,['version' => 'v3.1']);
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => "noreply@addypin.com",
                        'Name' => "Addypin"
                    ],
                    'To' => [
                        [
                        'Email' => $to,
                        'Name' => ""
                        ]
                    ],
                    'Subject' => $subject,
                    'TextPart' => "",
                    'HTMLPart' => $body,
                    'CustomID' => "AddyPinMail".date('c')
                ]
            ]
        ];
        
        if($attachment_encoded != null) {
            $body['Messages'][0]['Attachments'] = [
                                                    [
                                                        'ContentType' => "image/png",
                                                        'Filename' => "map.png",
                                                        'Base64Content' => $attachment_encoded
                                                    ]
                                                ];
        }
        $response = $mj->post(Resources::$Email, ['body' => $body]);
        
        return ['status' => $response->success(), 'data' => $response->getData()];
    }

    public static function sendGrid($to,$subject,$body,$from_email,$from_name, $attachment_encoded=null){
            //Avoidaccess@msn.com / Wild**345
            $email = new \SendGrid\Mail\Mail(); 
            $email->setFrom($from_email, "Addypin");
            $email->setSubject($subject);
            $email->addTo($to);
            //$email->addContent("text/plain", "This is really good !!");
            $email->addContent("text/html", $body);

            if($attachment_encoded != null) {
                $email->addAttachment($attachment_encoded, "image/png");
            }
            


            $sendgrid = new SendGrid("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");
            try {
                $response = $sendgrid->send($email);
                //print $response->statusCode() . "\n";
                //print_r($response->headers());
                //print $response->body() . "\n";
                return $response->statusCode();
            } catch (Exception $e) {
                //echo 'Caught exception: '. $e->getMessage() ."\n";
            }
            return false;
    }
    
    
    public static function mail_attachment($filename, $path, $mailto, $from_mail, $from_name, $replyto, $subject, $message) {
        $file = $path.$filename;
        $file_size = filesize($file);
        $handle = fopen($file, "r");
        $content = fread($handle, $file_size);
        fclose($handle);
        $content = chunk_split(base64_encode($content));
        $uid = md5(uniqid(time()));
        $header = "From: ".$from_name." <".$from_mail.">\r\n";
        $header .= "Reply-To: ".$replyto."\r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
        $header .= "This is a multi-part message in MIME format.\r\n";
        $header .= "--".$uid."\r\n";
        $header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
        $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $header .= $message."\r\n\r\n";
        $header .= "--".$uid."\r\n";
        $header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
        $header .= "Content-Transfer-Encoding: base64\r\n";
        $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
        $header .= $content."\r\n\r\n";
        $header .= "--".$uid."--";
        if (mail($mailto, $subject, "", $header)) {
        echo "mail send ... OK"; // or use booleans here
        } else {
        echo "mail send ... ERROR!";
        }
    }

}