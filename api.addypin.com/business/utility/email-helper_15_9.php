<?php
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
class EmailHelper
{
    public static function SendResponseEmailWithMap($lat, $lng, $code, $to)
    {
        $url = "http://maps.google.com/maps/api/staticmap?center=" . $lat . "," . $lng . "&markers=color:red|" . $lat . "," . $lng . "&zoom=15&size=640x640&sensor=false";

        // $mandrill = new Mandrill('jtOedMdGZKj5haYzki6FHg');

        $attachment = file_get_contents($url);
        $attachment_encoded = base64_encode($attachment);
        /*
        $message = array(
            'subject' => "Download Map for Reference #: " . $code,
            'html' => "<img src='cid:mapimg' />", // or just use 'html' to support HTMl markup
            'from_email' => 'noreply@addypin.com',
            'from_name' => 'Map Request', //optional
            'to' => array(
                //array( 'email' => 'autumnremain@gmail.com' ),
                //array( 'email' => 'basitnizami@gmail.com' ),
                //array( 'email' => 'amr.samir@cairenes.co' )
                array( 'email' => $to )
            ),
            /*"attachments" => array(
                array(
                    'content' => $attachment_encoded,
                    'type' => "image/png",
                    'name' => 'map.png',
                )
                ),*/
            /*
            "images"=>array(
                array(
                    "type"=> "image/png",
                    "name"=> "mapimg",
                    "content"=> $attachment_encoded
                )
            ),
            'track_opens' => TRUE,
            'track_clicks' => TRUE
        );

        $result = $mandrill->messages->send($message);
        return $result;
        */
        //$subject = "Download Map for Reference #: " . $code;
        $subject = "Map for addypin " . strtoupper($code);
        
        //$body = "<img src='cid:mapimg' />";
        /*$body = "<p style='font-family:calibri;'>Hello Nomad!<br><br>
        Here’s how the location looks like if you’re going with a drone!<br><br>
        <img src='cid:mapimg' /><br><br>
        Don’t tinder and drive!<br><br>
        The addypin team<br><br></p>
        <hr>
        <div style='text-align:center;color:#ccc;font-size:12px'>addypin is a product of Cairenes.co, “Cairenes Solutions LLC” is registered in Egypt No. 83927</div>";

        $body = '<head>
        <title>Test</title>
        <style type="text/css">

            .main-div{
                width:100%;background-color:#F2F2F4; padding-bottom: 15px;
            }

            .main-div h4{
                text-align:center; color:#000; padding-top: 30px; font-size: 24px; margin-top: 0px;
            }
            .email-content{
                width:80%;  background-color:#fff; margin: 0 auto; align-content: center; padding-bottom: 15px; margin-bottom:78px;
            }
            .email-para{
                line-height: 22px;font-size: 16px; font-family:"HelveticaNeue-Light","Helvetica Neue Light","Helvetica Regular",Arial,sans-serif;
            }
            .user{
                padding-top: 60px; font-size: 34px; color: #000; text-align:center;
            }
            .pin-btn
            {
                font-size: 16px;background-color: #1fbad6;padding: 15px;color: #fff;position: relative;width: 112px;text-decoration:none;
            }
            .end-para{
                font-size: 16px; color: #000; text-align:center; padding-bottom:35px;   
            }

            .main-div img{
                padding-top: 33px;
                max-width:100%;
            }
            .images{
                margin-top: 26px;
            }

        </style>
    </head>
    <body style="background-size:100%;" >
        <div class="main-div" align="center" style="">
            <img src="https://www.addypin.com/assets/img/logo.png">

            <div class="email-content" align="center" style=" ">
                <p class="user" style="">Hello Nomad!<p>
                    <p class="email-para">Here’s how the location looks like if you’re going with a drone!</p>
                    <p class="email-para"><img src="cid:mapimg" />
                    </p>
                    <p class="email-para"><a class="pin-btn" style="" href="http://maps.google.com/maps?ll='.$lat.','.$lng.'" target="_blank">View on Google Maps</a></p>
                    <p class="end-para"  style="">
                        <i>
                            Don\'t tinder and drive!<br/>
                            The addypin team
                        </i>
                    </p>
                </div>
                <div>
                </div>

            </body>
            ';*/

            $body = file_get_contents(dirname ( __FILE__ )."/templates/map_addypin.php");
            $body = str_replace("#CODE#",$code, $body );
            $body = str_replace("#LAT#",$lat, $body );
            $body = str_replace("#LNG#",$lng, $body );

            $r = EmailHelper::SendEmail($to,$subject,$body,'noreply@addypin.com','Addypin',$attachment_encoded);
            return $r;
        }

        public static function SendSharePinEmail($code, $email, $senderEmail){
        //$mandrill = new Mandrill('jtOedMdGZKj5haYzki6FHg');
        /*$body = "Dear User,<br/><br/>
                Your friend $senderEmail has just shared a pin with you via www.addypin.com. You can view the pin by visiting http://$code.addypin.com or you can send an email to $code@addypin.com and we will reply you back with a map attached.<br/><br/>
                Cheers!<br/>
                The Addypin Team
                ";*/
         /*       $body = "<p style='font-family:calibri;'>Hello Nomad!<br/><br/>
                
                Your friend <a href='mailto:$senderEmail'>$senderEmail</a> has just shared their e-address with you via <a href='https://www.addypin.com'>www.addypin.com</a>.
                You can view the pin by visiting from your mobile <a href='http://$code.addypin.com'>http://$code.addypin.com</a> or you can send an email to <a href='mailto:$code@addypin.com'>$code@F.com</a> and we will reply with a map attached.<br><br>
                
                Don’t facebook and drive!<br/><br/>
                The addypin team<br/><br/></p>
                <hr>
                <div style='text-align:center;color:#ccc;font-size:12px'>addypin is a product of Cairenes.co, “Cairenes Solutions LLC” is registered in Egypt No. 83927</div>
                ";*/

               /* $body ='<head>
                <title>Test</title>
                <style type="text/css">
                    p{
                            
                            -webkit-margin-before: 0;
                            -webkit-margin-after: 0;
                            margin: 10px 0;
                    }
                    .main-div{
                        width:100%;background-color:#F2F2F4; padding-bottom: 15px;
                    }

                    .main-div h4{
                        text-align:center; color:#000; padding-top: 30px; font-size: 24px; margin-top: 0px;
                    }
                    .email-content{
                            width: 77%;
                            background-color: #fff;
                            margin: 0 auto;
                            align-content: center;
                            padding-bottom: 15px;
                            margin-bottom: 78px;
                            padding: 14px;
                    }
                    .email-para{
                        line-height: 22px;font-size: 16px; font-family:"HelveticaNeue-Light","Helvetica Neue Light","Helvetica Regular",Arial,sans-serif;
                    }
                    .user{
                        padding-top: 60px; font-size: 34px; color: #000; text-align:center;
                    }
                    .pin-btn
                    {
                        font-size: 16px;background-color: #1fbad6;padding: 15px;color: #fff;position: relative;text-decoration:none;
                        display: block;
                        max-width: 200px;
                    }
                    .end-para{
                        font-size: 16px; color: #000; text-align:center; padding-bottom:35px;   
                    }

                    .main-div img{
                        padding-top: 33px;
                    }
                    .images{
                        margin-top: 26px;
                    }
                    

                </style>
            </head>
            <body style="background-size:100%;" >

                <div class="main-div" align="center" style="">
                    <img src="https://www.addypin.com/assets/img/logo.png">

                    <div class="email-content" align="center" style=" ">
                        <p class="user" style="">Welcome Nomad!<p>
                            <p class="email-para" style="">Your friend <a href="mailto:'.$senderEmail.'">'.$senderEmail.'</a> has just shared their e-address.

                                <p>You can view the pin by visiting from your mobile</p> <a class="pin-btn pin-new" style="display:block; max-width: 200px" href="http://'.$code.'.addypin.com">http://'.$code.'.addypin.com</a>
                                <br><p> Or you can send an email to </p> <br> <a class="pin-btn pin-new" href="mailto:'.$code.'@addypin.com">'.$code.'@addypin.com</a> <br> <p> and we will reply with a map attached. </p> <br><br>

                                <p class="end-para"  style=""><i> Don’t facebook and drive!<br/><br/>
                                    The addypin team</i></p></p>
                                </div>
                            </div>  
                        </body>';*/




        /*
        $message = array(
            'subject' => "A Pin was shared with you",
            'html' => $body, // or just use 'html' to support HTMl markup
            'from_email' => 'noreply@addypin.com',
            'from_name' => 'Addypin', //optional
            'to' => array(
                //array( 'email' => 'autumnremain@gmail.com' ),
                array( 'email' => $email)
            ),
            'track_opens' => TRUE,
            'track_clicks' => TRUE
        );
       
        $result = $mandrill->messages->send($message);
        return $result;
        */
        //$r = EmailHelper::SendEmail($email,'A Pin was shared with you',$body,'noreply@addypin.com','Addypin');

        $body = file_get_contents(dirname ( __FILE__ )."/templates/share_addypin.php");
        $body = str_replace("#PIN_NAME#",$code, $body );
        $body = str_replace("#SENDER_EMAIL#",$senderEmail, $body );


        $r = EmailHelper::SendEmail($email,'Your friend has shared their addypin',$body,'noreply@addypin.com','Addypin');

        return $r;



    }

    public static function SendManagePinsVerificationEmail($link, $email){
        //$mandrill = new Mandrill('jtOedMdGZKj5haYzki6FHg');

        /*$body = "Dear User,<br/><br/>
                Please click on the following verification link to manage your pins or copy and paste it in your browser address bar.<br/><br/>
                $link <br/><br/>
                Cheers!<br/>
                The Addypin Team
                ";*/
        /*$body = "Dear User,<br/><br/>
                Please click <a href='$link' target='_blank'>here</a> to manage your pins.<br/><br/>
                Cheers!<br/>
                The Addypin Team
                ";*/
       /* $body = "<p style='font-family:calibri;'>Hello Nomad!<br/><br/>
                Please click on the following link to manage your addypins.<br/><br/>
                <a href='$link' target='_blank'>Click me</a><br/><br/>
                Don’t PokemonGo and drive!<br/><br/>
                The addypin team<br><br></p>
                <hr>
                <div style='text-align:center;color:#ccc;font-size:12px'>addypin is a product of Cairenes.co, “Cairenes Solutions LLC” is registered in Egypt No. 83927</div>
        ";
*/
        /*$body = '<head>
        <title>Test</title>
        <style type="text/css">

            .main-div{
                width:100%;background-color:#F2F2F4; padding-bottom: 15px;
            }

            .main-div h4{
                text-align:center; color:#000; padding-top: 30px; font-size: 24px; margin-top: 0px;
            }
            .email-content{
                width:80%;  background-color:#fff; margin: 0 auto; align-content: center; padding-bottom: 15px; margin-bottom:78px;
            }
            .email-para{
                line-height: 22px;font-size: 16px; font-family:"HelveticaNeue-Light","Helvetica Neue Light","Helvetica Regular",Arial,sans-serif;
            }
            .user{
                padding-top: 60px; font-size: 34px; color: #000; text-align:center;
            }
            .pin-btn
            {
                font-size: 16px;background-color: #1fbad6;display: block;padding: 15px;color: #fff;position: relative;width: 112px;text-decoration:none;
            }
            .end-para{
                font-size: 16px; color: #000; text-align:center; padding-bottom:35px;   
            }

            .main-div img{
                padding-top: 33px;
            }
            .images{
                margin-top: 26px;
            }

        </style>
    </head>
    <body style="background-size:100%;" >

        <div class="main-div" align="center" style="">
            <img src="https://www.addypin.com/assets/img/logo.png">

            <div class="email-content" align="center" style=" ">
                <p class="user" style="">Welcome Nomad!<p>
                    <p class="email-para" style="">Please click on the following link to manage your addypins. </p>
                    <p class="email-para"><a class="pin-btn" style="" href="'.$link.'" target="_blank">Click me</a></p>

                    <p class="end-para"  style=""><i> Don’t PokemonGo and drive!<br/><br/>
                        The addypin team</i></p></p>
                    </div>
                </div>  
            </body>';*/







        /*
        $message = array(
            'subject' => "Manage Pins Verification",
            'html' => $body, // or just use 'html' to support HTMl markup
            'from_email' => 'noreply@addypin.com',
            'from_name' => 'Addypin', //optional
            'to' => array(
                //array( 'email' => 'autumnremain@gmail.com' ),
                array( 'email' => $email)
            ),
            'track_opens' => TRUE,
            'track_clicks' => TRUE
        );

        $result = $mandrill->messages->send($message);
        return $result;
        */
        //$r = EmailHelper::SendEmail($email,'Manage Pins Verification',$body,'noreply@addypin.com','Addypin');

        $body = file_get_contents(dirname ( __FILE__ )."/templates/manage_addypin.php");
        $body = str_replace("#LINK#",$link, $body );

        $r = EmailHelper::SendEmail($email,'Manage your addypins',$body,'noreply@addypin.com','Addypin');
        return $r;
    }

    public static function SendTestEmail(){
        //$mandrill = new Mandrill('jtOedMdGZKj5haYzki6FHg');
        $body = "Hello";
       /*
        $message = array(
            'subject' => "Test Data",
            'html' => $body, // or just use 'html' to support HTMl markup
            'from_email' => 'noreply@addypin.com',
            'from_name' => 'Addypin', //optional
            'to' => array(
                array( 'email' => 'basitnizami@gmail.com' )
            ),
            'track_opens' => TRUE,
            'track_clicks' => TRUE
        );

        $result = $mandrill->messages->send($message);
        return $result;
        */
        $r = EmailHelper::SendEmail('rajaishtiaq6@gmail.com','Test Data',$body,'noreply@addypin.com','Addypin');
        return $r;
    }



    public static function SendContactEmail($email, $text){

        $signer = new Sha256();

            $body = file_get_contents(dirname ( __FILE__ )."/templates/feedback.php");
            $body = str_replace("#EMAIL#",$email, $body );
            $body = str_replace("#TEXT#",$text, $body );
            
            $r = EmailHelper::SendEmail("addypin@cairenes.co",'Customer’s feedback from addypin',$body,'noreply@addypin.com','Addypin');
			//$r = EmailHelper::SendEmail("pramod.jain@consagous.com",'Customer’s feedback from addypin',$body,'noreply@addypin.com','Addypin');
            return $r;
        }



        public static function SendRegistrationEmail($email, $type, $guid, $isExistingUser = false){
       // $mandrill = new Mandrill('jtOedMdGZKj5haYzki6FHg');
            $body = "";
            $subject = "";
            $link = "https://" . SITE . ".com/#/register-continue/" . $guid . "/" . $email . "/" . $type;
            if($isExistingUser){
                $subject = "Addypin Registration";
            /*$body = "Dear User,<br/><br/>
                Please click on the following verification link to continue creating your Addypin.<br/><br/>
                $link <br/><br/>
                Cheers!<br/>
                The Addypin Team
                ";*/
                $body = "Dear User,<br/><br/>
                Please click <a href='$link' target='_blank'>here</a> to continue creating your Addypin.<br/><br/>
                Cheers!<br/>
                The Addypin Team
                ";
            }
            else{
                $subject = "Addypin Registration Confirmation";
            /*$body = "Dear User,<br/><br/>
                Thank you very much for choosing Addypin. Please click on the following verification link to continue creating your Addypin.<br/><br/>
                $link <br/><br/>
                Cheers!<br/>
                The Addypin Team
                ";*/
                $body = "Dear User,<br/><br/>
                Thank you very much for choosing Addypin. Please click <a href='$link' target='_blank'>here</a> to continue creating your Addypin.<br/><br/>
                Cheers!<br/>
                The Addypin Team
                ";
            }
        
        $r = EmailHelper::SendEmail($email,$subject,$body,'noreply@addypin.com','Addypin');
        return $r;
    }

    public static function PinActivationEmail($uid, $pid, $guid, $email,$code=""){
        //$mandrill = new Mandrill('jtOedMdGZKj5haYzki6FHg');

        $signer = new Sha256();
        $token = TokenHelper::GenerateToken();

        $link = "http://" . SITE . "/#/verify-pin/".(string)$token."/".$uid."/".$guid."/".$pid;
        $delete_link = "http://" . SITE . "/#/delete-pin/".(string)$token."/".$uid."/".$guid."/".$pid."/".$code;

        /*$body = "Dear User,<br/><br/>
                Please click on the following verification link to activate your pin or copy and paste it in your browser address bar.<br/><br/>
                $link <br/><br/>
                Cheers!<br/>
                The Addypin Team
                ";*/
        /*$body = "Dear User,<br/><br/>
                Please click <a href='$link' target='_blank'>here</a> to activate your pin.<br/><br/>
                Cheers!<br/>
                The Addypin Team
                ";*/
        /*$body = "<p style='font-family:calibri;'>Welcome Nomad!<br/><br/>
                You have just created a new addypin <b>".$code."</b><br><br>
                You can open it from your mobile maps ".$code.".addypin.com or retrieve the map through ".$code."@addypin.com<br><br>
                <i>Please click on the following verification link to activate your addypin. &quot;addypin will be automatically deleted within 7 days if no confirmation received.&quot;<br/><br/>
                <a href='$link' target='_blank'>Click me</a><br/><br/>
                In case you didn't notate this addypin please delete it <a href='$link' target='_blank'>here</a>.<br/><br/>
                <i>Don't drink and drive!<br/><br/>
                The addypin team</i></p>
                <hr>
                <div style='text-align:center;color:#ccc;font-family:calibri;font-size:11px'>addypin is a product of Cairenes.co, “Cairenes Solutions LLC” is registered in Egypt No. 83927</div>
                ";*/
                /*$body = '<head>
                <title>Test</title>
                <style type="text/css">

                    .main-div{
                        width:100%;background-color:#F2F2F4; padding-bottom: 15px;
                    }

                    .main-div h4{
                        text-align:center; color:#000; padding-top: 30px; font-size: 24px; margin-top: 0px;
                    }
                    .email-content{
                        width:80%;  background-color:#fff; margin: 0 auto; align-content: center; padding-bottom: 15px; margin-bottom:78px;
                    }
                    .email-para{
                        line-height: 22px;font-size: 16px; font-family:"HelveticaNeue-Light","Helvetica Neue Light","Helvetica Regular",Arial,sans-serif;
                    }
                    .user{
                        padding-top: 60px; font-size: 34px; color: #000; text-align:center;
                    }
                    .pin-btn
                    {
                        font-size: 16px;background-color: #1fbad6;display: block;padding: 15px;color: #fff;position: relative;width: 112px;text-decoration:none;
                    }
                    .end-para{
                        font-size: 16px; color: #000; text-align:center; padding-bottom:35px;   
                    }

                    .main-div img{
                        padding-top: 33px;
                    }
                    .images{
                        margin-top: 26px;
                    }

                </style>
            </head>
            <body style="background-size:100%;" >

                <div class="main-div" align="center" style="">
                    <img src="https://www.addypin.com/assets/img/logo.png">

                    <div class="email-content" align="center" style="padding:30px">
                        <p class="user" style="">Welcome Nomad!<p>
                            <p class="email-para">You have just created a new addypin <b><a href="https://'.$code.'.addypin.com" target="_blank">'.$code.'</a></b></p>
                            <p class="email-para">You can open it from your mobile maps <a href="https://'.$code.'.addypin.com" target="_blank">'.$code.'.addypin.com</a> or retrieve the map through <a href="mailto:'.$code.'@addypin.com">'.$code.'@addypin.com</a></p>
                            <p class="email-para"><i>Please click on the following link to activate your addypin.<br>&quot;addypin will be automatically deleted within 7 days if no confirmation received.&quot;<br/><br/></p>
                            <p class="email-para" style="">Please click on the following verification button to activate your pin </p>
                            <p class="email-para"><a class="pin-btn" style="" href="'.$link.'" target="_blank">Verfiy Pins</a></p>

                            <p class="email-para">In case you didn\'t notate this addypin please delete it <a href="'.$link.'" target="_blank">here</a>.<br/><br/></p>
                            <p class="end-para"  style=""><i>Don\'t drink and drive!<br/><br/>
                                The addypin team</i></p></p>
                            </div>
                        </div>  
                    </body>';/*
        /*
        $message = array(
            'subject' => "Pin Activation",
            'html' => $body, // or just use 'html' to support HTMl markup
            'from_email' => 'noreply@addypin.com',
            'from_name' => 'Addypin', //optional
            'to' => array(
                //array( 'email' => 'autumnremain@gmail.com' ),
                array( 'email' => $email)
            ),
            'track_opens' => TRUE,
            'track_clicks' => TRUE
        );

        $result = $mandrill->messages->send($message);
        return $result;
        */
        //$r = EmailHelper::SendEmail($email,'Pin Activation',$body,'noreply@addypin.com','Addypin');
        // echo dirname ( __FILE__ );
        // echo "<br/>--------\n";
        // echo "file".__FILE__;
        // die("sending email");
        $sql = "SELECT * from users where user_id = :user_id";
        $where['user_id'] = $uid;
        $result = Query($sql, $where);
        $code_type = $result->data[0]->user_type_id==2?"Business":"Personal";
        $body = file_get_contents(dirname ( __FILE__ )."/templates/new_addypin.php");
        $body = str_replace("#PIN_TYPE#",$code_type, $body );
        $body = str_replace("#PIN_NAME#",$code, $body );
        $body = str_replace("#VERIFY_LINK#",$link, $body );
        $body = str_replace("#DELETE_LINK#",$delete_link, $body );
        

        
        
        

        $r = EmailHelper::SendEmail($email,'Activate your '.$code_type.' addypin – '.$code,$body,'noreply@addypin.com','Addypin');
        return $r;
    }


       public static function PinUpdatedEmail($uid, $pid, $guid, $email,$code=""){
         $signer = new Sha256();
        $token = TokenHelper::GenerateToken();

        $link = "http://" . SITE . "/#/verify-pin/".(string)$token."/".$uid."/".$guid."/".$pid;
        $delete_link = "http://" . SITE . "/#/delete-pin/".(string)$token."/".$uid."/".$guid."/".$pid."/".$code;

       
        $sql = "SELECT * from users where user_id = :user_id";
        $where['user_id'] = $uid;
        $result = Query($sql, $where);
        $code_type = $result->data[0]->user_type_id==2?"Business":"Personal";
        $body = file_get_contents(dirname ( __FILE__ )."/templates/addypin.change.coord.php");   /*update_addypin.php*/
        $body = str_replace("#PIN_TYPE#",$code_type, $body );
        $body = str_replace("#PIN_NAME#",$code, $body );
        $body = str_replace("#VERIFY_LINK#",$link, $body );
        $body = str_replace("#DELETE_LINK#",$delete_link, $body );
        
        $r = EmailHelper::SendEmail($email,"You just updated ".$code_type." addypin – ".$code,$body,'noreply@addypin.com','Addypin');
        return $r;
    }
	

       public static function PinUpdatedAddypin($uid, $pid, $guid, $email,$code="",$old_code=""){
         $signer = new Sha256();
        $token = TokenHelper::GenerateToken();

        $link = "http://" . SITE . "/#/verify-pin/".(string)$token."/".$uid."/".$guid."/".$pid;
        $delete_link = "http://" . SITE . "/#/delete-pin/".(string)$token."/".$uid."/".$guid."/".$pid."/".$code;

       
        $sql = "SELECT * from users where user_id = :user_id";
        $where['user_id'] = $uid;
        $result = Query($sql, $where);
        $code_type = $result->data[0]->user_type_id==2?"Business":"Personal";
        $body = file_get_contents(dirname ( __FILE__ )."/templates/addypin.change.addypin.php");   /*update_addypin.php*/
        $body = str_replace("#PIN_TYPE#",$code_type, $body );
        $body = str_replace("#PIN_NAME#",$code, $body );

        $body = str_replace("#PIN_NAME_OLD#",$old_code, $body );

        $body = str_replace("#VERIFY_LINK#",$link, $body );
        $body = str_replace("#DELETE_LINK#",$delete_link, $body );
        
        $r = EmailHelper::SendEmail($email,"You just updated addypin – ".$code,$body,'noreply@addypin.com','Addypin');
        return $r;
    }

	
       public static function PinActivationReminderEmail($uid, $pid, $guid, $email,$code=""){
        $signer = new Sha256();
        $token = TokenHelper::GenerateToken();

        $link = "http://" . SITE . "/#/verify-pin/".(string)$token."/".$uid."/".$guid."/".$pid;
        $delete_link = "http://" . SITE . "/#/delete-pin/".(string)$token."/".$uid."/".$guid."/".$pid."/".$code;

     
        $sql = "SELECT * from users where user_id = :user_id";
        $where['user_id'] = $uid;
        $result = Query($sql, $where);
        $code_type = $result->data[0]->user_type_id==2?"Business":"Personal";
        $body = file_get_contents(dirname ( __FILE__ )."/templates/reminder_addypin.php");
        $body = str_replace("#PIN_TYPE#",$code_type, $body );
        $body = str_replace("#PIN_NAME#",$code, $body );
        $body = str_replace("#VERIFY_LINK#",$link, $body );
        $body = str_replace("#DELETE_LINK#",$delete_link, $body );
        
        $r = EmailHelper::SendEmail($email,'Reminder Activate your '.$code_type.' addypin – '.$code,$body,'noreply@addypin.com','Addypin');
        return $r;
    }
	
	
	 public static function ErrorEmail($from_email, $to, $code){
        $signer = new Sha256();
        $token = TokenHelper::GenerateToken();

        $body = file_get_contents(dirname ( __FILE__ )."/templates/error404_addypin.php");
    
	    $body = str_replace("#EMAIL#",$to, $body );
        $body = str_replace("#CODE#", strtoupper($code), $body );
        
        $r = EmailHelper::SendEmail($from_email, 'addypin not found!',  $body, 'noreply@addypin.com','Addypin');
        return $r;
    }

    public function SendEmail($to,$subject,$body,$from_email,$from_name, $attachment_encoded=null){

        $mandrill = new Mandrill('jtOedMdGZKj5haYzki6FHg');

        $message  = array(
            'subject' => $subject ,
            'html' => $body,
            'from_email' => $from_email ,
            'from_name' => $from_name,
            'to' => array(
                array( 'email' => $to)
                ),
            'track_opens' => TRUE,
            'track_clicks' => TRUE
            );

        if($attachment_encoded != null){
            $message['images'] = array(
                array(
                    "type"=> "image/png",
                    "name"=> "mapimg",
                    "content"=> $attachment_encoded
                    )
                );
        }



        $result = $mandrill->messages->send($message);
        return $result;

    }

}