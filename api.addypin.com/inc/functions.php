<?php
function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}
function debug($str){
    echo "<pre>";
    print_r($str);
    echo "</pre>";
}
function Query($sql, $where){
    //return "ok0";
    // New DB Object
    $db = new DB();
    //return "ok1";
    // Run the required SQL Statement and Count the number of records affected/returned
    $sql = $db->query($sql, $where);
    //return "ok2";
    $count = $db->rowsCount;
    //return "ok3";
    // Create a result object and return it
    $result = new stdClass();
    $result->data = $sql;
    //return "ok4";
    $result->count = $count;
    //return "ok5";
    $result->db = $db;
    //return "ok6";
    return $result;
}

if( !function_exists('apache_request_headers') ) {
    ///
    function apache_request_headers() {
      $arh = array();
      $rx_http = '/\AHTTP_/';
      foreach($_SERVER as $key => $val) {
        if( preg_match($rx_http, $key) ) {
          $arh_key = preg_replace($rx_http, '', $key);
          $rx_matches = array();
          // do some nasty string manipulations to restore the original letter case
          // this should work in most cases
          $rx_matches = explode('_', $arh_key);
          if( count($rx_matches) > 0 and strlen($arh_key) > 2 ) {
            foreach($rx_matches as $ak_key => $ak_val) $rx_matches[$ak_key] = ucfirst($ak_val);
            $arh_key = implode('-', $rx_matches);
          }
          $arh[$arh_key] = $val;
        }
      }
    
        return($arh);
    }
}

function getLocationByIp($ip){
  $url = 'http://www.geoplugin.net/json.gp?ip='.$ip;
  $content = file_get_contents($url);
  $data = json_decode($content, 1);
  
  return ['country_code' => $data['geoplugin_countryCode'], 'country_name' => $data['geoplugin_countryName'], 'city' => $data['geoplugin_city'], 'lat'=> $data['geoplugin_latitude'], 'long'=> $data['geoplugin_longitude']];
}
