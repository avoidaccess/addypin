angular.module('main').factory('url', function ($timeout, $http, envService){
    return {
        apiUrl: function (url){
            console.log(envService);
            var apiUrl = envService.read('apiUrl');
            return apiUrl + url;
        },
        subdomainUrl: function (subdomain){
            var url = "//" + subdomain + envService.read('subdomainUrl');
            return url;
        },
        subdomainUrlDisplay: function (subdomain){
            var url = "http://" + subdomain + envService.read('subdomainUrl');
            return url;
        },
        host: function (h){

            if(h.indexOf("addypin.com") != -1)
                return 'production';
            else if(h.indexOf("addypin.digitalmeetings.net") != -1)
                return 'staging';
            else
                return 'development';


            /*switch(h){
                case 'localhost':
                case 'addypin':
                    return 'development';
                break;

                case 'addypin.digitalmeetings.net':
                    return 'staging';
                break;

                case 'addypin.com':
                case 'www.addypin.com':
                    return 'production';
                break;
            }*/
        },
        getSubdomainName: function (host, env){
            var subdomain = "";
            switch(env){
                case 'development':
                    subdomain = host.replace(".addypin", "");
                break;

                case 'staging':
                    subdomain = host.replace(".addypin.digitalmeetings.net", "");
                break;

                case 'production':
                    subdomain = host.replace(".addypin.com", "");
                break;
            }
            return subdomain;
        }
    };
});