angular.module('main').factory('common', function ($timeout, $http, deviceDetector, url){
    return {
        http: {
            post: function (url, data, successCallback, errorCallback){
                var promise = $http({
                    url: url,
                    method: 'POST',
                    data: data
                }).then(function (response){
                    if(successCallback && successCallback != undefined && successCallback != null){
                        successCallback(response);
                    }
                    return response.data;
                },
                function (response){
                    if(errorCallback && errorCallback != undefined && errorCallback != null){
                        errorCallback(response);
                    }
                    //return response.data;
                });
                return promise;
            },
            get: function (url, data, successCallback, errorCallback){
                var promise = $http({
                    url: url,
                    method: 'GET',
                }).then(function (response){
                        if(successCallback && successCallback != undefined && successCallback != null){
                            successCallback(response);
                        }
                        return response.data;
                    },
                    function (response){
                        if(errorCallback && errorCallback != undefined && errorCallback != null){
                            errorCallback(response);
                        }
                        //return response.data;
                    });
                return promise;
            }
        },
        environmentConfig: function (){
            return {
                domains: {
                    development: ['admin.addypin'],
                        production: ['admin.addypin.com']
                    // anotherStage: []
                },
                vars: {
                    development: {
                        apiUrl: '//api.addypin',
                            //staticUrl: '//localhost/static'
                        // antoherCustomVar: ''
                    },
                    production: {
                        apiUrl: '//api.addypin.com',
                            //staticUrl: '//static.acme.com'
                        // antoherCustomVar: ''
                    }
                }
            }
        },
        getUserData: function (){
            var dt = deviceDetector;
            var data = {
                email: '',
                type: '',
                browser: dt.browser,
                device: dt.device,
                os: dt.os,
                os_version: dt.os_version,
                is_desktop: dt.isDesktop(),
                is_mobile: dt.isMobile(),
                is_table: dt.isTablet(),
                user_agent: dt.raw.userAgent
            };
            return data;
        },
        getPinDataByCode: function (code,navigatorUser){
            var promise = $http({ url: url.apiUrl('/pin/by/code/' + code +'/'+ navigatorUser), method: 'GET'})
                .then(function (response){
                    console.log(response);
                   
                    return response.data;   

                    
                });
            return promise;
        }
    };
});