angular.module('main', ['ui.router', 'angular-jwt', 'httpPostFix', 'environment', 'ng.deviceDetector','ngMap','720kb.socialshare'])
    .run(['$rootScope', '$state', function($rootScope, $state) {

    }]).factory('httpInterceptor', function ($q, $rootScope, $injector) {
        return {
            request: function (config) {
                angular.element( document.querySelector( '.cssload-loader' ) ).removeClass('hide');
                return config || $q.when(config)
            },
            response: function (response) {
                angular.element( document.querySelector( '.cssload-loader' ) ).addClass('hide');
                return response || $q.when(response);
            },
            responseError: function (response) {
                angular.element( document.querySelector( '.cssload-loader' ) ).addClass('hide');
                if (response.status === 401) {
                    console.log('401 Occurred');
                    $injector.get('$state').transitionTo('main', { 'e': 1 });
                }
                return $q.reject(response);
            }
        };
    })
    .config(function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, jwtInterceptorProvider, /*common,*/ envServiceProvider/*, envService*/) {
        $stateProvider
            /*.state('main', {
                url: '/home',
                templateUrl: 'modules/shared/views/home-layout.html',
                controller: 'HomeController'
            })*/
            .state('pin', {
                url: '/pin',
                //template: '<div ui-view></div>', 
                templateUrl: 'modules/shared/views/layout.html',
                controller: 'PinController'
            })
			 .state('limitReached',{
              url: '/limitReached',
                //template: '<div ui-view></div>',
                templateUrl: 'modules/shared/views/limitReached.html',
                controller: 'PinController'
            })
			 .state('addypinNotFound',{
              url: '/addypinNotFound',
                //template: '<div ui-view></div>',
                templateUrl: 'modules/shared/views/addypinNotFound.html',
                controller: 'PinController'
            })
            .state('notfound',{
              url: '/notfound',
                //template: '<div ui-view></div>',
                templateUrl: 'modules/shared/views/notfound.html',
                controller: 'PinController'
            });
        $urlRouterProvider.otherwise('/pin');

        jwtInterceptorProvider.tokenGetter = ['common', function(common) {
            return localStorage.getItem('token');
        }];

        $httpProvider.interceptors.push('jwtInterceptor');
        $httpProvider.interceptors.push('httpInterceptor');
        envServiceProvider.config({
            domains: {
                development: ['admin.addypin'],
                staging: ['admin.addypin.digitalmeetings.net'],
                production: ['admin.addypin.com']
                // anotherStage: []
            },
            vars: {
                development: {
                    apiUrl: '//api.addypin.com/index.php',
                    subdomainUrl: '.addypin',
                    //staticUrl: '//localhost/static'
                    // antoherCustomVar: ''
                },
                staging: {
                    apiUrl: '//api.addypin.digitalmeetings.net',
                    subdomainUrl: '.addypin.digitalmeetings.net',
                },
                production: {
                    apiUrl: '//api.addypin.com/index.php',
                    subdomainUrl: '.addypin.com',
                    //staticUrl: '//static.acme.com'
                    // antoherCustomVar: ''
                }
            }
        });
        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.withCredentials = true;
        delete $httpProvider.defaults.headers.common["X-Requested-With"];
        $httpProvider.defaults.headers.common["Accept"] = "application/json";
        $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
        //common.environmentConfig();
        //envService.set('development');
    })
    .controller('PinController', ['$scope', '$rootScope', '$location', '$timeout', '$stateParams', 'deviceDetector', 'url', 'common', 'envService','$state', function ($scope, $rootScope, $location, $timeout, $stateParams, deviceDetector, url, common, envService,$state) {
        envService.set(url.host($location.host()));
        //$scope.pinLoaded = false;
        var host = $location.host();
        // formatting host, changing first index to all caps <start>
        hostpart = host.split('.');
        hostpart[0] = hostpart[0].toUpperCase();
        host = hostpart.join('.');


        $scope.shareWhatApp = function(){
 
        // var message = encodeURIComponent(text) + " - " + encodeURIComponent(url);

        var message = "Find me here "+$scope.code+".addypin.com or email "+ $scope.code +"@addypin.com"
       
        message =  encodeURIComponent(message);
       // alert (message)
        var whatsapp_url = "whatsapp://send?text=" + message;
       // alert(whatsapp_url);
        window.location.href = whatsapp_url;
    }

        // changing page title
        document.title = document.title+' - '+hostpart[0];
		
		$scope.hostpart = hostpart[0];

        $scope.isAndroid = isAndroid();
        $scope.isIOS = isIOS();
        $scope.isBlackBerry = isBlackBerry();
        $scope.isWindowPhone = isWindowPhone();
        // <end>

        var code = url.getSubdomainName(host, envService.get());
        $scope.code = code;
        $scope.host = $location.protocol() + "://" + host;
        console.log($location);
        $scope.message = "";

        
                       
                   
        common.getPinDataByCode(code).then(function (data){
            //console.log(data);
             angular.element( document.querySelector( '.cssload-loader' ) ).removeClass('hide');
            if(data != null && data != undefined && data.pin != null && data.pin != undefined && data.pin[0] != null && data.pin[0] != undefined){
               
              /* $timeout(function () {*/
                angular.element( document.querySelector( '.cssload-loader' ) ).addClass('hide');
                $scope.pinData = data.pin[0];
                var userAgent = navigator.userAgent || navigator.vendor || window.opera;
                $scope.pinData.btn1 = "";
                $scope.pinData.btn2 = "";
                $scope.pinData.btn3 = "";
                  // Windows Phone must come first because its UA also contains "Android"
                $scope.pinData.btn2 += 'http://maps.google.com/?q='+$scope.pinData.lat+','+$scope.pinData.long+'';
                if (/windows phone/i.test(userAgent)) {
                    $scope.pinData.btn3 += 'http://maps.apple.com/?q='+$scope.pinData.lat+','+$scope.pinData.long;
                }

                if (/android/i.test(userAgent)) {
                }

                // iOS detection from: http://stackoverflow.com/a/9039885/177710
                if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
                    //http://maps.apple.com/?q='+lat+','+lng+'
                    $scope.pinData.btn2 += 'http://maps.google.com/?q='+$scope.pinData.lat+','+$scope.pinData.long;
                    $scope.pinData.btn1 += 'http://maps.apple.com/?q='+$scope.pinData.lat+','+$scope.pinData.long;
                }
                $scope.pinData.referer = navigator.userAgent;
                $scope.pinData.code = $scope.pinData.code.toUpperCase();

				console.log(data.pin[0]);
           /*  }, 10000);*/
            }else if(data.limit_reached != null){
				$state.go('limitReached');
         	}else if(data.notfound != null){
				$state.go('addypinNotFound');
         	}
            else{
                // Something went wrong and the code is not available.
                $state.go('notfound');
                console.log("Pin not found");
                $scope.message = "The pin you're trying to view was not found!";
            }
        });
          

        $scope.navigate = function (lat, lng, type) {
            console.log('I am here'+lat+'  '+lng)
            window.open('http://maps.apple.com/?q='+lat+','+lng+'');
            // If it's an iPhone..
            /*console.log0(navigator.platform)
            if ((navigator.platform.indexOf("iPhone") !== -1) || (navigator.platform.indexOf("iPod") !== -1)) {
                function iOSversion() {
                    if (/iP(hone|od|ad)/.test(navigator.platform)) {
                        // supports iOS 2.0 and later: <http://bit.ly/TJjs1V>
                        var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
                        return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
                    }
                }
                var ver = iOSversion() || [0];

                if (ver[0] >= 6) {
                    protocol = 'maps://';
                } else {
                    protocol = 'http://';

                }
                window.location = protocol + 'maps.apple.com/maps?daddr=' + lat + ',' + lng + '&amp;ll=';
            }
            else {
                //window.open('http://maps.google.com?daddr=' + lat + ',' + lng + '&amp;ll=');
                //window.open('http://maps.blackberry.com/?lat=' + lat + '&lon=' + lng + '&scr_z=4&z=4');
                if(type == 1)
                    window.location = 'http://maps.blackberry.com/?lat=' + lat + '&lon=' + lng + '&addrLabel=&address=&city=&country=&label=&scr_cnt=&scr_z=4&z=4';
                else if(type == 2)
                    window.open('https://www.google.com/maps?z=15&t=m&q=loc:' + lat + '+' + lng + '');
                    //window.open('http://maps.blackberry.com/?lat=' + lat + '&lon=' + lng + '&addrLabel=&address=&city=&country=&label=&scr_cnt=&scr_z=4&z=4');
            }*/
        };

        window.dv = deviceDetector;
        console.log(deviceDetector);
    }])
    .directive('toggle', function(){
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                if (attrs.toggle=="tooltip"){
                    $(element).tooltip();
                }
                if (attrs.toggle=="popover"){
                    $(element).popover();
                }
            }
        };
    })
;


function isAndroid(){
    return navigator.userAgent.match(/Android/i);
}

function isIOS(){
    return (navigator.userAgent.match(/iPhone/i)
 || navigator.userAgent.match(/iPad/i)
 || navigator.userAgent.match(/iPod/i));
}

function isBlackBerry(){
    return navigator.userAgent.match(/BlackBerry/i);
}

function isWindowPhone(){
    return navigator.userAgent.match(/Windows Phone/i);
}



function detectmob() { 
 if( navigator.userAgent.match(/Android/i)
 || navigator.userAgent.match(/webOS/i)
 || navigator.userAgent.match(/iPhone/i)
 || navigator.userAgent.match(/iPad/i)
 || navigator.userAgent.match(/iPod/i)
 || navigator.userAgent.match(/BlackBerry/i)
 || navigator.userAgent.match(/Windows Phone/i)
 ){
    return true;
  }
 else {
    return false;
  }
}